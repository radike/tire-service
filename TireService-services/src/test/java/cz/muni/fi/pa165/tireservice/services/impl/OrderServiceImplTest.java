/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser;
import static cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser.parseAdditionalServices;
import static cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser.parseCustomer;
import static cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser.parseOrder;
import static cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser.parseTire;
import cz.muni.fi.pa165.tireservice.dto.OrderDto;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.enums.TireType;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import static org.mockito.Mockito.mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Samuel Briškár
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:test-spring-security.xml")
public class OrderServiceImplTest {

    private OrderDao dao;

    @Before
    public void setup(){
        dao = mock(OrderDao.class);
    }
    
    @Test
    public void insertOrder() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        Order order = new Order();

        order.setAdditionalServices(createSampleAdditionalServices());
        order.setCustomer(createSampleCustomer());
        order.setTire(createSampleTire());
        order.setVehicle(VehicleType.CAR);

        OrderDto orderDto = DtoParser.parseOrder(order);
        
        adminAuthentication();
        
        orderService.createOrder(orderDto);

        Mockito.verify(dao).insert(order);
    }

    @Test
    public void insertOrderWithTotalPrice() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        Order order = new Order();

        order.setAdditionalServices(createSampleAdditionalServices());
        order.setCustomer(createSampleCustomer());
        order.setTire(createSampleTire());
        order.setVehicle(VehicleType.CAR);
        BigDecimal totalPrice = order.getTire().getPrice();
        for (AdditionalService s : order.getAdditionalServices()) {
            totalPrice = totalPrice.add(s.getPrice());
        }
        order.setTotalPrice(totalPrice);
        OrderDto orderDto = DtoParser.parseOrder(order);
        
        adminAuthentication();
        
        orderService.createOrder(orderDto);

        Mockito.verify(dao).insert(order);
    }

    @Test(expected = ServiceFailureException.class)
    public void insertOrderServiceWithWrongTotalPrice() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        Order order = new Order();

        order.setAdditionalServices(createSampleAdditionalServices());
        order.setCustomer(createSampleCustomer());
        order.setTire(createSampleTire());
        order.setVehicle(VehicleType.CAR);
        order.setTotalPrice(BigDecimal.TEN);
        OrderDto orderDto = DtoParser.parseOrder(order);
        orderService.createOrder(orderDto);
    }

    @Test
    public void findAll() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        orderService.getAllOrders();
        Mockito.verify(dao).findAll();
    }

    @Test
    public void update() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        orderService.updateOrder(createSampleOrderDto1());
        Mockito.verify(dao).update(createSampleOrder1());
    }

    @Autowired
    private AuthenticationManager manager;
    
    @Test
    public void getOrdersByCustomer() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        
         //Get the user by username from configured user details service
        adminAuthentication();        
        
        orderService.getOrdersByCustomer(createSampleCustomerDto1());
        Mockito.verify(dao).findByCustomer(createSampleCustomer());
    }

    @Test
    public void getOrdersByTotalPrice() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        BigDecimal bigDecimal = new BigDecimal(1.0);
        orderService.getOrdersByTotalPrice(bigDecimal);
        Mockito.verify(dao).findByTotalPrice(bigDecimal);
    }

    @Test
    public void getOrdersByDate() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        Date date = new Date();
        Date date2 = new Date();
        orderService.getOrdersByDate(date, date2);
        Mockito.verify(dao).findByDate(date, date2);
    }

    @Test
    public void remove() {
        OrderServiceImpl orderService = new OrderServiceImpl();
        orderService.setOrderDao(dao);
        orderService.removeOrder(createSampleOrderDto1());
        Mockito.verify(dao).remove(createSampleOrder1());
    }

    private OrderDto createSampleOrderDto1() {
        Order order1 = createSampleOrder1();
        OrderDto order2 = new OrderDto();
        order2 = parseOrder(order1);
        return order2;
    }

    private Order createSampleOrder1() {
        Order order1 = new Order();

        order1.setTotalPrice(new BigDecimal(1.0));
        order1.setVehicle(VehicleType.TRUCK);
        order1.setDate(new Date());
        order1.setCustomer(createSampleCustomer());
        order1.setTire(createSampleTire());

        return order1;
    }

    private Customer createSampleCustomer() {
        Customer customer = new Customer();
        customer.setName("John Doe");
        customer.setAddress("Doeville 316");
        customer.setPhoneNumber("00 420 720 512 632");
        return customer;
    }

    private CustomerDto createSampleCustomerDto1() {
        Customer customer1 = createSampleCustomer();
        CustomerDto customer2 = new CustomerDto();
        customer2 = parseCustomer(customer1);
        return customer2;
    }

    private Tire createSampleTire() {
        Tire tire = new Tire();
        tire.setType(TireType.ROAD);
        tire.setSize(54);
        tire.setManufacturer("TireManufacturer");
        tire.setPrice(new BigDecimal(123));

        return tire;
    }

    private TireDto createSampleTireDto1() {
        Tire tire1 = createSampleTire();
        TireDto tire2 = new TireDto();
        tire2 = parseTire(tire1);
        return tire2;
    }

    private List<AdditionalService> createSampleAdditionalServices() {
        AdditionalService service1 = new AdditionalService();
        service1.setName("service1");
        service1.setPrice(new BigDecimal(1));

        AdditionalService service2 = new AdditionalService();
        service2.setName("service2");
        service2.setPrice(new BigDecimal(2));

        return Arrays.asList(service1, service2);
    }

    private List<AdditionalServiceDto> createSampleAdditionalServicesDto1() {
        List<AdditionalService> additionalServices1 = createSampleAdditionalServices();
        List<AdditionalServiceDto> additionalServices2 = new ArrayList<AdditionalServiceDto>();
        additionalServices2 = parseAdditionalServices(additionalServices1);
        return additionalServices2;
    }

    private void adminAuthentication() {
        Authentication authToken = new UsernamePasswordAuthenticationToken ("admin", "admin");
        Authentication auth = manager.authenticate(authToken);
        SecurityContextHolder.getContext().setAuthentication(auth);
    }
}
