/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dao.TireDao;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.dto.DtoTestHelper;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.enums.TireType;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.junit.Assert.*;
import org.junit.Before;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.dao.DataAccessException;

/**
 * Tests of {@link TireServiceImpl}
 * 
 * @author Martin Pruska
 */
public class TireServiceImplTest {

    private final ServiceFailureException serviceFailureException =
            new ServiceFailureException("simulation of service failure");
    private final IllegalArgumentException illegalArgumentException = 
            new IllegalArgumentException("simulation of invoking service with illegal argument");
    private TireServiceImpl service;
    private Tire tire;
    private TireDto tireDto;
    private TireDao dao;

    @Before
    public void setUp() {
        dao = mock(TireDao.class);

        service = new TireServiceImpl();
        service.setTireDao(dao);

        tire = new Tire();
        tire.setId(2);
        tire.setType(TireType.ROAD);
        tire.setSize(50);
        tire.setManufacturer("Michelin");
        tire.setPrice(new BigDecimal(100));
        
        tireDto = new TireDto();
        tireDto.setId(2);
        tireDto.setType(TireType.ROAD);
        tireDto.setSize(50);
        tireDto.setManufacturer("Michelin");
        tireDto.setPrice(new BigDecimal(100));
    }

    @Test
    public void createTire() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Tire t = invocation.getArgumentAt(0, Tire.class);
                assertEquals(tireDto.getType(), t.getType());
                assertEquals(tireDto.getSize(), t.getSize());
                assertEquals(tireDto.getManufacturer(), t.getManufacturer());
                assertEquals(tireDto.getPrice(), t.getPrice());
                return null;
            }
        }).when(dao).insert(isA(Tire.class));
        service.createTire(tireDto);
        verify(dao, times(1)).insert(isA(Tire.class));
    }

    @Test(expected = DataAccessException.class)
    public void exceptionInCreateTire() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw illegalArgumentException;
            }
        }).when(dao).insert(isA(Tire.class));
        service.createTire(tireDto);
    }
    
    @Test
    public void updateTire() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Tire t = invocation.getArgumentAt(0, Tire.class);
                DtoTestHelper.assertTire(t, tireDto);
                return null;
            }
        }).when(dao).update(isA(Tire.class));
        service.updateTire(tireDto);
        verify(dao, times(1)).update(isA(Tire.class));
    }
    
    @Test(expected = DataAccessException.class)
    public void exceptionInUpdateTire() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw serviceFailureException;
            }
        }).when(dao).update(isA(Tire.class));
        service.updateTire(tireDto);
    }
    
    @Test
    public void removeTire() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Tire t = invocation.getArgumentAt(0, Tire.class);
                DtoTestHelper.assertTire(t, tireDto);
                return null;
            }
        }).when(dao).remove(isA(Tire.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return tire;
            }
        }).when(dao).findById(tireDto.getId());
        OrderDao orderDao = mock(OrderDao.class);
        service.setOrderDao(orderDao);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return new ArrayList<Order>();
            }
        }).when(orderDao).findByTire(tire);
        service.removeTire(tireDto);
        verify(dao, times(1)).remove(isA(Tire.class));
    }
    
    @Test(expected=EntityHasOrderException.class)
    public void removeAdditionalServiceThatIsInRelatonWithOrder() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return tire;
            }
        }).when(dao).findById(tireDto.getId());
        OrderDao orderDao = mock(OrderDao.class);
        service.setOrderDao(orderDao);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return Arrays.asList(new Order());
            }
        }).when(orderDao).findByTire(tire);
        service.removeTire(tireDto);
    }
    
    @Test(expected = DataAccessException.class)
    public void exceptionInRemoveTire() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw illegalArgumentException;
            }
        }).when(dao).remove(isA(Tire.class));
        service.removeTire(tireDto);
    }
    
    @Test
    public void getAllTires() {
        when(dao.findAll()).thenReturn(Arrays.asList(tire));
        List<TireDto> tires = service.getAllTires();
        assertNotNull(tires);
        assertEquals(tires.size(), 1);
        DtoTestHelper.assertTire(tire, tires.get(0));
        verify(dao, times(1)).findAll();
    }
    
    @Test(expected = DataAccessException.class)
    public void exceptionInGetAllTire() {
        when(dao.findAll()).thenThrow(serviceFailureException);
        service.getAllTires();
    }
}
