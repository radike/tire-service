/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dto;

import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.enums.TireType;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

/**
 * Test base for Dto's tests. Contains assertion methods, which create sample
 * dto objects.
 *
 * @author Radoslav Rabara
 */
public class DtoTestBase {

    protected Customer createSampleCustomer() {
        Customer entity = new Customer();
        entity.setId(1048);
        entity.setName("John");
        entity.setPhoneNumber("0012300456");
        entity.setAddress("Doeland");
        entity.setVehicles(Arrays.asList(VehicleType.CAR, VehicleType.SUV));
        return entity;
    }

    protected CustomerDto createSampleCustomerDto() {
        CustomerDto dto = new CustomerDto();
        dto.setId(7);
        dto.setName("James Bond");
        dto.setPhoneNumber("007");
        dto.setAddress("London, England");
        dto.setVehicles(Arrays.asList(VehicleType.CAR));
        return dto;
    }

    protected Tire createSampleTire() {
        Tire entity = new Tire();
        entity.setId(2049);
        entity.setManufacturer("Super Manufacturer");
        entity.setPrice(BigDecimal.valueOf(69.9));
        entity.setType(TireType.ROAD);
        return entity;
    }

    protected TireDto createSampleTireDto() {
        TireDto dto = new TireDto();
        dto.setId(2049);
        dto.setManufacturer("Super Manufacturer");
        dto.setPrice(BigDecimal.valueOf(69.9));
        dto.setType(TireType.ROAD);
        return dto;
    }

    protected List<AdditionalService> createSampleAdditionalServices() {
        AdditionalService entity1 = new AdditionalService();
        entity1.setId(34);
        entity1.setName("service1");
        entity1.setPrice(new BigDecimal(1));

        AdditionalService entity2 = new AdditionalService();
        entity2.setId(47);
        entity2.setName("service2");
        entity2.setPrice(new BigDecimal(2));

        return Arrays.asList(entity1, entity2);
    }

    protected List<AdditionalServiceDto> createSampleAdditionalServicesDto() {
        AdditionalServiceDto dto1 = new AdditionalServiceDto();
        dto1.setId(32);
        dto1.setName("service1dto");
        dto1.setPrice(new BigDecimal(12));

        AdditionalServiceDto dto2 = new AdditionalServiceDto();
        dto2.setId(42);
        dto2.setName("service2dto");
        dto2.setPrice(new BigDecimal(22));

        return Arrays.asList(dto1, dto2);
    }
    
    protected long generateId() {
        return (long) (Math.random() * 100);
    }
}
