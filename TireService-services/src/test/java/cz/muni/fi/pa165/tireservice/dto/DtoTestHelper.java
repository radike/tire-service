/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dto;

import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Dto Test helper. Contains assertion methods, which asserts entity with dto.
 *
 * @author Radoslav Rabara
 */
public class DtoTestHelper {

    public static void assertCustomer(Customer entity, CustomerDto dto) {
        assertNotNull(entity);
        assertNotNull(dto);
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getAddress(), dto.getAddress());
        assertEquals(entity.getPhoneNumber(), dto.getPhoneNumber());
        assertEquals(entity.getVehicles(), dto.getVehicles());
    }

    public static void assertTire(Tire entity, TireDto dto) {
        assertNotNull(entity);
        assertNotNull(dto);
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getManufacturer(), dto.getManufacturer());
        assertEquals(entity.getPrice(), dto.getPrice());
        assertEquals(entity.getType(), dto.getType());
    }

    public static void assertAdditionalService(AdditionalService entity,
            AdditionalServiceDto dto) {
        assertEquals(entity.getId(), dto.getId());
        assertEquals(entity.getName(), dto.getName());
        assertEquals(entity.getPrice(), dto.getPrice());
    }

    public static void assertAdditionalServices(List<AdditionalService> entities,
            List<AdditionalServiceDto> dtos) {
        assertNotNull(entities);
        assertNotNull(dtos);
        assertEquals(entities.size(), dtos.size());
        for (int i = 0; i < entities.size(); i++) {
            AdditionalService entity = entities.get(i);
            AdditionalServiceDto dto = dtos.get(i);
            assertAdditionalService(entity, dto);
        }
    }
}
