/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.CustomerDao;
import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.dto.DtoTestHelper;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import static org.junit.Assert.*;
import org.junit.Before;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import org.springframework.dao.DataAccessException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Tests of {@link CustomerServiceImpl}
 *
 * @author Radoslav Rabara
 */
public class CustomerServiceImplTest {

    private final ServiceFailureException serviceFailureException
            = new ServiceFailureException("simulation of service failure");
    private final IllegalArgumentException illegalArgumentException
            = new IllegalArgumentException("simulation of invoking service with illegal argument");
    private CustomerServiceImpl service;
    private Customer customer;
    private CustomerDto customerDto;
    private CustomerDao dao;

    @Before
    public void setUp() {
        dao = mock(CustomerDao.class);
        
        service = new CustomerServiceImpl();
        service.setCustomerDao(dao);
        service.setEncoder(new BCryptPasswordEncoder());
        
        customer = new Customer();
        customer.setId(2);
        customer.setName("John");
        customer.setAddress("Doeland");
        customer.setPhoneNumber("00420123456789");
        customer.setVehicles(Arrays.asList(VehicleType.CAR));

        customerDto = new CustomerDto();
        customerDto.setId(2);
        customerDto.setName("John");
        customerDto.setAddress("Doeland");
        customerDto.setPhoneNumber("00420123456789");
        customerDto.setVehicles(Arrays.asList(VehicleType.CAR));
    }

    @Test
    public void createCustomer() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Customer c = invocation.getArgumentAt(0, Customer.class);
                assertEquals(customerDto.getName(), c.getName());
                assertEquals(customerDto.getAddress(), c.getAddress());
                assertEquals(customerDto.getPhoneNumber(), c.getPhoneNumber());
                assertEquals(customerDto.getVehicles(), c.getVehicles());
                return null;
            }
        }).when(dao).insert(isA(Customer.class));
        service.createCustomer(customerDto);
        verify(dao, times(1)).insert(isA(Customer.class));
    }

    @Test(expected = DataAccessException.class)
    public void exceptionInCreateCustomer() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw illegalArgumentException;
            }
        }).when(dao).insert(isA(Customer.class));
        service.createCustomer(customerDto);
    }

    @Test
    public void updateCustomer() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Customer c = invocation.getArgumentAt(0, Customer.class);
                DtoTestHelper.assertCustomer(c, customerDto);
                return null;
            }
        }).when(dao).update(isA(Customer.class));
        service.updateCustomer(customerDto);
        verify(dao, times(1)).update(isA(Customer.class));
    }

    @Test(expected = DataAccessException.class)
    public void exceptionInUpdateCustomer() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw serviceFailureException;
            }
        }).when(dao).update(isA(Customer.class));
        service.updateCustomer(customerDto);
    }

    @Test
    public void removeCustomer() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                Customer c = invocation.getArgumentAt(0, Customer.class);
                DtoTestHelper.assertCustomer(c, customerDto);
                return null;
            }
        }).when(dao).remove(isA(Customer.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return customer;
            }
        }).when(dao).findById(customerDto.getId());
        OrderDao orderDao = mock(OrderDao.class);
        service.setOrderDao(orderDao);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return new ArrayList<Order>();
            }
        }).when(orderDao).findByCustomer(customer);
        service.removeCustomer(customerDto);
        verify(dao, times(1)).remove(isA(Customer.class));
    }

    @Test(expected=EntityHasOrderException.class)
    public void removeAdditionalServiceThatIsInRelatonWithOrder() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return customer;
            }
        }).when(dao).findById(customerDto.getId());
        OrderDao orderDao = mock(OrderDao.class);
        service.setOrderDao(orderDao);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return Arrays.asList(new Order());
            }
        }).when(orderDao).findByCustomer(customer);
        service.removeCustomer(customerDto);
    }
    
    @Test(expected = DataAccessException.class)
    public void exceptionInRemoveCustomer() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw illegalArgumentException;
            }
        }).when(dao).remove(isA(Customer.class));
        service.removeCustomer(customerDto);
    }

    @Test
    public void getAllCustomers() {
        when(dao.findAll()).thenReturn(Arrays.asList(customer));
        List<CustomerDto> customers = service.getAllCustomers();
        assertNotNull(customers);
        assertEquals(customers.size(), 1);
        DtoTestHelper.assertCustomer(customer, customers.get(0));
        verify(dao, times(1)).findAll();
    }

    @Test(expected = DataAccessException.class)
    public void exceptionInGetAllCustomer() {
        when(dao.findAll()).thenThrow(serviceFailureException);
        service.getAllCustomers();
    }
}
