/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.AdditionalServiceDao;
import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.dto.DtoTestHelper;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.junit.Assert.*;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Before;
import org.junit.Test;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Marek
 */
public class AdditionalServiceServiceImplTest {

    private final ServiceFailureException serviceFailureException
            = new ServiceFailureException("simulation of service failure");
    private final IllegalArgumentException illegalArgumentException
            = new IllegalArgumentException("simulation of invoking service with illegal argument");
    private AdditionalServiceServiceImpl service;
    private AdditionalService additionalService;
    private AdditionalServiceDto additionalServiceDto;
    private AdditionalServiceDao dao;

    @Before
    public void setUp() {
        dao = mock(AdditionalServiceDao.class);

        service = new AdditionalServiceServiceImpl();
        service.setAdditionalServiceDao(dao);

        additionalService = new AdditionalService();
        additionalService.setName("vyvazenie kolies");
        additionalService.setPrice(new BigDecimal(5.0));

        additionalServiceDto = new AdditionalServiceDto();
        additionalServiceDto.setName("vyvazenie kolies");
        additionalServiceDto.setPrice(new BigDecimal(5.0));
    }

    @Test
    public void createAdditionalService() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AdditionalService a = invocation.getArgumentAt(0, AdditionalService.class);
                assertEquals(additionalServiceDto.getName(), a.getName());
                assertEquals(additionalServiceDto.getPrice(), a.getPrice());
                return null;
            }
        }).when(dao).insert(isA(AdditionalService.class));
        service.createAdditionalService(additionalServiceDto);
        verify(dao, times(1)).insert(isA(AdditionalService.class));
    }

    @Test(expected = DataAccessException.class)
    public void exceptionInCreateAdditionalService() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw illegalArgumentException;
            }
        }).when(dao).insert(isA(AdditionalService.class));
        service.createAdditionalService(additionalServiceDto);
    }

    @Test
    public void updateAdditionalService() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AdditionalService a = invocation.getArgumentAt(0, AdditionalService.class);
                List<AdditionalService> entityList = new ArrayList<AdditionalService>();
                entityList.add(a);
                List<AdditionalServiceDto> dtoList = new ArrayList<AdditionalServiceDto>();
                dtoList.add(additionalServiceDto);
                DtoTestHelper.assertAdditionalServices(entityList, dtoList);
                return null;
            }
        }).when(dao).update(isA(AdditionalService.class));
        service.updateAdditionalService(additionalServiceDto);
        verify(dao, times(1)).update(isA(AdditionalService.class));
    }

    @Test(expected = DataAccessException.class)
    public void exceptionInUpdateAdditionalService() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw serviceFailureException;
            }
        }).when(dao).update(isA(AdditionalService.class));
        service.updateAdditionalService(additionalServiceDto);
    }

    @Test
    public void removeAdditionalService() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                AdditionalService a = invocation.getArgumentAt(0, AdditionalService.class);
                List<AdditionalService> entityList = new ArrayList<AdditionalService>();
                entityList.add(a);
                List<AdditionalServiceDto> dtoList = new ArrayList<AdditionalServiceDto>();
                dtoList.add(additionalServiceDto);
                DtoTestHelper.assertAdditionalServices(entityList, dtoList);
                return null;
            }
        }).when(dao).remove(isA(AdditionalService.class));
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return additionalService;
            }
        }).when(dao).findById(additionalServiceDto.getId());
        OrderDao orderDao = mock(OrderDao.class);
        service.setOrderDao(orderDao);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return new ArrayList<Order>();
            }
        }).when(orderDao).findByAdditionalService(additionalService);
        service.removeAdditionalService(additionalServiceDto);
        verify(dao, times(1)).remove(isA(AdditionalService.class));
    }

    @Test(expected = EntityHasOrderException.class)
    public void removeAdditionalServiceThatIsInRelatonWithOrder() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return additionalService;
            }
        }).when(dao).findById(additionalServiceDto.getId());
        OrderDao orderDao = mock(OrderDao.class);
        service.setOrderDao(orderDao);
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                return Arrays.asList(new Order());
            }
        }).when(orderDao).findByAdditionalService(additionalService);
        service.removeAdditionalService(additionalServiceDto);
    }
    
    @Test(expected = DataAccessException.class)
    public void exceptionInRemoveAdditionalService() {
        doAnswer(new Answer() {
            @Override
            public Object answer(InvocationOnMock invocation) throws Throwable {
                throw illegalArgumentException;
            }
        }).when(dao).remove(isA(AdditionalService.class));
        service.removeAdditionalService(additionalServiceDto);
    }

    @Test
    public void getAllAdditionalServices() {
        when(dao.findAll()).thenReturn(Arrays.asList(additionalService));
        List<AdditionalServiceDto> additionalServices = service.getAllAdditionalServices();
        assertNotNull(additionalServices);
        assertEquals(additionalServices.size(), 1);
        List<AdditionalService> entityList = new ArrayList<AdditionalService>();
        entityList.add(additionalService);
        DtoTestHelper.assertAdditionalServices(entityList, additionalServices);
        verify(dao, times(1)).findAll();
    }

    @Test(expected = DataAccessException.class)
    public void exceptionIngetAllAdditionalServices() {
        when(dao.findAll()).thenThrow(serviceFailureException);
        service.getAllAdditionalServices();
    }

    @Test
    public void getAdditionalServicesByName() {
        final String name1 = "name1";
        final String name2 = "name2";
        when(dao.findByName(name1)).thenReturn(null);
        when(dao.findByName(name2)).thenReturn(additionalService);

        AdditionalServiceDto result = service.getAdditionalServiceByName(name1);
        assertNull(result);

        result = service.getAdditionalServiceByName(name2);
        assertNotNull(result);

        DtoTestHelper.assertAdditionalService(additionalService, result);
        verify(dao, times(1)).findByName(name1);
        verify(dao, times(1)).findByName(name2);
    }

    @Test(expected = DataAccessException.class)
    public void exceptionIngetAdditionalServiceByName() {
        final String name = "name";
        when(dao.findByName(name)).thenThrow(serviceFailureException);
        service.getAdditionalServiceByName(name);
    }
}
