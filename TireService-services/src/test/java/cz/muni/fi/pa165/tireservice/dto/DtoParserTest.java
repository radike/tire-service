/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dto;

import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import java.math.BigDecimal;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 * Test of {@link DtoParser}.
 *
 * @author Radoslav Rabara
 */
public class DtoParserTest extends DtoTestBase {

    @Test
    public void parse() {
        long id = generateId();
        String name = "service name";
        BigDecimal price = BigDecimal.valueOf(1208);

        AdditionalService entity = createAdditionalService(id, name, price);
        AdditionalServiceDto dto = DtoParser.parseAdditionalService(entity);
        DtoTestHelper.assertAdditionalService(createAdditionalService(id, name, price), dto);
    }

    @Test
    public void parseOrder() {
        long id = generateId();
        Date date = new Date((int) (Math.random() * 10_000));
        BigDecimal totalPrice = BigDecimal.valueOf(Math.random() * 10_000);
        VehicleType vehicle = VehicleType.CAR;

        Order entity = new Order();
        entity.setId(id);
        entity.setDate(date);
        entity.setTotalPrice(totalPrice);
        entity.setVehicle(vehicle);
        entity.setCustomer(createSampleCustomer());
        entity.setTire(createSampleTire());
        entity.setAdditionalServices(createSampleAdditionalServices());

        OrderDto dto = DtoParser.parseOrder(entity);
        assertNotNull(dto);
        assertEquals(id, dto.getId());
        assertEquals(date, dto.getDate());
        assertEquals(totalPrice, dto.getTotalPrice());
        assertEquals(vehicle, dto.getVehicle());

        DtoTestHelper.assertCustomer(createSampleCustomer(), dto.getCustomer());
        DtoTestHelper.assertTire(createSampleTire(), dto.getTire());
        DtoTestHelper.assertAdditionalServices(createSampleAdditionalServices(),
                dto.getAdditionalServices());
    }

    private AdditionalService createAdditionalService(long id, String name, BigDecimal price) {
        AdditionalService entity = new AdditionalService();
        entity.setId(id);
        entity.setName(name);
        entity.setPrice(price);
        return entity;
    }
}
