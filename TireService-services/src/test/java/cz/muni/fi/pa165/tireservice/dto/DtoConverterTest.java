/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dto;

import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoConverter;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import java.math.BigDecimal;
import java.util.Date;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import org.junit.Test;

/**
 * Test of {@link DtoConverter}.
 *
 * @author Radoslav Rabara
 */
public class DtoConverterTest extends DtoTestBase {

    @Test
    public void convertToAdditionalService() {
        long id = generateId();
        String name = "service name";
        BigDecimal price = BigDecimal.valueOf(1208);

        AdditionalServiceDto dto = createAdditionalServiceDto(id, name, price);
        AdditionalService entity = DtoConverter.convertToAdditionalService(dto);
        DtoTestHelper.assertAdditionalService(entity, createAdditionalServiceDto(id, name, price));
    }

    @Test
    public void convertToOrder() {
        long id = generateId();
        Date date = new Date((int) (Math.random() * 10_000));
        BigDecimal totalPrice = BigDecimal.valueOf(Math.random() * 10_000);
        VehicleType vehicle = VehicleType.CAR;

        OrderDto dto = new OrderDto();
        dto.setId(id);
        dto.setDate(date);
        dto.setTotalPrice(totalPrice);
        dto.setVehicle(vehicle);
        dto.setCustomer(createSampleCustomerDto());
        dto.setTire(createSampleTireDto());
        dto.setAdditionalServices(createSampleAdditionalServicesDto());

        Order entity = DtoConverter.convertToOrder(dto);
        assertNotNull(entity);
        assertEquals(id, entity.getId());
        assertEquals(date, entity.getDate());
        assertEquals(totalPrice, entity.getTotalPrice());
        assertEquals(vehicle, entity.getVehicle());
        DtoTestHelper.assertCustomer(entity.getCustomer(), createSampleCustomerDto());
        DtoTestHelper.assertTire(entity.getTire(), createSampleTireDto());
        DtoTestHelper.assertAdditionalServices(entity.getAdditionalServices(),
                createSampleAdditionalServicesDto());
    }

    private AdditionalServiceDto createAdditionalServiceDto(long id, String name, BigDecimal price) {
        AdditionalServiceDto dto = new AdditionalServiceDto();
        dto.setId(id);
        dto.setName(name);
        dto.setPrice(price);
        return dto;
    }
}
