/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.service.dtohelper;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.dto.OrderDto;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.entities.UserData;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides methods that convert dto object to entity.
 *
 * @author Radoslav Rabara
 */
public class DtoConverter {

    public static AdditionalService convertToAdditionalService(
            AdditionalServiceDto dto) {
        if (dto == null) {
            return null;
        }
        AdditionalService entity = new AdditionalService();
        entity.setId(dto.getId());
        entity.setName(dto.getName());
        entity.setPrice(dto.getPrice());
        return entity;
    }

    public static List<AdditionalService> convertToAdditionalServices(
            List<AdditionalServiceDto> dtos) {
        List<AdditionalService> parsed = new ArrayList<>();
        if (dtos != null) {
            for (AdditionalServiceDto dto : dtos) {
                parsed.add(convertToAdditionalService(dto));
            }
        }
        return parsed;
    }

    public static Customer convertToCustomer(CustomerDto dto) {
        if (dto == null) {
            return null;
        }
        Customer entity = new Customer();
        
        entity.setId(dto.getId());
        entity.setUsername(dto.getUsername());
        entity.setPassword(dto.getPassword());
        entity.setUserRole(dto.getUserRole());
        entity.setName(dto.getName());
        entity.setAddress(dto.getAddress());
        entity.setPhoneNumber(dto.getPhoneNumber());     
        entity.setVehicles(dto.getVehicles());    
        
        return entity;
    }

    public static Order convertToOrder(OrderDto dto) {
        if (dto == null) {
            return null;
        }
        Customer customer = convertToCustomer(dto.getCustomer());
        Tire tire = convertToTire(dto.getTire());
        List<AdditionalService> services = convertToAdditionalServices(dto.getAdditionalServices());
        
        Order entity = new Order();
        entity.setId(dto.getId());
        entity.setAdditionalServices(services);
        entity.setCustomer(customer);
        entity.setDate(dto.getDate());
        entity.setTire(tire);
        entity.setTotalPrice(dto.getTotalPrice());
        entity.setVehicle(dto.getVehicle());
        return entity;
    }

    public static Tire convertToTire(TireDto dto) {
        if (dto == null) {
            return null;
        }
        Tire entity = new Tire();
        entity.setId(dto.getId());
        entity.setType(dto.getType());
        entity.setSize(dto.getSize());
        entity.setManufacturer(dto.getManufacturer());
        entity.setPrice(dto.getPrice());     
        return entity;
    }
}
