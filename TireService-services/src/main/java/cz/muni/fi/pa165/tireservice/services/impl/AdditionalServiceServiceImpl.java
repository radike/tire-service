/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.AdditionalServiceDao;
import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoConverter;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.services.AdditionalServiceService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.util.List;
import javax.persistence.NoResultException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link AdditionalServiceService}.
 *
 * @author Martin Pruska
 * @see AdditionalServiceService
 */
@Service
@Transactional
public class AdditionalServiceServiceImpl implements AdditionalServiceService {

    @Autowired
    private AdditionalServiceDao dao;

    @Autowired
    private OrderDao orderDao;

    public void setAdditionalServiceDao(AdditionalServiceDao dao) {
        this.dao = dao;
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void createAdditionalService(AdditionalServiceDto service) throws ServiceFailureException {
        try {
            if (service.getName() != null && getAdditionalServiceByName(service.getName()) != null) {
                throw new IllegalArgumentException("There already is an additional service with the specified name");
            }
            AdditionalService entity = DtoConverter.convertToAdditionalService(service);
            dao.insert(entity);
            service.setId(entity.getId());
        } catch (Throwable e) {
            throw new ServiceFailureException("Inserting new service " + service, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateAdditionalService(AdditionalServiceDto service) throws ServiceFailureException {
        try {
            if (service.getId() > 0 && service.getName() != null) {
                AdditionalServiceDto fromDb = getAdditionalServiceByName(service.getName());
                if (fromDb != null && fromDb.getId() != service.getId()) {
                    throw new IllegalArgumentException("There already is an additional service with the specified name");
                }
            }
            dao.update(DtoConverter.convertToAdditionalService(service));
        } catch (Throwable e) {
            throw new ServiceFailureException("Updating service " + service, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeAdditionalService(AdditionalServiceDto service) throws ServiceFailureException {
        try {
            AdditionalService entity = dao.findById(service.getId());
            if (entity == null) {
                throw new IllegalArgumentException("AdditionalService was not found.");
            }
            if (orderDao.findByAdditionalService(entity).size() > 0) {
                throw new EntityHasOrderException("AdditionalService " + service + " is in relation with order");
            }
            dao.remove(entity);
        } catch (EntityHasOrderException e) {
            throw e;
        } catch (Throwable e) {
            throw new ServiceFailureException("Removing service " + service, e);
        }
    }

    @Override
    public List<AdditionalServiceDto> getAllAdditionalServices() throws ServiceFailureException {
        try {
            return DtoParser.parseAdditionalServices(dao.findAll());
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    public AdditionalServiceDto getAdditionalServiceByName(String name) throws ServiceFailureException {
        try {
            AdditionalService entity = dao.findByName(name);
            if (entity == null) {
                return null;
            }
            return DtoParser.parseAdditionalService(entity);
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (NoResultException e) {
            return null;
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    public AdditionalServiceDto getAdditionalServiceById(long id) throws ServiceFailureException {
        try {
            AdditionalService entity = dao.findById(id);
            if (entity == null) {
                return null;
            }
            return DtoParser.parseAdditionalService(entity);
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

}
