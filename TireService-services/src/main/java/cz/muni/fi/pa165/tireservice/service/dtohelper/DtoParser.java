/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.service.dtohelper;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.dto.OrderDto;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.entities.UserData;
import java.util.ArrayList;
import java.util.List;

/**
 * Provides parsing method that parse entity to dto object.
 *
 * @author Radoslav Rabara
 */
public class DtoParser {

    public static AdditionalServiceDto parseAdditionalService(
            AdditionalService entity) {
        if (entity == null) {
            return null;
        }
        AdditionalServiceDto dto = new AdditionalServiceDto();
        dto.setId(entity.getId());
        dto.setName(entity.getName());
        dto.setPrice(entity.getPrice());
        return dto;
    }

    public static List<AdditionalServiceDto> parseAdditionalServices(
            List<AdditionalService> entities) {
        List<AdditionalServiceDto> parsed = new ArrayList<>();
        if (entities != null) {
            for (AdditionalService entity : entities) {
                parsed.add(parseAdditionalService(entity));
            }
        }
        return parsed;
    }

    public static CustomerDto parseCustomer(Customer entity) {
        if (entity == null) {
            return null;
        }
        CustomerDto dto = new CustomerDto();

        dto.setId(entity.getId());
        dto.setUsername(entity.getUsername());
        dto.setPassword(entity.getPassword());
        dto.setUserRole(entity.getUserRole());
        dto.setName(entity.getName());
        dto.setAddress(entity.getAddress());
        dto.setPhoneNumber(entity.getPhoneNumber());
        dto.setVehicles(entity.getVehicles());

        return dto;
    }

    public static OrderDto parseOrder(Order entity) {
        if (entity == null) {
            return null;
        }
        CustomerDto customer = parseCustomer(entity.getCustomer());
        TireDto tire = parseTire(entity.getTire());
        List<AdditionalServiceDto> services = new ArrayList<>();
        if (entity.getAdditionalServices() != null && entity.getAdditionalServices().size() != 0) {
            services = parseAdditionalServices(
                    entity.getAdditionalServices());
        }

        OrderDto dto = new OrderDto();
        dto.setId(entity.getId());
        dto.setDate(entity.getDate());
        dto.setCustomer(customer);
        dto.setTire(tire);
        dto.setAdditionalServices(services);
        dto.setTotalPrice(entity.getTotalPrice());
        dto.setVehicle(entity.getVehicle());
        return dto;
    }

    public static TireDto parseTire(Tire entity) {
        if (entity == null) {
            return null;
        }
        TireDto dto = new TireDto();
        dto.setId(entity.getId());
        dto.setSize(entity.getSize());
        dto.setType(entity.getType());
        dto.setManufacturer(entity.getManufacturer());
        dto.setPrice(entity.getPrice());
        return dto;
    }
}
