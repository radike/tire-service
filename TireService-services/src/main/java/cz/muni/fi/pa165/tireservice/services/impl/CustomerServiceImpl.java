/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.CustomerDao;
import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoConverter;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.enums.UserRole;
import cz.muni.fi.pa165.tireservice.security.SecurityHelper;
import cz.muni.fi.pa165.tireservice.services.CustomerService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

/**
 *
 * @author Marek
 */
@Service
@Transactional
public class CustomerServiceImpl implements CustomerService, UserDetailsService {

    @Autowired
    private CustomerDao dao;
    
    @Autowired
    private OrderDao orderDao;

    @Autowired
    private PasswordEncoder encoder;
    
    public void setCustomerDao(CustomerDao dao) {
        this.dao = dao;
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }
 
    public void setEncoder(PasswordEncoder encoder) {
        this.encoder = encoder;
    }
    
    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void createCustomer(CustomerDto dto) {
        try {
            Customer entity = DtoConverter.convertToCustomer(dto);
            if (entity.getPassword() != null && entity.getPassword().length() > 0) {
                entity.setPassword(encoder.encode(entity.getPassword()));
            }
            dao.insert(entity);
            dto.setId(entity.getId());
        } catch (Throwable e) {
            throw new ServiceFailureException("Inserting new customer " + dto, e);
        }

    }

    @Override
    public void updateCustomer(CustomerDto customer) {
        try {
            Customer entity = DtoConverter.convertToCustomer(customer);
            if (entity.getPassword() != null && entity.getPassword().length() > 0) {
                entity.setPassword(encoder.encode(entity.getPassword()));
            }
            dao.update(entity);
        } catch (Throwable e) {
            throw new ServiceFailureException("Updating customer " + customer, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeCustomer(CustomerDto dto) {
        try {
            Customer entity = dao.findById(dto.getId());
            if (entity == null) {
                throw new IllegalArgumentException("Customer was not found.");
            }
            if (orderDao.findByCustomer(entity).size() > 0) {
                throw new EntityHasOrderException("Customer " + dto + " is in relation with order");
            }
            dao.remove(entity);
        } catch (EntityHasOrderException e) {
            throw e;
        } catch (Throwable e) {
            throw new ServiceFailureException("Removing customer " + dto, e);
        }
    }

    @Override
    public CustomerDto getCustomerById(long id) {
        try {
            Customer entity = dao.findById(id);
            if (entity == null) {
                return null;
            }
            CustomerDto customer = DtoParser.parseCustomer(entity);
            if (!loggedUserCanAccessToCustomer(customer)) {
                throw new IllegalArgumentException(
                            "User '" + SecurityHelper.getLoggedUserUsername()
                                    + "' can't access to '"
                                    + customer.getUsername()
                                    + "' with id " + id);
            }
            return customer;
        } catch (EmptyResultDataAccessException e) {
            return null;
        } catch (NoResultException e) {
            return null;
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    public CustomerDto getCustomerByUsername(String username) {
        try {
            CustomerDto customer = DtoParser.parseCustomer(dao.findByUsername(username));
            if (!loggedUserCanAccessToCustomer(customer)) {
                throw new IllegalArgumentException(
                            "User '" + SecurityHelper.getLoggedUserUsername()
                                    + "' can't access to customer '"
                                    + username
                                    + "' with id " + customer.getId());
            }
            return customer;            
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<CustomerDto> getAllCustomers() {
        try {
            return convertCustomers(dao.findAll());
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    private List<CustomerDto> convertCustomers(List<Customer> customers) {
        List<CustomerDto> converted = new ArrayList<CustomerDto>();
        for (Customer customer : customers) {
            converted.add(DtoParser.parseCustomer(customer));
        }
        return converted;
    }
    
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            if (dao.getCountOfCustomers() == 0) {
                createDefaultAdminUser();
            }
            Customer user = dao.findByUsername(username);
            return new org.springframework.security.core.userdetails.User(user.getUsername(),
                    user.getPassword(), true, true, true, true,
                    Arrays.asList(new SimpleGrantedAuthority(user.getUserRole().toString())));
        } catch (Throwable e) {
            e.printStackTrace();
            throw new UsernameNotFoundException("Username '" + username + "' was not found.", e);
        }
    }
    
    private boolean loggedUserCanAccessToCustomer(CustomerDto customer) {
        if (!SecurityHelper.loggedUserIsAdmin()) {
            if (!SecurityHelper.getLoggedUserUsername().equals(customer.getUsername())) {
                return false;
            }
        }
        return true;
    } 

    private void createDefaultAdminUser() {
        CustomerDto defaultAdmin = new CustomerDto();
        defaultAdmin.setUsername("admin");
        defaultAdmin.setPassword("admin");
        defaultAdmin.setUserRole(UserRole.ROLE_ADMIN);
        defaultAdmin.setName("Default admin user");
        defaultAdmin.setAddress("-- address --");
        defaultAdmin.setPhoneNumber("000000000");
        createCustomer(defaultAdmin);
    }
}
