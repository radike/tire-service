/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoConverter;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser;
import cz.muni.fi.pa165.tireservice.dto.OrderDto;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.security.SecurityHelper;
import cz.muni.fi.pa165.tireservice.services.OrderService;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link OrderService}.
 *
 * @author Radoslav Rabara
 * @see OrderService
 */
@Service
@Transactional
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao dao;

    public void setOrderDao(OrderDao dao) {
        this.dao = dao;
    }

    @Override
    public void createOrder(OrderDto dto) {
        try {
            if (!loggedUserCanAccessToOrder(dto)) {
                throw new IllegalArgumentException(
                            "User '" + SecurityHelper.getLoggedUserUsername()
                                    + "' can't create new order as user '"
                                    + dto.getCustomer().getUsername() + "'");
            }
            // total price
            BigDecimal totalPrice = calculateOrderTotalPrice(dto);
            if (dto.getTotalPrice() != null && !totalPrice.equals(dto.getTotalPrice())) {
                throw new ServiceFailureException(
                        new IllegalArgumentException("total price is not correct. "
                                + "Expected was " + dto.getTotalPrice()
                                + " but there is " + totalPrice));
            }
            Order entity = DtoConverter.convertToOrder(dto);
            entity.setTotalPrice(totalPrice);

            // date
            if (entity.getDate() == null) {
                entity.setDate(new Date());
            }
            dao.insert(entity);
            dto.setId(entity.getId());
        } catch (Throwable e) {
            throw new ServiceFailureException("Inserting new order " + dto, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateOrder(OrderDto order) {
        try {
            BigDecimal totalPrice = calculateOrderTotalPrice(order);
            if (order.getTotalPrice() == null || !order.getTotalPrice().equals(totalPrice)) {
                order.setTotalPrice(totalPrice);
            }
            Order entity = DtoConverter.convertToOrder(order);
            if (entity.getDate() == null) {
                Order fromDb = dao.findById(entity.getId());
                entity.setDate(fromDb.getDate());
            }
            dao.update(entity);
        } catch (Throwable e) {
            throw new ServiceFailureException("Updating order " + order, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeOrder(OrderDto order) {
        try {
            dao.remove(DtoConverter.convertToOrder(order));
        } catch (Throwable e) {
            throw new ServiceFailureException("Removing order " + order, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<OrderDto> getAllOrders() {
        try {
            return convertOrders(dao.findAll());
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    public OrderDto getOrderById(long id) {
        try {
            OrderDto order =  DtoParser.parseOrder(dao.findById(id));
            if (!loggedUserCanAccessToOrder(order)) {
                throw new IllegalArgumentException(
                            "User '" + SecurityHelper.getLoggedUserUsername()
                                    + "' can't access to '"
                                    + order.getCustomer().getUsername()
                                    + "' order with id " + id);
            }
            return order;
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    public List<OrderDto> getOrdersByCustomer(CustomerDto customer) {
        try {
            if (!SecurityHelper.loggedUserIsAdmin()) {
                if (!SecurityHelper.getLoggedUserUsername().equals(customer.getUsername())) {
                    throw new IllegalArgumentException(
                            "User '" + SecurityHelper.getLoggedUserUsername()
                                    + "' can't access to '"
                                    + customer.getUsername() + "' orders");
                }
            }
            return convertOrders(dao.findByCustomer(
                    DtoConverter.convertToCustomer(customer)));
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<OrderDto> getOrdersByTotalPrice(BigDecimal totalPrice) {
        try {
            return convertOrders(dao.findByTotalPrice(totalPrice));
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public List<OrderDto> getOrdersByDate(Date from, Date to) {
        return convertOrders(dao.findByDate(from, to));
    }

    private List<OrderDto> convertOrders(List<Order> orders) {
        List<OrderDto> converted = new ArrayList<OrderDto>();
        for (Order order : orders) {
            converted.add(DtoParser.parseOrder(order));
        }
        return converted;
    }

    @Override
    public BigDecimal calculateOrderTotalPrice(OrderDto order) {
        BigDecimal totalPrice = order.getTire().getPrice();
        if (totalPrice == null) {
            throw new ServiceFailureException(
                    new IllegalAccessException("tire has null as price"));
        }
        if (order.getAdditionalServices() != null) {
            for (AdditionalServiceDto service : order.getAdditionalServices()) {
                if (service.getPrice() == null) {
                    throw new ServiceFailureException(
                            new IllegalAccessException("additional service has null as price"));
                }
                totalPrice = totalPrice.add(service.getPrice());
            }
        }
        return totalPrice;
    }
    
    private boolean loggedUserCanAccessToOrder(OrderDto order) {
        if (!SecurityHelper.loggedUserIsAdmin()) {
                if (!SecurityHelper.getLoggedUserUsername().equals(order.getCustomer().getUsername())) {
                    return false;
                }
            }
        return true;
    }
}
