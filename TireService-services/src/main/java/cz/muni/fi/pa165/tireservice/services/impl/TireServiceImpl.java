/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.impl;

import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dao.TireDao;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoConverter;
import cz.muni.fi.pa165.tireservice.service.dtohelper.DtoParser;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import cz.muni.fi.pa165.tireservice.services.TireService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementation of {@link TireService}.
 *
 * @author Samuel Briškár
 * @see TireService
 */
@Service
@Transactional
public class TireServiceImpl implements TireService {

    @Autowired
    private TireDao dao;

    @Autowired
    private OrderDao orderDao;

    public void setTireDao(TireDao dao) {
        this.dao = dao;
    }

    public void setOrderDao(OrderDao orderDao) {
        this.orderDao = orderDao;
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void createTire(TireDto dto) {
        try {
            Tire tire = DtoConverter.convertToTire(dto);
            dao.insert(tire);
            dto.setId(tire.getId());
        } catch (Throwable e) {
            throw new ServiceFailureException("Inserting new tire " + dto, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void updateTire(TireDto tire) {
        try {
            dao.update(DtoConverter.convertToTire(tire));
        } catch (Throwable e) {
            throw new ServiceFailureException("Updating tire " + tire, e);
        }
    }

    @Override
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void removeTire(TireDto tire) {
        try {
            Tire entity = dao.findById(tire.getId());
            if (entity == null) {
                throw new IllegalArgumentException("Tire was not found.");
            }
            if (orderDao.findByTire(entity).size() > 0) {
                throw new EntityHasOrderException("Tire " + tire + " is in relation with order");
            }
            dao.remove(entity);
        } catch (EntityHasOrderException e) {
            throw e;
        } catch (Throwable e) {
            throw new ServiceFailureException("Removing tire " + tire, e);
        }
    }

    @Override
    public List<TireDto> getAllTires() {
        try {
            return convertTires(dao.findAll());
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }

    private List<TireDto> convertTires(List<Tire> tires) {
        List<TireDto> converted = new ArrayList<TireDto>();
        for (Tire tire : tires) {
            converted.add(DtoParser.parseTire(tire));
        }
        return converted;
    }

    @Override
    public TireDto getTireById(long id) {
        try {
            return DtoParser.parseTire(dao.findById(id));
        } catch (Throwable e) {
            throw new ServiceFailureException(e);
        }
    }
}
