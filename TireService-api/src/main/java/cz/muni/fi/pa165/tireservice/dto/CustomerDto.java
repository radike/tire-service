/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dto;

import cz.muni.fi.pa165.tireservice.enums.UserRole;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import java.util.List;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.springframework.security.access.prepost.PreAuthorize;

/**
 *
 * @author Marek Halas
 */
public class CustomerDto {

    private long id;

    @NotNull
    @Size(min = 1, max = 45)
    private String username;

    @NotNull
    @Size(min = 1, max = 60)
    private String password;

    @NotNull
    private UserRole userRole = UserRole.ROLE_USER;

    @NotNull
    @Size(min = 1)
    private String name;

    @NotNull
    @Size(min = 1)
    private String address;

    @NotNull
    @Size(min = 1)
    @Pattern(regexp = "^[+]?[()/0-9. -]{9,}$")
    private String phoneNumber;

    private List<VehicleType> vehicles;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public List<VehicleType> getVehicles() {
        return vehicles;
    }

    public void setVehicles(List<VehicleType> vehicles) {
        this.vehicles = vehicles;
    }

}
