/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services;

import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.dto.OrderDto;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Order service<br/>
 * Provides operations with order.
 *
 * @author Radoslav Rabara
 */
public interface OrderService {

    /**
     * Creates Order.
     * If the order has not set total price (is null), than total price
     * is set.
     * If the order has not set date (is null), than the current date is used.
     * 
     * @param order order
     * @throws ServiceFailureException if creating of order has failed or
     * if the total price is set and is not equal to price of tire plus
     * sum of additional services price
     */
    void createOrder(OrderDto order);

    /**
     * Updates the specified <var>order</var>.
     * If the order has not set total price (is null), than total price
     * is set.
     * Date is updated only if it is set.
     * 
     * @param order order to be updated
     * @throws ServiceFailureException if updating of order has failed
     */
    void updateOrder(OrderDto order);

    /**
     * Removes the specified <var>order</var>.
     *
     * @param order order to be removed
     * @throws ServiceFailureException if removing of order has failed
     */
    void removeOrder(OrderDto order);

    /**
     * Returns all orders.
     *
     * @return {@link List} of all orders
     * @throws ServiceFailureException if retrieving of all orders have failed
     */
    List<OrderDto> getAllOrders();

    /**
     * Returns orders, which were created by the specified <var>customer</var>
     *
     * @param customer customer, whose orders will be returned
     * @return {@link List} of orders created by the specified customer
     * @throws ServiceFailureException if retrieving of orders have failed
     */
    List<OrderDto> getOrdersByCustomer(CustomerDto customer);

    /**
     * Returns orders, which have the specified total price
     *
     * @param totalPrice total price of the orders, which will be returned
     * @return {@link List} of orders with the specified total price
     * @throws ServiceFailureException if retrieving of orders have failed
     */
    List<OrderDto> getOrdersByTotalPrice(BigDecimal totalPrice);

    /**
     * Returns orders that were created between <from, to>.
     *
     * @param from start of the filter interval (takes day, month, year and
     * ignores time
     * @param to end of the filter interval (the same as <var>from</var>)
     * @return {@link List} of orders with date of creation between
     * <var>from</var> and <var>to</var>
     * @throws ServiceFailureException if retrieving of orders have failed
     */
    List<OrderDto> getOrdersByDate(Date from, Date to);

    public OrderDto getOrderById(long id);

    public BigDecimal calculateOrderTotalPrice(OrderDto order);
}
