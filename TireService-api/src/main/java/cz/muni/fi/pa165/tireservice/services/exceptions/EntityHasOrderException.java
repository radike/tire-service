/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.exceptions;

/**
 * Specifies that removing of entity failed because the entity
 * ({@link AdditionalService}, {@link Customer} or {@link Tire})
 * is in relation with a {@link Order}.
 * 
 * @author Radoslav Rabara
 */
public class EntityHasOrderException extends ServiceFailureException {
    public EntityHasOrderException(String message) {
        super(message);
    }
}
