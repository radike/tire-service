/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.security;

import cz.muni.fi.pa165.tireservice.enums.UserRole;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;

/**
 *
 * @author Radoslav Rabara
 */
public class SecurityHelper {
    public static boolean loggedUserIsAdmin() {
         User user = getLoggedUser();
         for (GrantedAuthority auth : user.getAuthorities()) {
             System.err.println("auth " + auth.getAuthority() + " " + UserRole.ROLE_ADMIN);
             if (auth.getAuthority().equals(UserRole.ROLE_ADMIN.toString())) {
                 return true;
             }
         }
         return false;
    }
    
    public static User getLoggedUser() {
        User user = (User)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (user == null) {
            throw new IllegalStateException("logged user is null");
        }
        return user;
    }
    
    public static String getLoggedUserUsername() {
        return getLoggedUser().getUsername();
    }
}
