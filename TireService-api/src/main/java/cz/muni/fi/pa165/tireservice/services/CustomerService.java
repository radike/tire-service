/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services;

import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import java.util.List;

/**
 *
 * @author Marek Halas
 */
public interface CustomerService {

    /**
     * Creates new <var>customer</var>.
     *
     * @param customer customer to be created
     * @throws ServiceFailureException if creating of customer has failed
     */
    void createCustomer(CustomerDto customer);

    /**
     * Updates the given <var>customer</var>.
     *
     * @param customer customer to be updated
     * @throws ServiceFailureException if updating of customer has failed
     */
    void updateCustomer(CustomerDto customer);

    /**
     * Removes the given <var>customer</var>.
     *
     * @param customer customer to be removed
     * @throws ServiceFailureException if removing of customer has failed
     */
    void removeCustomer(CustomerDto customer);

    /**
     * Retrieves {@link CustomerDto} of the specified <var>id</var>.
     *
     * @param id ID of {@link CustomerDto} to retrieve
     * @return {@link CustomerDto} if there is one of specified name in
     * db, otherwise returns <var>null</var>.
     * @throws ServiceFailureException if retrieving of a customer has failed
     */
    CustomerDto getCustomerById(long id);
    
    /**
     * Retrieves {@link CustomerDto} with the specified <var>username</var>.
     *
     * @param id username of {@link CustomerDto} to retrieve
     * @return {@link CustomerDto} if there is one of specified name in
     * db, otherwise returns <var>null</var>.
     * @throws ServiceFailureException if retrieving of a customer has failed
     */
    CustomerDto getCustomerByUsername(String username);
    
    /**
     * Retrieves all {@link CustomerDto}s.
     *
     * @return list of {@link CustomerDto}s
     * @throws ServiceFailureException if retrieving of customers has failed
     */
    List<CustomerDto> getAllCustomers();
}
