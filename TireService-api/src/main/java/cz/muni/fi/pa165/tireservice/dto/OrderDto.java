/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dto;

import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 *
 * @author Radoslav Rabara
 */
public class OrderDto {

    private long id;
    private CustomerDto customer;
    private TireDto tire;
    private List<AdditionalServiceDto> additionalServices;
    private BigDecimal totalPrice;
    private Date date;
    private VehicleType vehicle;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public CustomerDto getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerDto customer) {
        this.customer = customer;
    }

    public TireDto getTire() {
        return tire;
    }

    public void setTire(TireDto tire) {
        this.tire = tire;
    }

    public List<AdditionalServiceDto> getAdditionalServices() {
        return additionalServices;
    }

    public void setAdditionalServices(List<AdditionalServiceDto> services) {
        this.additionalServices = services;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public VehicleType getVehicle() {
        return vehicle;
    }

    public void setVehicle(VehicleType vehicle) {
        this.vehicle = vehicle;
    }

    @Override
    public String toString() {
        return "OrderDto{" + "id=" + id + ", customer=" + customer + ", tire=" + tire + ", additionalServices=" + additionalServices + ", totalPrice=" + totalPrice + ", date=" + date + ", vehicle=" + vehicle + '}';
    }

}
