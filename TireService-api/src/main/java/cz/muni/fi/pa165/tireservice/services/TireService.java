/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services;

import cz.muni.fi.pa165.tireservice.dto.TireDto;
import java.util.List;

/**
 * Tire service<br/>
 * Provides operations with tire.
 *
 * @author Samuel Briškár
 */
public interface TireService {

    /**
     * Creates new <var>tire</var>.
     *
     * @param tire tire to be created
     * @throws ServiceFailureException if creating of tire has failed
     */
    void createTire(TireDto tire);

    /**
     * Updates the given <var>tire</var>.
     *
     * @param tire tire to be updated
     * @throws ServiceFailureException if updating of tire has failed
     */
    void updateTire(TireDto tire);

    /**
     * Removes the given <var>tire</var>.
     *
     * @param tire tire to be removed
     * @throws ServiceFailureException if removing of tire has failed
     */
    void removeTire(TireDto tire);

    /**
     * Retrieves all {@link TireDto}s.
     *
     * @return list of {@link TireDto}s
     * @throws ServiceFailureException if retrieving of tires has failed
     */
    List<TireDto> getAllTires();

    public TireDto getTireById(long id);

}
