/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services.exceptions;

import org.springframework.dao.DataAccessException;

/**
 * Exception thrown when a service fails.
 *
 * @author Radoslav Rabara
 */
public class ServiceFailureException extends DataAccessException {

    public ServiceFailureException(String message) {
        super(message);
    }

    public ServiceFailureException(Throwable cause) {
        super("Exception while executing service", cause);
    }

    public ServiceFailureException(String message, Throwable cause) {
        super(message, cause);
    }

}
