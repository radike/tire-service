/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.services;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.util.List;

/**
 * AdditionalService service<br/>
 * Provides operations with additional service.
 *
 * @author Martin Pruska
 */
public interface AdditionalServiceService {

    /**
     * Creates <var>additional service</var>.
     *
     * @param service additional service
     * @throws ServiceFailureException if creating of service failed.
     */
    void createAdditionalService(AdditionalServiceDto service) throws ServiceFailureException;

    /**
     * Updates <var>additional service</var>.
     *
     * @param service Additional service to be updated.
     * @throws ServiceFailureException if updating of service failed.
     */
    void updateAdditionalService(AdditionalServiceDto service) throws ServiceFailureException;

    /**
     * Removes <var>additional service</var>.
     *
     * @param service Additional service to be removed.
     * @throws ServiceFailureException if removing of service failed.
     */
    void removeAdditionalService(AdditionalServiceDto service) throws ServiceFailureException;

    /**
     * Retrieves all {@link AdditionalServiceDto}s.
     *
     * @return list of {@link AdditionalServiceDto}s
     * @throws ServiceFailureException if retrieving of services failed.
     */
    List<AdditionalServiceDto> getAllAdditionalServices() throws ServiceFailureException;

    /**
     * Retrieves {@link AdditionalServiceDto} of specified name.
     *
     * @param name Name of {@link AdditionalServiceDto} to retrieve.
     * @return {@link AdditionalServiceDto} if there is one of specified name in
     * db, otherwise returns <var>null</var>.
     * @throws ServiceFailureException if retrieving of service failed.
     */
    AdditionalServiceDto getAdditionalServiceByName(String name) throws ServiceFailureException;

    /**
     * Retrieves {@link AdditionalServiceDto} of specified ID.
     *
     * @param id ID of {@link AdditionalServiceDto} to retrieve.
     * @return {@link AdditionalServiceDto} if there is one of specified id in
     * db, otherwise returns <var>null</var>.
     * @throws ServiceFailureException if retrieving of a service failed.
     */
    AdditionalServiceDto getAdditionalServiceById(long id) throws ServiceFailureException;
}
