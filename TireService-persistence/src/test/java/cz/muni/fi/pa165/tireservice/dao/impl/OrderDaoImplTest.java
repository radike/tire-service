/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.dao.AdditionalServiceDao;
import cz.muni.fi.pa165.tireservice.dao.CustomerDao;
import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.dao.TireDao;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.enums.TireType;
import cz.muni.fi.pa165.tireservice.enums.UserRole;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Test of {@link OrderDaoImpl}
 *
 * @author Samuel Briškár
 */
public class OrderDaoImplTest extends AbstractIntegrationTest {

    @Autowired
    private OrderDao orderDao;
    @Autowired
    private CustomerDao customerDao;
    @Autowired
    private TireDao tireDao;
    @Autowired
    private AdditionalServiceDao servicesDao;

    @Test
    public void insertOrder() {
        Order order = createSampleOrder1();
        orderDao.insert(order);
        assertTrue(order.getId() != 0);
    }

    @Test
    public void controlOrder() {
        Order order = createSampleOrder1();
        orderDao.insert(order);

        List<Order> orders = orderDao.findAll();
        assertEquals(1, orders.size());

        order = orders.get(0);
        assertEquals(VehicleType.TRUCK, order.getVehicle());
        assertEquals(new BigDecimal(1.0), order.getTotalPrice());
        assertTrue(order.getId() != 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertNull() {
        orderDao.insert(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertOrderWithAssignedId() {
        Order order = new Order();
        order.setId(1);
        order.setTotalPrice(new BigDecimal(1.0));
        orderDao.insert(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertOrderWithoutAssignedCustomer() {
        Order order = createSampleOrder1();
        order.setCustomer(null);
        orderDao.insert(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertOrderWithoutAssignedTire() {
        Order order = createSampleOrder1();
        order.setTire(null);
        orderDao.insert(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertOrderWithoutAssignedDate() {
        Order order = createSampleOrder1();
        order.setDate(null);
        orderDao.insert(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertOrderWithoutAssignedTotalPrice() {
        Order order = createSampleOrder1();
        order.setTotalPrice(null);
        orderDao.insert(order);
    }

    @Test
    public void findAll() {
        List<Order> orders = orderDao.findAll();
        assertNotNull(orders);
        assertEquals(orders.size(), 0);

        Order order1 = createSampleOrder1();
        orderDao.insert(order1);

        orders = orderDao.findAll();
        assertNotNull(orders);
        assertEquals(orders.size(), 1);
        assertEquals("Expected was list with " + order1 + " but there is "
                + orders.get(0), orders, Arrays.asList(order1));

        Order order2 = createSampleOrder2();
        orderDao.insert(order2);

        orders = orderDao.findAll();
        assertNotNull(orders);
        assertEquals(orders.size(), 2);
        assertTrue("Expected was list containing " + order1 + " but there is "
                + Arrays.toString(orders.toArray()), orders.contains(order1));
        assertTrue("Expected was list containing " + order2 + " but there is "
                + Arrays.toString(orders.toArray()), orders.contains(order2));
    }

    @Test
    public void update() {
        Order order1 = createSampleOrder1();
        Order order2 = createSampleOrder2();

        orderDao.insert(order1);
        orderDao.insert(order2);

        orderDao.update(order1);
        List<Order> orders = orderDao.findAll();
        assertNotNull(orders);
        assertEquals(orders.size(), 2);
        assertTrue("Expected was list containing " + order1 + " but there is "
                + Arrays.toString(orders.toArray()), orders.contains(order1));
        assertTrue("Expected was list containing " + order2 + " but there is "
                + Arrays.toString(orders.toArray()), orders.contains(order2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNull() {
        orderDao.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateCustomerWithoutAssignedId() {
        Order order = new Order();
        orderDao.update(order);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateCustomerWithoutCustomer() {
        Order order = new Order();
        order.setCustomer(null);
        orderDao.update(order);
    }

    @Test
    public void remove() {
        Order order1 = createSampleOrder1();
        Order order2 = createSampleOrder2();

        orderDao.insert(order1);
        orderDao.insert(order2);

        orderDao.remove(order1);
        List<Order> orders = orderDao.findAll();
        assertNotNull(orders);
        assertEquals(orders.size(), 1);
        assertTrue("Expected was list containing " + order2 + " but there is "
                + Arrays.toString(orders.toArray()), orders.contains(order2));
    }
    
    @Test
    public void removeOrderKeepCustomerTireAndAdditionalService() {
        Customer customer = new Customer();
        customer.setUsername("username");
        customer.setPassword("password");
        customer.setUserRole(UserRole.ROLE_USER);
        customer.setName("Anna");
        customer.setAddress("Buckingham");
        customer.setPhoneNumber("0078900123");
        
        Tire tire = new Tire();
        tire.setType(TireType.ROAD);
        tire.setSize(42);
        tire.setManufacturer("Matador");
        tire.setPrice(new BigDecimal(99));
        
        AdditionalService service = new AdditionalService();
        service.setName("Cleaning service");
        service.setPrice(new BigDecimal(9.90));
        
        Order order = new Order();
        order.setTotalPrice(new BigDecimal(12.3));
        order.setVehicle(VehicleType.CAR);
        order.setDate(new Date());
        order.setCustomer(customer);
        order.setTire(tire);
        order.setAdditionalServices(Arrays.asList(service));

        customerDao.insert(customer);
        tireDao.insert(tire);
        servicesDao.insert(service);
        orderDao.insert(order);

        
        assertNotNull(orderDao.findById(order.getId()));
        
        assertNotNull(customerDao.findById(customer.getId()));
        assertNotNull(tireDao.findById(tire.getId()));
        assertNotNull(servicesDao.findById(service.getId()));
        
        orderDao.remove(order);
        
        try {
            orderDao.findById(order.getId());
            throw new AssertionError("Order " + order + " should not be in db!");
        } catch(IllegalArgumentException e) {
            // ok
        }
        
        assertNotNull(customerDao.findById(customer.getId()));
        assertNotNull(tireDao.findById(tire.getId()));
        assertNotNull(servicesDao.findById(service.getId()));
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNull() {
        orderDao.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeOrderWithoutAssignedId() {
        Order order = new Order();
        orderDao.remove(order);
    }

    private Order createSampleOrder1() {
        Order order1 = new Order();

        order1.setTotalPrice(new BigDecimal(1.0));
        order1.setVehicle(VehicleType.TRUCK);
        order1.setDate(new Date());
        order1.setCustomer(createSampleCustomer("customer1"));
        order1.setTire(createSampleTire());

        return order1;
    }

    private Order createSampleOrder2() {
        Order order2 = new Order();

        order2.setTotalPrice(new BigDecimal(2.0));
        order2.setVehicle(VehicleType.CAR);
        order2.setDate(new Date());
        order2.setCustomer(createSampleCustomer("customer2"));
        order2.setTire(createSampleTire());
        order2.setAdditionalServices(createSampleAdditionalServices());
        return order2;
    }

    private Customer createSampleCustomer(String username) {
        Customer customer = new Customer();
        customer.setUsername(username);
        customer.setPassword("password");
        customer.setUserRole(UserRole.ROLE_USER);
        customer.setName("John Doe");
        customer.setAddress("Doeville 316");
        customer.setPhoneNumber("00 420 720 512 632");

        customerDao.insert(customer);
        return customer;
    }
    
    private Tire createSampleTire() {
        Tire tire = new Tire();
        tire.setType(TireType.ROAD);
        tire.setSize(54);
        tire.setManufacturer("TireManufacturer");
        tire.setPrice(new BigDecimal(123));

        tireDao.insert(tire);
        return tire;
    }

    private List<AdditionalService> createSampleAdditionalServices() {
        AdditionalService service1 = new AdditionalService();
        service1.setName("service1");
        service1.setPrice(new BigDecimal(1));

        AdditionalService service2 = new AdditionalService();
        service2.setName("service2");
        service2.setPrice(new BigDecimal(2));

        servicesDao.insert(service1);
        servicesDao.insert(service2);
        return Arrays.asList(service1, service2);
    }
}
