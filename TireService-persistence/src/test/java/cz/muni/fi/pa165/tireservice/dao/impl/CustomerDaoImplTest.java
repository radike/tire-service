package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.enums.UserRole;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * Test of {@link CustomerDaoImpl}
 *
 * @author Radoslav Rabara
 */
public class CustomerDaoImplTest extends AbstractIntegrationTest {

    @Autowired
    private CustomerDaoImpl dao;

    @Test
    public void insertCustomer() {
        Customer customer = new Customer();

        final String name = "Jozo";
        final String address = "sk";
        final String phoneNumber = "123";

        customer.setUsername("username");
        customer.setPassword("password");
        customer.setUserRole(UserRole.ROLE_USER);
        customer.setName(name);
        customer.setAddress(address);
        customer.setPhoneNumber(phoneNumber);

        dao.insert(customer);
        assertTrue(customer.getId() != 0);
        assertEquals(customer.getName(), name);
        assertEquals(customer.getAddress(), address);
        assertEquals(customer.getPhoneNumber(), phoneNumber);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertNull() {
        dao.insert(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertCustomerWithAssignedId() {
        Customer customer = new Customer();
        customer.setId(1);
        dao.insert(customer);
    }

    public void findAll() {
        List<Customer> customers = dao.findAll();
        assertNotNull(customers);
        assertEquals("There are no customers in db, so list of customers "
                + "should be empty", customers.size(), 0);

        Customer customer1 = new Customer();

        customer1.setUsername("username1");
        customer1.setPassword("password");
        customer1.setUserRole(UserRole.ROLE_USER);
        customer1.setName("Jeremy Clarkson");
        customer1.setAddress("Notingham 42");
        customer1.setPhoneNumber("44-0207-123-4567");

        dao.insert(customer1);

        customers = dao.findAll();
        assertNotNull(customers);
        assertEquals("There should be exactly 1 customer", customers.size(), 1);
        assertEquals("Expected was list with " + customer1 + " but there is "
                + customers.get(0), customers, Arrays.asList(customer1));

        Customer customer2 = new Customer();

        customer2.setUsername("username2");
        customer2.setPassword("password");
        customer2.setUserRole(UserRole.ROLE_USER);
        customer2.setName("James Bond");
        customer2.setAddress("Londom, Main Street 5912");
        customer2.setPhoneNumber("007");

        dao.insert(customer2);

        customers = dao.findAll();
        assertNotNull(customers);
        assertEquals("There should be exactly 2 customers", customers.size(), 2);
        assertTrue("Expected was list containing " + customer1 + " but there is "
                + Arrays.toString(customers.toArray()), customers.contains(customer1));
        assertTrue("Expected was list containing " + customer2 + " but there is "
                + Arrays.toString(customers.toArray()), customers.contains(customer2));
    }

    @Test
    public void findCustomerByUsername() {
        Customer customer = new Customer();

        final String name = "Jozo";
        final String address = "sk";
        final String phoneNumber = "123";

        customer.setUsername("username");
        customer.setPassword("password");
        customer.setUserRole(UserRole.ROLE_USER);
        customer.setName(name);
        customer.setAddress(address);
        customer.setPhoneNumber(phoneNumber);

        dao.insert(customer);

        Customer found = dao.findByUsername("username");
        assertNotNull(found);
    }

    @Test(expected = javax.persistence.NoResultException.class)
    public void findNonExistingCustomerByUsername() {
        dao.findByUsername("username");
    }

    @Test
    public void findById() {
        Customer customer = new Customer();

        customer.setUsername("username");
        customer.setPassword("password");
        customer.setUserRole(UserRole.ROLE_USER);
        customer.setName("John Doe");
        customer.setAddress("Doeland 12");
        customer.setPhoneNumber("0000");

        dao.insert(customer);

        Customer foundCustomer = dao.findById(customer.getId());
        assertNotNull(foundCustomer);
        assertEquals(customer, foundCustomer);
    }

    @Test(expected = javax.persistence.NoResultException.class)
    public void findByNonExistingId() {
        dao.findById(1);
    }

    @Test
    public void update() {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer();

        customer1.setUsername("username1");
        customer1.setPassword("password");
        customer1.setUserRole(UserRole.ROLE_USER);
        customer1.setName("Barack Obama");
        customer1.setAddress("White House, Washington DC");
        customer1.setPhoneNumber("911");

        customer2.setUsername("username2");
        customer2.setPassword("password");
        customer2.setUserRole(UserRole.ROLE_USER);
        customer2.setName("Jano Jansky");
        customer2.setAddress("Some address");
        customer2.setPhoneNumber("321321321");

        dao.insert(customer1);
        dao.insert(customer2);

        customer1.setName("Josif Jozefovic");
        customer1.setAddress("Moskow");
        customer1.setPhoneNumber("987");

        dao.update(customer1);
        List<Customer> customers = dao.findAll();
        assertNotNull(customers);
        assertEquals("There should be exactly 2 customers", customers.size(), 2);
        assertTrue("Expected was list containing " + customer1 + " but there is "
                + Arrays.toString(customers.toArray()), customers.contains(customer1));
        assertTrue("Expected was list containing " + customer2 + " but there is "
                + Arrays.toString(customers.toArray()), customers.contains(customer2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNull() {
        dao.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateCustomerWithoutAssignedId() {
        Customer customer = new Customer();
        dao.update(customer);
    }

    @Test
    public void remove() {
        Customer customer1 = new Customer();
        Customer customer2 = new Customer();

        customer1.setUsername("username1");
        customer1.setPassword("password");
        customer1.setUserRole(UserRole.ROLE_USER);
        customer1.setName("Jozo");
        customer1.setAddress("sk");
        customer1.setPhoneNumber("123");

        customer2.setUsername("username2");
        customer2.setPassword("password");
        customer2.setUserRole(UserRole.ROLE_USER);
        customer2.setName("Fero");
        customer2.setAddress("cz");
        customer2.setPhoneNumber("321");

        dao.insert(customer1);
        dao.insert(customer2);

        dao.remove(customer1);
        List<Customer> customers = dao.findAll();
        assertNotNull(customers);
        assertEquals("There should be exactly 2 customers", customers.size(), 1);
        assertTrue("Expected was list containing " + customer2 + " but there is "
                + Arrays.toString(customers.toArray()), customers.contains(customer2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNull() {
        dao.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeCustomerWithoutAssignedId() {
        Customer customer = new Customer();
        dao.remove(customer);
    }
}
