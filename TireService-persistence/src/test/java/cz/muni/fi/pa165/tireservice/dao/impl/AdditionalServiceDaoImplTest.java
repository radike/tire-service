/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
/**
 *
 * @author Marek Halas
 */
public class AdditionalServiceDaoImplTest extends AbstractIntegrationTest {
    
    @Autowired
    private AdditionalServiceDaoImpl dao;
    
    @Test
    public void insert() {
        AdditionalService additionalService = new AdditionalService();     
        
        additionalService.setName("vyvazenie kolies");
        additionalService.setPrice(new BigDecimal(5.0));
        //additionalService.setOrder(new Order());
        
        dao.insert(additionalService);
        assertTrue(additionalService.getId() != 0);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void insertNull() {
        dao.insert(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void insertAdditionalServiceWithAssignedId() {
        AdditionalService additionalService = new AdditionalService();
        additionalService.setId(1);
        
        dao.insert(additionalService);
    }
    
    @Test
    public void update() {
        AdditionalService additionalService1 = new AdditionalService();
        additionalService1.setName("vyvazenie kolies");
        additionalService1.setPrice(new BigDecimal(5.0));
        
        AdditionalService additionalService2 = new AdditionalService();
        additionalService2.setName("kontrola zbiehavosti kolies");
        additionalService2.setPrice(new BigDecimal(7.0));
        
        dao.insert(additionalService1);
        dao.insert(additionalService2);
        
        additionalService1.setName("cistenie pneumatik");
        additionalService1.setPrice(new BigDecimal(9.0));
        
        dao.update(additionalService1);
        
        List<AdditionalService> additionalServices = dao.findAll();
        
        assertNotNull(additionalServices);
        assertEquals("There should be exactly 2 additionalServices", additionalServices.size(), 2);
        assertTrue("Expected was list containing " + additionalService1 + " but there is "
                + Arrays.toString(additionalServices.toArray()),
                additionalServices.contains(additionalService1));
        assertTrue("Expected was list containing " + additionalService2 + " but there is "
                + Arrays.toString(additionalServices.toArray()),
                additionalServices.contains(additionalService2));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateNull() {
        dao.update(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void updateAdditionalServiceWithoutAssignedId() {
        AdditionalService additionalService = new AdditionalService();
        dao.update(additionalService);
    }   
    
    @Test
    public void remove() {
        AdditionalService additionalService1 = new AdditionalService();
        additionalService1.setName("vyvazenie kolies");
        additionalService1.setPrice(new BigDecimal(5.0));
        
        AdditionalService additionalService2 = new AdditionalService();
        additionalService2.setName("kontrola zbiehavosti kolies");
        additionalService2.setPrice(new BigDecimal(7.0));
        
        dao.insert(additionalService1);
        dao.insert(additionalService2);
        
        dao.remove(additionalService1);
        
        List<AdditionalService> additionalServices = dao.findAll();
        
        assertNotNull(additionalServices);
        assertEquals("There should be exactly 2 additionalServices", additionalServices.size(), 1);
        assertTrue("Expected was list containing " + additionalService2 + " but there is "
                + Arrays.toString(additionalServices.toArray()),
                additionalServices.contains(additionalService2));
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void removeNull() {
        dao.remove(null);
    }
    
    @Test(expected = IllegalArgumentException.class)
    public void removeAdditionalServiceWithoutAssignedId() {
        AdditionalService additionalService = new AdditionalService();
        dao.remove(additionalService);
    }
    
    @Test public void controlAdditionalService() {
        AdditionalService additionalService = new AdditionalService();  
        additionalService.setName("vyvazenie kolies");
        additionalService.setPrice(new BigDecimal(5.0));
        
        dao.insert(additionalService);
        
        List<AdditionalService> additionalServices = dao.findAll();
        
        assertEquals(1, additionalServices.size());

        additionalService = additionalServices.get(0);
        assertEquals("vyvazenie kolies", additionalService.getName());
        assertEquals(new BigDecimal(5.0), additionalService.getPrice());
        assertTrue(additionalService.getId() != 0);  
    }
    
    @Test
    public void findAll() {
        List<AdditionalService> additionalServices = dao.findAll();
        
        assertNotNull(additionalServices);
        assertEquals("There are no services in db, "
                + "so list of services should be empty",
                additionalServices.size(), 0);

        AdditionalService additionalService1 = new AdditionalService();
        additionalService1.setName("vyvazenie kolies");
        additionalService1.setPrice(new BigDecimal(5.0));
        
        dao.insert(additionalService1);

        additionalServices = dao.findAll();
        
        assertNotNull(additionalServices);
        assertEquals("There should be exactly 1 additionalService", additionalServices.size(), 1);
        assertEquals("Expected was list with " + additionalService1 + " but there is "
                + additionalServices.get(0),
                additionalServices, Arrays.asList(additionalService1));

        AdditionalService additionalService2 = new AdditionalService();
        additionalService2.setName("kontrola zbiehavosti kolies");
        additionalService2.setPrice(new BigDecimal(7.0));
        
        dao.insert(additionalService2);

        additionalServices = dao.findAll();
        
        assertNotNull(additionalServices);
        assertEquals("There should be exactly 2 additionalServices",
                additionalServices.size(), 2);
        assertTrue( "Expected was list containing " + additionalService1 + " but there is "
                + Arrays.toString(additionalServices.toArray()),
                additionalServices.contains(additionalService1));
        assertTrue("Expected was list containing " + additionalService2 + " but there is "
                + Arrays.toString(additionalServices.toArray()),
                additionalServices.contains(additionalService2));
    } 
    
    @Test
    public void findAdditionalServiceByName() throws Exception{       
        AdditionalService additionalService1 = new AdditionalService();
        additionalService1.setName("vyvazenie kolies");
        additionalService1.setPrice(new BigDecimal(5.0));
        
        AdditionalService additionalService2 = new AdditionalService();
        additionalService2.setName("kontrola zbiehavosti kolies");
        additionalService2.setPrice(new BigDecimal(7.0));
        
        dao.insert(additionalService1);
        dao.insert(additionalService2);
        
        AdditionalService foundedService = dao.findByName("vyvazenie kolies");
        
        assertEquals(additionalService1, foundedService);
    }
    
    @Test
    public void findAdditionalServiceById() throws Exception{       
        AdditionalService additionalService1 = new AdditionalService();
        additionalService1.setName("vyvazenie kolies");
        additionalService1.setPrice(new BigDecimal(5.0));
        
        AdditionalService additionalService2 = new AdditionalService();
        additionalService2.setName("kontrola zbiehavosti kolies");
        additionalService2.setPrice(new BigDecimal(7.0));
        
        dao.insert(additionalService1);
        dao.insert(additionalService2);
        
        AdditionalService foundedService = dao.findById(additionalService1.getId());
        
        assertEquals(foundedService.getId(), additionalService1.getId());
        
        foundedService = dao.findById(additionalService2.getId());
        
        assertEquals(foundedService.getId(), additionalService2.getId());
       
        
    }
}
