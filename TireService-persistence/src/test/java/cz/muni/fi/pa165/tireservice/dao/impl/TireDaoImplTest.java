package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.entities.Tire;
import cz.muni.fi.pa165.tireservice.enums.TireType;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.junit.Test;

/**
 * Test of {@link TireDaoImpl}
 *
 * @author Martin Pruska
 */
public class TireDaoImplTest extends AbstractIntegrationTest {

    @Autowired
    private TireDaoImpl dao;

    @Test
    public void insertTire() {
        Tire tire = new Tire();
        tire.setType(TireType.ROAD);
        tire.setSize(50);
        tire.setManufacturer("Michelin");
        tire.setPrice(new BigDecimal(100));

        dao.insert(tire);
        assertTrue(tire.getId() != 0);

        // control parametrs of inserted tire
        List<Tire> tires = dao.findAll();
        assertEquals("There should be exactly 1 tire.", tires.size(), 1);
        Tire foundedTire = tires.get(0);
        assertNotNull("Not able to find tire after insert().", foundedTire);
        assertEquals(foundedTire.getId(), tire.getId());
        assertEquals(foundedTire.getType(), tire.getType());
        assertEquals(foundedTire.getSize(), tire.getSize());
        assertEquals(foundedTire.getManufacturer(), tire.getManufacturer());
        assertEquals(foundedTire.getPrice(), tire.getPrice());
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertNull() {
        dao.insert(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void insertTireWithAssignedId() {
        Tire tire = new Tire();
        tire.setId(1);
        dao.insert(tire);
    }

    @Test
    public void update() {
        Tire tire1 = new Tire();
        tire1.setType(TireType.ROAD);
        tire1.setSize(50);
        tire1.setManufacturer("Michelin");
        tire1.setPrice(new BigDecimal(100));

        Tire tire2 = new Tire();
        tire2.setType(TireType.ENDURO);
        tire2.setSize(10);
        tire2.setManufacturer("Braun");
        tire2.setPrice(new BigDecimal(333));

        dao.insert(tire1);
        dao.insert(tire2);

        // change tire1 data and update
        tire1.setType(TireType.LANDSCAPING);
        tire1.setSize(80);
        tire1.setManufacturer("NewMichelin");
        tire1.setPrice(new BigDecimal(111));
        dao.update(tire1);

        // check if tire1 was updated
        Tire foundTire = null;
        for (Tire tire : dao.findAll()) {
            if (tire.getId() == tire1.getId()) {
                foundTire = tire;
                break;
            }
        }
        assertNotNull("Not able to find tire after update().", foundTire);
        assertEquals(foundTire.getId(), tire1.getId());
        assertEquals(foundTire.getType(), tire1.getType());
        assertEquals(foundTire.getSize(), tire1.getSize());
        assertEquals(foundTire.getManufacturer(), tire1.getManufacturer());
        assertEquals(foundTire.getPrice(), tire1.getPrice());

        // check database
        List<Tire> tires = dao.findAll();
        assertNotNull(tires);
        assertEquals("There should be exactly 2 tires", tires.size(), 2);
        assertTrue("Expected was list containing " + tire1 + " but there is "
                + Arrays.toString(tires.toArray()), tires.contains(tire1));
        assertTrue("Expected was list containing " + tire2 + " but there is "
                + Arrays.toString(tires.toArray()), tires.contains(tire2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateNull() {
        dao.update(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void updateTireWithoutAssignedId() {
        Tire tire = new Tire();
        dao.update(tire);
    }

    @Test
    public void remove() {
        Tire tire1 = new Tire();
        tire1.setType(TireType.ROAD);
        tire1.setSize(50);
        tire1.setManufacturer("Michelin");
        tire1.setPrice(new BigDecimal(100));

        Tire tire2 = new Tire();
        tire2.setType(TireType.ENDURO);
        tire2.setSize(10);
        tire2.setManufacturer("Braun");
        tire2.setPrice(new BigDecimal(333));

        dao.insert(tire1);
        dao.insert(tire2);

        dao.remove(tire1);
        List<Tire> tires = dao.findAll();
        assertNotNull(tires);
        assertEquals("There should be exactly 1 tire", tires.size(), 1);
        assertTrue("Expected was list containing " + tire2 + " but there is "
                + Arrays.toString(tires.toArray()), tires.contains(tire2));
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeNull() {
        dao.remove(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void removeTireWithoutAssignedId() {
        Tire tire = new Tire();
        dao.remove(tire);
    }

    @Test
    public void findAll() {
        List<Tire> tires = dao.findAll();
        assertNotNull(tires);
        assertEquals("There are no tires in db, so list of tires should be empty",
                tires.size(), 0);

        Tire tire1 = new Tire();
        tire1.setType(TireType.ROAD);
        tire1.setSize(50);
        tire1.setManufacturer("Michelin");
        tire1.setPrice(new BigDecimal(100));
        dao.insert(tire1);

        tires = dao.findAll();
        assertNotNull(tires);
        assertEquals("There should be exactly 1 tire", tires.size(), 1);
        assertEquals("Expected was list with " + tire1 + " but there is "
                + tires.get(0), tires, Arrays.asList(tire1));

        Tire tire2 = new Tire();
        tire2.setType(TireType.ENDURO);
        tire2.setSize(10);
        tire2.setManufacturer("Braun");
        tire2.setPrice(new BigDecimal(333));
        dao.insert(tire2);

        tires = dao.findAll();
        assertNotNull(tires);
        assertEquals("There should be exactly 2 tires", tires.size(), 2);
        assertTrue("Expected was list containing " + tire1 + " but there is "
                + Arrays.toString(tires.toArray()), tires.contains(tire1));
        assertTrue("Expected was list containing " + tire2 + " but there is "
                + Arrays.toString(tires.toArray()), tires.contains(tire2));
    }
}
