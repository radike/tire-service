/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao;

import cz.muni.fi.pa165.tireservice.entities.Tire;
import java.util.List;

/**
 *
 * @author Samuel Briškár
 */
public interface TireDao {

    /**
     * Inserts <var>tire</var> to db.
     *
     * @param tire {@link Tire} to be stored in db
     * @throws IllegalArgumentException if <var>tire</var> is <code>null</code>
     * or has assigned id
     */
    void insert(Tire tire);

    /**
     * Update parameter of given <var>tire</var> in db.
     *
     * @param tire {@link Tire} to be update in db
     * @throws IllegalArgumentException if <var>tire</var> is <code>null</code>
     * or is not in db.
     */
    void update(Tire tire) throws IllegalArgumentException;

    /**
     * Remove given <var>tire</var> from db.
     *
     * @param tire {@link Tire to be removed from db
     * @throws IllegalArgumentException if <var>tire</var> is <code>null</code>
     * or is not in db.
     */
    void remove(Tire tire) throws IllegalArgumentException;

    /**
     * Retrieves all {@link Tire} from db.
     *
     * @return list of {@link Tire}s
     */
    List<Tire> findAll();

    /**
     * Retrieves {@link Tire} from db with the specified <var>id</var>.
     *
     * @param id id of the tire to be returned
     * @return {@link Tire} with the specified id, otherwise returns null
     */
    public Tire findById(long id);

}
