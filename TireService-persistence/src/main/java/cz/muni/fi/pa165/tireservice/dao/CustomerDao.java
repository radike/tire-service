/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao;

import cz.muni.fi.pa165.tireservice.entities.Customer;
import java.util.List;

/**
 *
 * @author Marek Halas
 */
public interface CustomerDao {

    /**
     * Inserts <var>customer</var> to db.
     *
     * @param customer {@link Customer} to be stored in db
     * @throws IllegalArgumentException if <var>customer</var> is
     * <code>null</code> or has assigned id
     */
    void insert(Customer customer) throws IllegalArgumentException;

    /**
     * Update parameter of given <var>customer</var> in db.
     *
     * @param customer {@link Customer} to be update in db
     * @throws IllegalArgumentException if <var>customer</var> is
     * <code>null</code> or is not in db.
     */
    void update(Customer customer) throws IllegalArgumentException;

    /**
     * Remove given <var>customer</var> from db.
     *
     * @param customer {@link Customer} to be removed from db
     * @throws IllegalArgumentException if <var>customer</var> is
     * <code>null</code> or is not in db.
     */
    void remove(Customer customer) throws IllegalArgumentException;

    /**
     * Retrieves {@link Customer} of the specified <var>ID</var> from db.
     *
     * @param id ID of the {@link Customer} to retrieve
     * @throws javax.persistence.NoResultException if the required Customer is
     * not in db
     * @throws IllegalArgumentException if the given id is not positive number
     * @return {@link Customer} if there is one of specified <var>ID</var> in db
     */
    Customer findById(long id) throws IllegalArgumentException;

    /**
     * Retrieves {@link Customer} with the specified <var>username</var> from
     * db.
     *
     * @param username username of the {@link Customer} to retrieve
     * @throws javax.persistence.NoResultException if the required Customer is
     * not in db
     * @throws IllegalArgumentException if the given username is invalid (null
     * or empty)
     * @return {@link Customer} if there is one of specified <var>username</var>
     * in db
     */
    Customer findByUsername(String username) throws IllegalArgumentException;

    long getCountOfCustomers();

    /**
     * Retrieves all {@link Customer} from db.
     *
     * @return list of {@link Customer}s
     * @throws IllegalArgumentException if <var>id</var> is id <= 0.
     */
    List<Customer> findAll();
}
