/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.dao.TireDao;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Samuel Briškár
 */
@Repository
public class TireDaoImpl implements TireDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(Tire tire) {
        if (tire == null) {
            throw new IllegalArgumentException("tire can't be null");
        }
        if (tire.getId() != 0) {
            throw new IllegalArgumentException("tire can't have assigned id ( != 0)");
        }
        checkTire(tire);
        em.persist(tire);
    }

    @Override
    public void update(Tire tire) throws IllegalArgumentException {
        if (null == tire) {
            throw new IllegalArgumentException("Tire can't be null.");
        }
        if (0 == tire.getId()) {
            throw new IllegalArgumentException("Tire dosn't have assigned id ( ==0).");
        }
        checkTire(tire);
        Tire foundedTire
                = em.find(Tire.class, tire.getId());
        if (null == foundedTire) {
            throw new IllegalArgumentException("Specified tire is not in db.");
        }
        em.merge(tire);
    }

    @Override
    public void remove(Tire tire) throws IllegalArgumentException {
        if (null == tire) {
            throw new IllegalArgumentException("Tire can't be null.");
        }
        if (0 == tire.getId()) {
            throw new IllegalArgumentException("Tire dosn't have assigned id ( ==0).");
        }
        Tire foundedTire
                = em.find(Tire.class, tire.getId());
        if (null == foundedTire) {
            throw new IllegalArgumentException("Specified tire is not in db.");
        }
        em.remove(foundedTire);
    }

    @Override
    public List<Tire> findAll() {
        List<Tire> tires = em.createQuery("Select o FROM Tire o", Tire.class)
                .getResultList();
        return tires;
    }

    @Override
    public Tire findById(long id) {
        Tire tire = em.find(Tire.class, id);
        return tire;
    }
    
    private void checkTire(Tire tire) {
        if (tire == null) {
            throw new IllegalArgumentException("tire can't be null");
        }
        if (tire.getManufacturer() == null) {
            throw new IllegalArgumentException("tire can't have null as manufacturer");
        }
        if (tire.getManufacturer().length() == 0) {
            throw new IllegalArgumentException("tire can't have empty manufacturer");
        }
        if (tire.getPrice() == null) {
            throw new IllegalArgumentException("tire can't have null as price");
        }
        if (tire.getSize() < 0) {
            throw new IllegalArgumentException("tire can't have size below zero");
        }
        if (tire.getType() == null) {
            throw new IllegalArgumentException("tire can't have null as type");
        }
    }
}
