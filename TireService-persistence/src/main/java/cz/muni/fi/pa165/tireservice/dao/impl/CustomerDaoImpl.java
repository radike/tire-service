/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.dao.CustomerDao;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Marek Halas
 */
@Repository
public class CustomerDaoImpl implements CustomerDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(Customer customer) throws IllegalArgumentException {
        if (customer == null) {
            throw new IllegalArgumentException("customer can't be null");
        }
        if (customer.getId() != 0) {
            throw new IllegalArgumentException("customer can't have assigned id ( != 0)");
        }
        if (customer.getUsername() == null) {
            throw new IllegalArgumentException("customer can't have null as username");
        }
        if (customer.getUsername().length() == 0) {
            throw new IllegalArgumentException("customer can't have empty username");
        }
        if (customer.getPassword() == null) {
            throw new IllegalArgumentException("customer can't have null as password");
        }
        if (customer.getPassword().length() == 0) {
            throw new IllegalArgumentException("customer can't have empty password");
        }
        if (customer.getUserRole() == null) {
            throw new IllegalArgumentException("customer can't have null as user role");
        }
        em.persist(customer);
    }

    @Override
    public void update(Customer customer) throws IllegalArgumentException {
        if (customer == null) {
            throw new IllegalArgumentException("customer can't be null.");
        }
        if (customer.getId() <= 0) {
            throw new IllegalArgumentException("customer doesn't have assigned id ( <=0).");
        }

        Customer foundedCustomer = em.find(Customer.class, customer.getId());
        if (foundedCustomer == null) {
            throw new IllegalArgumentException("specified customer is not in db.");
        }
        em.merge(customer);
    }

    @Override
    public void remove(Customer customer) throws IllegalArgumentException {
        if (customer == null) {
            throw new IllegalArgumentException("customer can't be null.");
        }
        if (customer.getId() <= 0) {
            throw new IllegalArgumentException("customer dosn't have assigned id ( <=0).");
        }
        Customer foundedCustomer = em.find(Customer.class, customer.getId());
        if (foundedCustomer == null) {
            throw new IllegalArgumentException("specified customer is not in db.");
        }
        em.remove(foundedCustomer);
    }

    @Override
    public Customer findById(long id) throws IllegalArgumentException {
        if (id <= 0) {
            throw new IllegalArgumentException("id can't be negative number");
        }
        Customer customer = em.createQuery(
                "SELECT c FROM Customer c WHERE c.id = :selectedId",
                Customer.class).setParameter("selectedId", id).getSingleResult();
        return customer;
    }

    @Override
    public Customer findByUsername(String username) throws IllegalArgumentException {
        if (username == null) {
            throw new IllegalArgumentException("username can't be null");
        }
        if (username.length() == 0) {
            throw new IllegalArgumentException("username can't be empty");
        }
        Customer customer = em.createQuery(
                "SELECT c FROM Customer c WHERE c.username = :username",
                Customer.class).setParameter("username", username)
                .getSingleResult();
        return customer;
    }
    
    @Override
    public List<Customer> findAll() {
        List<Customer> customers = em.createQuery("SELECT c FROM Customer c",
                Customer.class).getResultList();
        return customers;
    }

    @Override
    public long getCountOfCustomers() {
        long count = (long) em.createQuery("SELECT COUNT(*) FROM Customer c").getSingleResult();
        return count;
    }
}
