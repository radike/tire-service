/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.dao.OrderDao;
import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 * Implementation of {@link OrderDao}
 *
 * @author Radoslav Rabara
 * @see OrderDao
 */
@Repository
public class OrderDaoImpl implements OrderDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(Order order) {
        checkOrder(order);
        if (order.getId() != 0) {
            throw new IllegalArgumentException("order can't have assigned id ( != 0)");
        }
        em.persist(order);
    }

    @Override
    public void update(Order order) throws IllegalArgumentException {
        checkOrder(order);
        Order foundOrder = findById(order.getId());
        em.merge(order);
    }

    @Override
    public void remove(Order order) throws IllegalArgumentException {
        if (order == null) {
            throw new IllegalArgumentException("order can't be null");
        }
        if (order.getId() == 0) {
            throw new IllegalArgumentException("order can't be without assigned id ( == 0)");
        }
        Order foundOrder = findById(order.getId());
        em.remove(foundOrder);
    }

    @Override
    public List<Order> findAll() {
        List<Order> orders = em.createQuery("Select o FROM OrderEntity o", Order.class)
                .getResultList();
        return orders;
    }

    @Override
    public List<Order> findByCustomer(Customer customer) {
        if (customer == null) {
            throw new IllegalArgumentException("customer can't be null");
        }
        if (customer.getId() == 0) {
            throw new IllegalArgumentException("customer can't be without assigned id");
        }
        List<Order> orders = em.createQuery("Select o FROM OrderEntity o WHERE o.customer = :customer", Order.class)
                .setParameter("customer", customer).getResultList();
        return orders;
    }

    @Override
    public List<Order> findByTotalPrice(BigDecimal totalPrice) {
        if (totalPrice == null) {
            throw new IllegalArgumentException("totalPrice can't be null");
        }
        List<Order> orders = em.createQuery("Select o FROM OrderEntity o WHERE o.totalPrice = :price", Order.class)
                .setParameter("price", totalPrice).getResultList();
        return orders;
    }

    @Override
    public List<Order> findByTire(Tire tire) {
        if (tire == null) {
            throw new IllegalArgumentException("tire can't be null");
        }
        if (tire.getId() == 0) {
            throw new IllegalArgumentException("tire can't be without assigned id");
        }
        List<Order> orders = em.createQuery("Select o FROM OrderEntity o WHERE o.tire = :tire", Order.class)
                .setParameter("tire", tire).getResultList();
        return orders;
    }

    @Override
    public List<Order> findByAdditionalService(AdditionalService service) {
        if (service == null) {
            throw new IllegalArgumentException("service can't be null");
        }
        if (service.getId() == 0) {
            throw new IllegalArgumentException("service can't be without assigned id");
        }
        List<Order> orders = em.createQuery("Select o FROM OrderEntity o WHERE :service MEMBER OF o.additionalServices", Order.class)
                .setParameter("service", service).getResultList();
        return orders;
    }

    @Override
    public Order findById(long id) {
        Order order = em.find(Order.class, id);
        if (null == order) {
            throw new IllegalArgumentException("Specified order is not in db.");
        }
        return order;
    }

    @Override
    public List<Order> findByDate(Date from, Date to) {
        if (from == null) {
            throw new IllegalArgumentException("from can't be null");
        }
        if (to == null) {
            throw new IllegalArgumentException("to can't be null");
        }
        from = createDateWithTime(from, 0, 0, 0, 0);
        to = createDateWithTime(to, 23, 59, 59, 999);
        List<Order> orders = em.createQuery("Select o FROM OrderEntity o WHERE o.date >= :from AND o.date <= :to",
                Order.class).setParameter("from", from).setParameter("to", to)
                .getResultList();
        return orders;
    }

    private void checkOrder(Order order) {
        if (order == null) {
            throw new IllegalArgumentException("order can't be null");
        }
        if (order.getCustomer() == null) {
            throw new IllegalArgumentException("order can't have null as customer");
        }
        if (order.getCustomer().getId() == 0) {
            throw new IllegalArgumentException("order can't have customer without assigned id");
        }
        if (order.getTire() == null) {
            throw new IllegalArgumentException("order can't have null as customer");
        }
        if (order.getTire().getId() == 0) {
            throw new IllegalArgumentException("order can't have tire without assigned id");
        }
        if (order.getVehicle() == null) {
            throw new IllegalArgumentException("order can't have null as vehicle");
        }
        if (order.getTotalPrice() == null) {
            throw new IllegalArgumentException("order can't have null as total price");
        }
        if (order.getDate() == null) {
            throw new IllegalArgumentException("order can't have null as date");
        }
    }

    private Date createDateWithTime(Date date, int hour, int minute, int second, int ms) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);
        cal.set(Calendar.MILLISECOND, 0);
        return cal.getTime();
    }
}
