/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao;

import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import java.util.List;

/**
 * AdditionalService DAO contract (DataAccessObject pattern)
 *
 * @author Martin Pruska
 */
public interface AdditionalServiceDao {

    /**
     * Inserts <var>service</var> to db.
     *
     * @param service {@link AdditionalService} to be stored in db
     * @throws IllegalArgumentException if <var>service</var> is
     * <code>null</code> or has assigned id.
     */
    void insert(AdditionalService service) throws IllegalArgumentException;

    /**
     * Update parameter of given <var>service</var> in db.
     *
     * @param service {@link AdditionalService} to be update in db
     * @throws IllegalArgumentException if <var>service</var> is
     * <code>null</code> or is not in db.
     */
    void update(AdditionalService service) throws IllegalArgumentException;

    /**
     * Remove given <var>service</var> from db.
     *
     * @param service {@link AdditionalService} to be removed from db
     * @throws IllegalArgumentException if <var>service</var> is
     * <code>null</code> or is not in db.
     */
    void remove(AdditionalService service) throws IllegalArgumentException;

    /**
     * Retrieves all {@link AdditionalService} from db.
     *
     * @return list of {@link AdditionalService}s
     */
    List<AdditionalService> findAll();

    /**
     * Retrieves {@link AdditionalService} of specified <var>ID</var> from db.
     *
     * @param id ID of {@link AdditionalService} to retrieve.
     * @return {@link AdditionalService} if there is one of specified
     * <var>ID</var> in db
     */
    AdditionalService findById(long id);

    /**
     * Retrieves {@link AdditionalService} of specified name from db.
     * <var>Name</var> is unique parameter of {@link AdditionalService}.
     *
     * @param name Name of {@link AdditionalService} to retrieve.
     * @return {@link AdditionalService} if there is one of specified name in
     * db, otherwise returns null.
     */
    AdditionalService findByName(String name);
}
