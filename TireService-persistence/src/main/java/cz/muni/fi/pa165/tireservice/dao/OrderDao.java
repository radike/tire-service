/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao;

import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.entities.Customer;
import cz.muni.fi.pa165.tireservice.entities.Order;
import cz.muni.fi.pa165.tireservice.entities.Tire;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * Order DAO contract (DataAccessObject pattern)
 *
 * @author Radoslav Rabara
 */
public interface OrderDao {

    /**
     * Inserts <var>order</var> to db.
     *
     * @param order {@link Order} to be stored in db
     * @throws IllegalArgumentException if <var>order</var> is <code>null</code>
     * or has assigned id or any of not nullable attribute is null or customer
     * doesn't have assigned id
     */
    void insert(Order order);

    /**
     * Update parameter of given <var>order</var> in db.
     *
     * @param order {@link Order} to be update in db
     * @throws IllegalArgumentException if <var>order</var> is <code>null</code>
     * or has assigned id or any of not nullable attribute is null or customer
     * doesn't have assigned id
     */
    void update(Order order) throws IllegalArgumentException;
    
    /**
     * Remove given <var>order</var> from db.
     *
     * @param order {@link Order} to be removed from db
     * @throws IllegalArgumentException if <var>order</var> is <code>null</code>
     * or doesn't have assigned id
     */
    void remove(Order order) throws IllegalArgumentException;    
        
    /**
     * Returns list of all {@link Order}s
     *
     * @return list of {@link Order}s
     */
    List<Order> findAll();

    /**
     * Returns list of {@link Order}s with the specified <var>customer</var>
     * @param customer customer
     * @return list of {@link Order}s
     */
    List<Order> findByCustomer(Customer customer);

    /**
     * Returns list of {@link Order}s with the specified <var>totalPrice</var>
     * @param totalPrice total price of the orders to return
     * @return list of {@link Order}s
     */
    List<Order> findByTotalPrice(BigDecimal totalPrice);

    List<Order> findByTire(Tire tire);
    
    List<Order> findByAdditionalService(AdditionalService service);
    
    Order findById(long id);
    
    /**
     * Returns list of {@link Order}s that was created between <from, to>.
     *
     * @param from start of the filter interval (takes day, month, year and
     * ignores time)
     * @param to end of the filter interval (the same as <var>from</var>)
     *
     * @return list of {@link Order}s
     */
    List<Order> findByDate(Date from, Date to);
}
