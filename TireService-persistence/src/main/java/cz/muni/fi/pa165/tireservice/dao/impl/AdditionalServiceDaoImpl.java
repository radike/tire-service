/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.dao.impl;

import cz.muni.fi.pa165.tireservice.entities.AdditionalService;
import cz.muni.fi.pa165.tireservice.dao.AdditionalServiceDao;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;

/**
 * Implementation of {@link AdditionalServiceDao}
 *
 * @author Martin Pruska
 * @see AdditionalServiceDao
 */
@Repository
public class AdditionalServiceDaoImpl implements AdditionalServiceDao {

    @PersistenceContext
    private EntityManager em;

    @Override
    public void insert(AdditionalService service) {
        if (null == service) {
            throw new IllegalArgumentException("AddtionalService can't be null.");
        }
        if (0 != service.getId()) {
            throw new IllegalArgumentException("AddtionalService can't have assigned id ( !=0).");
        }
        em.persist(service);
    }

    @Override
    public void update(AdditionalService service) throws IllegalArgumentException {
        if (null == service) {
            throw new IllegalArgumentException("AddtionalService can't be null.");
        }
        if (0 == service.getId()) {
            throw new IllegalArgumentException("AddtionalService doesn't have assigned id ( ==0).");
        }

        AdditionalService foundedService
                = em.find(AdditionalService.class, service.getId());
        if (null == foundedService) {
            throw new IllegalArgumentException("Specified service is not in db.");
        }
        em.merge(service);
    }

    @Override
    public void remove(AdditionalService service) throws IllegalArgumentException {
        if (null == service) {
            throw new IllegalArgumentException("AddtionalService can't be null.");
        }
        if (0 == service.getId()) {
            throw new IllegalArgumentException("AddtionalService doesn't have assigned id ( ==0).");
        }

        AdditionalService foundedService
                = em.find(AdditionalService.class, service.getId());
        if (null == foundedService) {
            throw new IllegalArgumentException("Specified service is not in db.");
        }
        em.remove(foundedService);
    }

    @Override
    public List<AdditionalService> findAll() {
        List<AdditionalService> services = em.createQuery(
                "SELECT s FROM AdditionalService s",
                AdditionalService.class)
                .getResultList();
        return services;
    }

    @Override
    public AdditionalService findById(long id) {
        AdditionalService service = em.createQuery(
                "SELECT s FROM AdditionalService s WHERE s.id = :sid",
                AdditionalService.class)
                .setParameter("sid", id)
                .getSingleResult();
        return service;
    }

    @Override
    public AdditionalService findByName(String name) {
        if (null == name) {
            throw new IllegalArgumentException("parameter 'name' can't be null");
        }
        AdditionalService service = em.createQuery(
                "SELECT s FROM AdditionalService s WHERE s.name = :sname",
                AdditionalService.class)
                .setParameter("sname", name)
                .getSingleResult();
        return service;
    }
}
