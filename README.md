# Tire Service ##
### Project description ###

* Tire Service system, in which customer can select new tires for his vehicle
* Tire service provides various kind of tires (size, manufacturer, price)
* Selection of a customer is logged with his name, address, telephone number and type of a vehicle
* Tire service also provides additionally services (additionally to changing of tires) like tire toe alignment etc, which can ordered together with tires changing
* One customer can create more orders
* The system also provides list of orders (filtered by customers, total price etc)

### Project launching ###

 To project build simply execute these commands from the directory where you cloned the project
 
```
#!bash
cd tire-service
mvn install
```

 To run web application and acces REST API

```
#!bash
cd TireService-web
mvn tomcat7:run
```

 The webpages will be available on the url

```
 http://localhost:8080
```

 The database is expected to be on port 1527 on localhost (database name: pa165, password: pa165)

Default user name is **admin** (password: admin)

 To test the REST API, you can use the client:

```
#!bash
cd TireService-client
mvn exec:java
```

### Participants ###

* Marek Halás - UCO 396 266
* Martin Pruška - UCO 373 835
* Radoslav Rábara - UCO 396 247
* Samuel Briškár - UCO 395 969