<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="additionalService.list.title"/>
<my:layout title="${title}">
    <jsp:attribute name="head">
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                $(".clickable").click(function () {
                    window.document.location = $(this).attr("href");
                });
            });
        </script>
    </jsp:attribute>
    <jsp:attribute name="body">
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="${pageContext.request.contextPath}/additionalService/new">
                <div class="addButton">
                    <fmt:message key="additionalService.list.newAdditionalService"/>
                </div>
            </a>
        </sec:authorize>
        <br/>
        <c:if test="${empty additionalServices}">
            <br/>
            <fmt:message key="additionalService.list.empty" />
        </c:if>
        <c:if test="${not empty additionalServices}">
            <table>
                <tr>
                    <th>ID</th>
                    <th><fmt:message key="additionalService.list.name" /></th>
                    <th><fmt:message key="additionalService.list.price" /></th>
                </tr>
                <c:forEach items="${additionalServices}" var="additionalService">
                    <tr class="clickable" href="${pageContext.request.contextPath}/additionalService/detail/${additionalService.id}">
                        <td><c:out value="${additionalService.id}"/></td>
                        <td><c:out value="${additionalService.name}"/></td>
                        <td><c:out value="${additionalService.price}"/></td>
                        <td>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <form:form method="get"
                                           action="${pageContext.request.contextPath}/additionalService/edit/${additionalService.id}">
                                        <input type="submit" value="<fmt:message key='additionalService.list.edit'/>">
                                </form:form>
                            </sec:authorize>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <form:form method="post"
                                   action="${pageContext.request.contextPath}/additionalService/delete/${additionalService.id}">
                                <input type="submit" value="<fmt:message key='additionalService.list.delete'/>">
                                </form:form>
                          </sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </jsp:attribute>
</my:layout>