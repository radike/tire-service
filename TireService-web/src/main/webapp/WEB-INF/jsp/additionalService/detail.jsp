<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<fmt:message var="title" key="additionalService.detail.title">
    <fmt:param value="${additionalService.name}" />
</fmt:message>
<my:layout title="${title}">
    <jsp:attribute name="body">
        <form:form modelAttribute="additionalService">
            <fieldset>
                <%@include file="detail_form.jsp"%>
            </fieldset>
        </form:form>
    </jsp:attribute>
</my:layout>