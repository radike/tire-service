<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table>
    <tr>
        <th class="table-key">
            <fmt:message key="additionalService.form.name" />
        </th>
        <td>
            <c:out value="${additionalService.name}" />
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="additionalService.form.price" />
        </th>
        <td>
            <c:out value="${additionalService.price}" />
        </td>
    </tr>
</table>