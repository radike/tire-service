<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<table>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.name" />
        </th>
        <td>
            <c:out value="${customer.name}" />
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.address" />
        </th>
        <td>
            <c:out value="${customer.address}" />
        </td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.phoneNumber" />
        </th>
        <td>
            <c:out value="${customer.phoneNumber}" />
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.username" />
        </th>
        <td>
            <c:out value="${customer.username}" />
        </td>
    </tr>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.userRole" />
        </th>
        <td>
            <c:out value="${customer.userRole}" />
        </td>
    </tr>
    </sec:authorize>
    <tr>
        <th class="table-key">
            <c:if test="${empty customer.vehicles}">
                <form:label path="vehicles"><fmt:message key="customer.form.noVehicles" /></form:label>
            </c:if>
            <c:if test="${not empty customer.vehicles}">
                <form:label path="vehicles"><fmt:message key="customer.form.vehicles" /></form:label>
            </c:if>
        </th>
        <td>
        <c:if test="${not empty customer.vehicles}">
            <table>                
            <c:forEach items="${customer.vehicles}" var="a" varStatus="status">
                <tr>
                    <td>
                        <form:hidden path="vehicle" value="${a.vehicle}" />
                        <c:if test="${a == 'CAR'}">
                            <fmt:message key="order.vehicle.car" />
                        </c:if>
                        <c:if test="${a == 'TRUCK'}">
                             <fmt:message key="order.vehicle.truck" />
                        </c:if>
                        <c:if test="${a == 'SUV'}">
                            <fmt:message key="order.vehicle.suv" />
                        </c:if>
                    </td>
                </tr>
            </c:forEach>
            </table>
        </c:if>
        </td>
    </tr>
</table>