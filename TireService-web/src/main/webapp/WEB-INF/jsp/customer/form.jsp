<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<table>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.name" />
        </th>
        <td>
            <form:input path="name"></form:input>
        </td>
        <td>
            <form:errors path="name" cssClass="error" />
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.address" />
        </th>
        <td>
            <form:input path="address"></form:input>
        </td>
        <td>
            <form:errors path="address" cssClass="error" />
        </td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.phoneNumber" />*
        </th>
        <td>
            <form:input path="phoneNumber"></form:input>
        </td>
        <td>
            <form:errors path="phoneNumber" cssClass="error" />
        </td>
    </tr>
    <tr>
        <td colspan="3">
            *<fmt:message key="customer.form.phoneNumberInfo" />
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.username" />
        </th>
        <td>
            <form:input  path="username"></form:input>
        </td>
        <td>
            <form:errors path="username" cssClass="error" />
        </td>
    </tr>
    </sec:authorize>
    <sec:authorize access="hasRole('ROLE_USER')">
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.username" />
        </th>
        <td>
            <form:hidden path="username" value="${customer.username}" />
            <c:out value="${customer.username}" />
        </td>
        <td>
            <form:errors path="username" cssClass="error" />
        </td>
    </tr>
    </sec:authorize>
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.password" />
        </th>
        <td>
            <form:password path="password"></form:password>
        </td>
        <td>
            <form:errors path="password" cssClass="error" />
        </td>
    </tr>
    <sec:authorize access="hasRole('ROLE_ADMIN')">
    <tr>
        <th class="table-key">
            <fmt:message key="customer.form.userRole" />
        </th>
        <td>
            <form:select path="userRole">
                <form:options items="${userRoles}" />
            </form:select>
        </td>
        <td>
            <form:errors path="userRole" cssClass="error" />
        </td>
    </tr>
    </sec:authorize>
</table>