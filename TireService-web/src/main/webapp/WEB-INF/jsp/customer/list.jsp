<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="customer.list.title"/>
<my:layout title="${title}">
    <jsp:attribute name="head">
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                $(".clickable").click(function () {
                    window.document.location = $(this).attr("href");
                });
            });
        </script>
    </jsp:attribute>
    <jsp:attribute name="body">
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="${pageContext.request.contextPath}/customer/new">
                <div class="addButton">
                    <fmt:message key="customer.list.newCustomer"/>
                </div>
            </a>
        </sec:authorize>
        <br/>
        <c:if test="${empty customers}">
            <br/>
            <fmt:message key="customer.list.empty" />
        </c:if>
        <c:if test="${not empty customers}">
            <table>
                <tr>
                    <th>ID</th>
                    <th><fmt:message key="customer.list.name" /></th>
                    <th><fmt:message key="customer.list.address" /></th>
                    <th><fmt:message key="customer.list.phoneNumber" /></th>
                </tr>
                <c:forEach items="${customers}" var="customer">
                    <tr class="clickable" href="${pageContext.request.contextPath}/customer/detail/${customer.id}">
                        <td><c:out value="${customer.id}"/></td>
                        <td><c:out value="${customer.name}"/></td>
                        <td><c:out value="${customer.address}"/></td>
                        <td><c:out value="${customer.phoneNumber}"/></td>
                        <td>
                            <form:form method="get"
                                       action="${pageContext.request.contextPath}/customer/edit/${customer.id}">
                                    <input type="submit" value="<fmt:message key='customer.list.edit'/>">
                            </form:form>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <form:form method="post"
                                           action="${pageContext.request.contextPath}/customer/delete/${customer.id}">
                                        <input type="submit" value="<fmt:message key='customer.list.delete'/>">
                                </form:form>
                            </sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </jsp:attribute>
</my:layout>