<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
        
<table>
    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.price" />
        </th>
        <td>
            <form:input path="price"></form:input>
        </td>
        <td>
            <form:errors path="price" cssClass="error" />
        </td>
    </tr>

    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.manufacturer" />
        </th>
        <td>
            <form:input path="manufacturer"></form:input>
        </td>
        <td>
            <form:errors path="manufacturer" cssClass="error" />
        </td>
    </tr>
    
    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.size" />
        </th>
        <td>
            <form:input path="size"></form:input>
        </td>
        <td>
            <form:errors path="size" cssClass="error" />
        </td>
    </tr>
    
     <tr>
        <th class="table-key">
            <fmt:message key="tire.form.type" />
        </th>
        <td>        
            <form:select path="type">
                <c:forEach items="${types}" var="v">
                    <option value="${v}" <c:if test="${v == tire.type}">selected</c:if>>
                        <c:if test="${v == 'ROAD'}">
                            <fmt:message key="tire.type.road" />
                        </c:if>
                        <c:if test="${v == 'LANDSCAPING'}">
                            <fmt:message key="tire.type.landscaping" />
                        </c:if>
                        <c:if test="${v == 'ENDURO'}">
                            <fmt:message key="tire.type.enduro" />
                        </c:if>
                    </option>
                </c:forEach>
            </form:select>
        </td>
        <td>
            <form:errors path="type" cssClass="error" />
        </td>
    </tr>
</table>