<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="tire.list.title"/>
<my:layout title="${title}">
    <jsp:attribute name="head">
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.js"></script>
        <script>
            jQuery(document).ready(function ($) {
                $(".clickable").click(function () {
                    window.document.location = $(this).attr("href");
                });
            });
        </script>
    </jsp:attribute>
    <jsp:attribute name="body">
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <a href="${pageContext.request.contextPath}/tire/new">
                <div class="addButton">
                    <fmt:message key="tire.list.newTire"/>
                </div>
            </a>
        </sec:authorize>
        <br/>
        <c:if test="${empty tires}">
            <br/>
            <fmt:message key="tire.list.empty" />
        </c:if>
        <c:if test="${not empty tires}">
            <table>
                <tr>
                    <th><fmt:message key="tire.list.id" /></th>
                    <th><fmt:message key="tire.list.size" /></th>
                    <th><fmt:message key="tire.list.type" /></th>
                    <th><fmt:message key="tire.list.price" /></th>
                </tr>
                <c:forEach items="${tires}" var="tire">
                    <tr class="clickable" href="${pageContext.request.contextPath}/tire/detail/${tire.id}">
                        <td><c:out value="${tire.id}"/></td>
                        <td><c:out value="${tire.size}"/></td>
                        <td><c:out value="${tire.type}"/></td>
                        <td><c:out value="${tire.price}"/></td>
                        <td>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <form:form method="get"
                                           action="${pageContext.request.contextPath}/tire/edit/${tire.id}">
                                        <input type="submit" value="<fmt:message key='tire.list.edit'/>">
                                </form:form>
                            </sec:authorize>
                            <sec:authorize access="hasRole('ROLE_ADMIN')">
                                <form:form method="post"
                                           action="${pageContext.request.contextPath}/tire/delete/${tire.id}">
                                        <input type="submit" value="<fmt:message key='tire.list.delete'/>">
                                </form:form>
                            </sec:authorize>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </jsp:attribute>
</my:layout>