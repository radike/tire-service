<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table>
    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.id" />
        </th>
        <td>
            <c:out value="${tire.id}" />
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.price" />
        </th>
        <td>
            <c:out value="${tire.price}" />
        </td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.manufacturer" />
        </th>
        <td>
            <c:out value="${tire.manufacturer}" />
        </td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.size" />
        </th>
        <td>
            <c:out value="${tire.size}" />
        </td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="tire.form.type" />
        </th>
        <td>
            <form:hidden path="type" value="${tire.type}" />
            <c:if test="${tire.type == 'ROAD'}">
                <fmt:message key="tire.type.road" />
            </c:if>
            <c:if test="${tire.type == 'LANDSCAPING'}">
                <fmt:message key="tire.type.landscaping" />
            </c:if>
            <c:if test="${tire.type == 'ENDURO'}">
                <fmt:message key="tire.type.enduro" />
            </c:if>
        </td>
        <td>
            <form:errors path="type" cssClass="error" />
        </td>
    </tr>
</table>