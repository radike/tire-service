<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<table>
    <tr>
        <th class="table-key">
            <form:label path="customer"><fmt:message key="order.form.customer" /></form:label>
        </th>
        <td>
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <form:select path="customer" >
                    <c:forEach items="${customers}" var="c">
                       <option value="${c.id}" <c:if test="${c.id == order.customer.id }">selected</c:if>>
                            <c:out value="${c.name}, ${c.address}, ${c.phoneNumber}" />
                        </option>
                    </c:forEach>
                </form:select>
            </sec:authorize>
            <sec:authorize access="hasRole('ROLE_USER')">
                <form:hidden path="customer" value="${user.id}" />
                <c:out value="${user.name}, ${user.address}, ${user.phoneNumber}" />
            </sec:authorize>
        </td>
        <td>
            <form:errors path="customer" cssClass="error" />
        </td>
    </tr>
    <tr>
        <th class="table-key">
            <form:label path="tire"><fmt:message key="order.form.tire" /></form:label>
        </th>
        <td>
            <form:select path="tire" >
                <c:forEach items="${tires}" var="t">
                    <option value="${t.id}" <c:if test="${t.id == order.tire.id }">selected</c:if>>
                        <c:out value="${t.type}, ${t.size}, ${t.manufacturer}" />
                    </option>
                </c:forEach>
            </form:select>
        </td>
        <td>
            <form:errors path="tire" cssClass="error" />
        </td>
    </tr>

    <tr>
        <th class="table-key">
            <form:label path="additionalServices"><fmt:message key="order.form.additionalServices" /></form:label>
        </th>
        <td>
            <form:select path="additionalServices" multiple="multiple">
                <c:forEach items="${additionalServices}" var="a">
                    <option value="${a.name}" <c:forEach items="${order.additionalServices}" var="oa"><c:if test="${a.id == oa.id}">selected</c:if></c:forEach>>
                        <c:out value="${a.name} ${a.price}" />
                    </option>
                </c:forEach>
            </form:select>
        </td>
        <td>
            <form:errors path="additionalServices" cssClass="error" />
        </td>
    </tr>
    <tr>
        <th class="table-key">
            <form:label path="vehicle"><fmt:message key="order.form.vehicle" /></form:label>
            </th>
            <td>
            <form:select path="vehicle">
                <c:forEach items="${vehicles}" var="v">
                    <option value="${v}" <c:if test="${v == order.vehicle}">selected</c:if>>
                        <c:if test="${v == 'CAR'}">
                            <fmt:message key="order.vehicle.car" />
                        </c:if>
                        <c:if test="${v == 'TRUCK'}">
                            <fmt:message key="order.vehicle.truck" />
                        </c:if>
                        <c:if test="${v == 'SUV'}">
                            <fmt:message key="order.vehicle.suv" />
                        </c:if>
                    </option>
                </c:forEach>
            </form:select>
        </td>
        <td>
            <form:errors path="vehicle" cssClass="error" />
        </td>
    </tr>
</table>