<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<table>
    <tr>
        <th class="table-key">
            <fmt:message key="order.form.date" />
        </th>
        <td>
            <c:out value="${order.date}" />
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <form:label path="customer"><fmt:message key="order.form.customer" /></form:label>
        </th>
        <td>
            <form:hidden path="customer" value="${order.customer.id}"/>
            <table>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.id" /></th>
                    <td>
                        <c:out value="${order.customer.id}" />
                    </td>
                </tr>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.customer.name" /></th>
                    <td>
                        <c:out value="${order.customer.name}" />
                    </td>
                </tr>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.customer.address" /></th>
                    <td>
                        <c:out value="${order.customer.address}" />
                    </td>
                </tr>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.customer.phoneNumber" /></th>
                    <td>
                        <c:out value="${order.customer.phoneNumber}" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <form:label path="tire"><fmt:message key="order.form.tire" /></form:label>
        </th>
        <td>
            <form:hidden path="tire" value="${order.tire.id}"/>
            <table>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.id" /></th>
                    <td>
                        <c:out value="${order.tire.id}" />
                    </td>
                </tr>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.tire.name" /></th>
                    <td>
                        <c:out value="${order.tire.type}" />
                    </td>
                </tr>
                <tr>
                    <th><fmt:message key="order.form.tire.size" /></th>
                    <td>
                        <c:out value="${order.tire.size}" />
                    </td>
                </tr>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.tire.manufacturer" /></th>
                    <td>
                        <c:out value="${order.tire.manufacturer}" />
                    </td>
                </tr>
                <tr>
                    <th class="table-key"><fmt:message key="order.form.tire.price" /></th>
                    <td>
                        <c:out value="${order.tire.price}" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <c:if test="${empty order.additionalServices}">
                <form:label path="additionalServices"><fmt:message key="order.form.noadditionalServices" /></form:label>
            </c:if>
            <c:if test="${not empty order.additionalServices}">
                <form:label path="additionalServices"><fmt:message key="order.form.additionalServices" /></form:label>
            </c:if>
        </th>
        <td>
        <c:if test="${not empty order.additionalServices}">
            <table>
            <c:forEach items="${order.additionalServices}" var="a" varStatus="status">
                <form:hidden path="additionalServices" value="${a.name}"/>
                
                    <tr>
                        <th class="table-key"><fmt:message key="order.form.id" /></th>
                        <td>
                            <c:out value="${a.id}" />
                        </td>
                    </tr>
                    <tr>
                        <th class="table-key"><fmt:message key="order.form.additionalService.name" /></th>
                        <td>
                            <c:out value="${a.name}" />
                        </td>
                    </tr>
                    <tr>
                        <th class="table-key"><fmt:message key="order.form.additionalService.price" /></th>
                        <td>
                            <c:out value="${a.price}" />
                        </td>
                    </tr>
                    <tr style="height:12px">
                        <th></th><td></td>
                    </tr>
            </c:forEach>
            </table>
        </c:if>
        </td>
    </tr>
    <tr style="height:12px">
        <th></th><td></td>
    </tr>
    <tr>
        <th class="table-key">
            <form:label path="vehicle"><fmt:message key="order.form.vehicle" /></form:label>
            </th>
            <td>
                <form:hidden path="vehicle" value="${order.vehicle}" />
                <c:if test="${order.vehicle == 'CAR'}">
                    <fmt:message key="order.vehicle.car" />
                </c:if>
                <c:if test="${order.vehicle == 'TRUCK'}">
                     <fmt:message key="order.vehicle.truck" />
                </c:if>
                <c:if test="${order.vehicle == 'SUV'}">
                    <fmt:message key="order.vehicle.suv" />
                </c:if>
        </td>
    </tr>
    <tr style="height:24px">
        <td rowspan="2"></td>
    </tr>
    <tr>
        <th class="table-key">
            <fmt:message key="order.form.totalPrice" />
        </th>
        <td>
            <c:out value="${order.totalPrice}" />
        </td>
    </tr>
</table>