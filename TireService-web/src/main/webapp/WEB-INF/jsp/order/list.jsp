<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="my" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<fmt:message var="title" key="order.list.title"/>
<my:layout title="${title}">
    <jsp:attribute name="head">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/js/jquery-ui.css">
        <script src="${pageContext.request.contextPath}/js/jquery-1.10.2.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.js"></script>
        <script>
//              http://jqueryui.com/datepicker/
            $(function() {
                $( "#datefrompicker" ).datepicker({dateFormat: "dd/mm/yy"});
                $( "#datetopicker" ).datepicker({dateFormat: "dd/mm/yy"});
              });
            jQuery(document).ready(function ($) {
                $(".clickable").click(function () {
                    window.document.location = $(this).attr("href");
                });
            });
        </script>
    </jsp:attribute>
    <jsp:attribute name="body">
        <a href="${pageContext.request.contextPath}/order/new">
            <div class="addButton">
                <fmt:message key="order.list.neworder"/>
            </div>
        </a>
        <br/>
        <c:if test="${empty orders}">
            <br/>
            <c:if test="${not empty filter}">
                <a href="${pageContext.request.contextPath}/order/list">
                    <fmt:message key="order.list.withoutFilter"/>
                </a>
                <br/>
                <c:if test="${empty orders}">
                    <br/>
                    <fmt:message key="order.list.filter.empty"/>
                </c:if>      
            </c:if>
            <c:if test="${empty filter}">
                <fmt:message key="order.list.empty" />
            </c:if>            
        </c:if>
        <c:if test="${not empty orders}">
            <sec:authorize access="hasRole('ROLE_ADMIN')">
                <table>
                    <tr>
                        <form:form method="post" action="${pageContext.request.contextPath}/order/list/filter" modelAttribute="orderFilterData">
                            <td><input type="submit" value="<fmt:message key='order.list.filter.customer'/>"></td>
                            <td><form:select path="customerId" >
                                <option value="-1"><fmt:message key="order.list.filter.noCustomer" /></option>
                                    <c:forEach items="${customers}" var="c">
                                        <option value="${c.id}" <c:if test="${c.id == orderFilterData.customerId }">selected</c:if>>
                                            <c:out value="${c.name}, ${c.address}, ${c.phoneNumber}" />
                                        </option>
                                    </c:forEach>
                                </form:select>
                            </td>
                        </form:form>
                    </tr>
                    <tr>
                        <form:form method="post" action="${pageContext.request.contextPath}/order/list/filter" modelAttribute="orderFilterData">
                            <td><input type="submit" value="<fmt:message key='order.list.filter.totalPrice'/>"></td>
                            <td><form:input path="totalPrice" value="" size="5" /></td>    
                        </form:form>
                    </tr>
                    <tr>
                        <form:form method="post" action="${pageContext.request.contextPath}/order/list/filter" modelAttribute="orderFilterData">
                            <td><input type="submit" value="<fmt:message key='order.list.filter.date'/>"></td>
                            <td><fmt:message key="order.list.filter.dateFrom" />
                                <form:input path="dateFrom" value="" id="datefrompicker" size="10"/>
                                <fmt:message key="order.list.filter.dateTo" />
                                <form:input path="dateTo" value="" id="datetopicker" size="10"/>
                            </td>
                        </form:form>
                    </tr>
            </table>
            </sec:authorize>
            <br/>
            <table>
                <tr>
                    <th>id</th>
                    <th><fmt:message key="order.list.customer" /></th>
                    <th><fmt:message key="order.list.date" /></th>
                    <th><fmt:message key="order.list.totalPrice" /></th>
                    <sec:authorize access="hasRole('ROLE_ADMIN')">
                        <th><fmt:message key="order.list.action" /></th>
                    </sec:authorize>
                </tr>
                <c:forEach items="${orders}" var="order">
                    <tr class="clickable" href="${pageContext.request.contextPath}/order/detail/${order.id}">
                        <td><c:out value="${order.id}"/></td>
                        <td><c:out value="${order.customer.name}, ${order.customer.address}, ${order.customer.phoneNumber}"/></td>
                        <td><c:out value="${order.date}"/></td>
                        <td><c:out value="${order.totalPrice}"/></td>
                        <sec:authorize access="hasRole('ROLE_ADMIN')">
                            <td>
                                <form:form method="get"
                                       action="${pageContext.request.contextPath}/order/edit/${order.id}">
                                    <input type="submit" value="<fmt:message key='order.list.edit'/>">
                                </form:form>
                                <form:form method="post"
                                           action="${pageContext.request.contextPath}/order/delete/${order.id}">
                                        <input type="submit" value="<fmt:message key='order.list.delete'/>">
                                </form:form>
                            </td>
                        </sec:authorize>
                    </tr>
                </c:forEach>
            </table>
        </c:if>
    </jsp:attribute>
</my:layout>