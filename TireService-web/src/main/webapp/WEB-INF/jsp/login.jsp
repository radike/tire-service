<%@ page contentType="text/html;charset=UTF-8" pageEncoding="utf-8" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html lang="${pageContext.request.locale}">
    <head>
        <title><fmt:message key="login.title"/></title>
        <style>
            .error {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #a94442;
                background-color: #f2dede;
                border-color: #ebccd1;
            }

            .msg {
                padding: 15px;
                margin-bottom: 20px;
                border: 1px solid transparent;
                border-radius: 4px;
                color: #31708f;
                background-color: #d9edf7;
                border-color: #bce8f1;
            }

            #login-box {
                width: 300px;
                padding: 20px;
                margin: 100px auto;
                background: #fff;
                -webkit-border-radius: 2px;
                -moz-border-radius: 2px;
                border: 1px solid #000;
            }
        </style>
    </head>
    <body onload='document.loginForm.username.focus();'>

    <center>
        <img src="${pageContext.request.contextPath}/images/logo.png" alt="logo" />
    </center>
    <div id="login-box">
        <h3><fmt:message key="login.text"/></h3>

        <c:if test="${not empty error}">
            <div class="error">${error}</div>
        </c:if>
        <c:if test="${not empty msg}">
            <div class="msg">${msg}</div>
        </c:if>

        <form name='loginForm'
              action="<c:url value='j_spring_security_check' />" method='POST'>

            <table>
                <tr>
                    <td><fmt:message key="login.user.label"/></td>
                    <td><input type='text' name='username' value=''></td>
                </tr>
                <tr>
                    <td><fmt:message key="login.password.label"/></td>
                    <td><input type='password' name='password' /></td>
                </tr>
                <tr>
                    <td colspan='2'>
                        <center>
                            <input name="submit" type="submit" value="<fmt:message key='login.submit'/>" />
                        </center>
                    </td>
                </tr>
            </table>
        </form>
    </div>

</body>
</html>