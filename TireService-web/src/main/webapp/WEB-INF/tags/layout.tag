<!DOCTYPE html>
<%@ tag pageEncoding="utf-8" dynamic-attributes="dynattrs" trimDirectiveWhitespaces="true" %>
<%@ attribute name="title" required="true" %>
<%@ attribute name="head" fragment="true" %>
<%@ attribute name="body" fragment="true" required="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="f" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html lang="${pageContext.request.locale}">
<head>
    <title><c:out value="${title}"/></title>
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/style.css"/>
    <jsp:invoke fragment="head"/>
</head>
<body>
<h1><c:out value="${title}"/></h1>

<div id="navigation">
    <ul>
        <li><img src="${pageContext.request.contextPath}/images/logo.png" alt="logo" /></li>
        <li><a href="${pageContext.request.contextPath}/order/list"><f:message key="navigation.order"/></a></li>
        <li><a href="${pageContext.request.contextPath}/additionalService/list"><f:message key="navigation.additionalService"/></a></li>
        <sec:authorize access="hasRole('ROLE_ADMIN')">
            <li><a href="${pageContext.request.contextPath}/customer/list"><f:message key="navigation.customer"/></a></li>
        </sec:authorize>
        <li><a href="${pageContext.request.contextPath}/tire/list"><f:message key="navigation.tire"/></a></li>
        <li></li>
        <li><a href="<c:url value="/j_spring_security_logout" />" > Logout</a></li>
        
    </ul>
</div>
<div id="content">
    <c:if test="${not empty message}">
        <div class="message"><c:out value="${message}"/></div>
        <br/>
    </c:if>
    <c:if test="${not empty errorMessage}">
        <div class="error-message"><c:out value="${errorMessage}"/></div>
        <br/>
    </c:if>
    <jsp:invoke fragment="body"/>
</div>
</body>
</html>

