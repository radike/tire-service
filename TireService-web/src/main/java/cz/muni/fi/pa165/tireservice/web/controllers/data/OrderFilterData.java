/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.web.controllers.data;

/**
 *
 * @author Radoslav Rabara
 */
public class OrderFilterData {

    private String customerId;

    private String totalPrice;

    private String dateFrom;

    private String dateTo;

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getDateFrom() {
        return dateFrom;
    }

    public void setDateFrom(String dateFrom) {
        this.dateFrom = dateFrom;
    }

    public String getDateTo() {
        return dateTo;
    }

    public void setDateTo(String dateTo) {
        this.dateTo = dateTo;
    }

    @Override
    public String toString() {
        return "OrderFilterData{" + "customerId=" + customerId + ", totalPrice=" + totalPrice + ", dateFrom=" + dateFrom + ", dateTo=" + dateTo + '}';
    }
}
