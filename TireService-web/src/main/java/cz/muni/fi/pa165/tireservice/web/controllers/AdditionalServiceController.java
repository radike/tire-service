package cz.muni.fi.pa165.tireservice.web.controllers;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.services.AdditionalServiceService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * SpringMVC Controller for handling Additional Services.
 *
 * @author Martin Pruska
 */
@Controller
@RequestMapping("/additionalService")
public class AdditionalServiceController {

    final static Logger log = LoggerFactory.getLogger(AdditionalServiceController.class);

    @Autowired
    private AdditionalServiceService service;

    @Autowired
    private MessageSource messageSource;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String defaultView(Model model) {
        return showList(model);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showList(Model model) {
        log.debug("showList()");
        model.addAttribute("additionalServices", service.getAllAdditionalServices());
        return "additionalService/list";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String showNewAdditionalService(Model model) {
        log.debug("showNewAdditionalService()");
        model.addAttribute("additionalService", new AdditionalServiceDto());
        return "additionalService/new";
    }

    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public String showAdditionalServiceDetail(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showAdditionalServiceDetail() id: {}", id);
        try {
            AdditionalServiceDto dto = service.getAdditionalServiceById(id);
            if (dto != null) {
                model.addAttribute("additionalService", dto);
                log.debug("additionalService was found {}", dto);
                return "additionalService/detail";
            }
            log.debug("not found");
        } catch (ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("additionalService.not.found", new Object[]{id}, locale));
        return "additionalService/list";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String showEditAdditionalService(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showEditAdditionalService() id: {}", id);
        try {
            AdditionalServiceDto dto = service.getAdditionalServiceById(id);
            if (dto != null) {
                model.addAttribute("additionalService", dto);
                log.debug("additionalService was found {}", dto);
                return "additionalService/edit";
            }
            log.debug("not found");
        } catch (ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("additionalService.not.found", new Object[]{id}, locale));
        return "additionalService/list";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public String deleteAdditionalService(@PathVariable("id") long id, Model model,
            RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("deleteAdditionalService() id: {}", id);
        try {
            AdditionalServiceDto dto = service.getAdditionalServiceById(id);
            if (dto != null) {
                log.debug("additionalService was found {}", dto);
                service.removeAdditionalService(dto);
                redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("additionalService.remove.message", new Object[]{String.valueOf(id)}, locale)
                );
            } else {
                model.addAttribute("errorMessage",
                        messageSource.getMessage("additionalService.not.found", new Object[]{String.valueOf(id)}, locale));
            }
        } catch (EntityHasOrderException e) {
            log.debug("ServiceFailureException {}", e);
            redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("additionalService.remove.errorOrderMessage", new Object[]{String.valueOf(id)}, locale));
        } catch (ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
            model.addAttribute("errorMessage",
                    messageSource.getMessage("additionalService.remove.errorMessage", new Object[]{String.valueOf(id)}, locale));
        }
        return "redirect:" + uriBuilder.path("/additionalService/list").build();
    }

    @RequestMapping(value = "/create", method = {RequestMethod.POST})
    public String create(@Valid @ModelAttribute AdditionalServiceDto additionalService, BindingResult result,
            Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("confirm(locale={}, additionalService={})", locale, additionalService);
        if (result.hasErrors()) {
            for (ObjectError ge : result.getGlobalErrors()) {
                log.debug("ObjectError: {}", ge);
            }
            for (FieldError fe : result.getFieldErrors()) {
                log.debug("FieldError: {}", fe);
            }
            model.addAttribute("additionalService", additionalService);
            return "additionalService/new";
        }
        try {
            service.createAdditionalService(additionalService);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("additionalService.add.message", new Object[]{}, locale)
            );
        } catch (ServiceFailureException t) {
            t.printStackTrace();
            redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("additionalService.add.message.error", new Object[]{}, locale));
        }
        return "redirect:" + uriBuilder.path("/additionalService/list").build();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute AdditionalServiceDto additionalService,
            BindingResult result, Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("update(locale={}, additionalService={})", locale, additionalService);
        if (result.hasErrors()) {
            for (ObjectError ge : result.getGlobalErrors()) {
                log.debug("ObjectError: {}", ge);
            }
            for (FieldError fe : result.getFieldErrors()) {
                log.debug("FieldError: {}", fe);
            }
            model.addAttribute("additionalService", additionalService);
            return "additionalService/edit";
        }
        if (additionalService.getId() == 0) {
            try {
                service.createAdditionalService(additionalService);
                redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("additionalService.add.message", new Object[]{}, locale)
                );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                        "errorMessage",
                        messageSource.getMessage("additionalService.add.message.error", new Object[]{}, locale));
            }
        } else {
            try {
                service.updateAdditionalService(additionalService);
                redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("additionalService.edit.message", new Object[]{String.valueOf(additionalService.getId())}, locale)
                );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                        "errorMessage",
                        messageSource.getMessage("additionalService.edit.message.error", new Object[]{}, locale));
            }
        }
        return "redirect:" + uriBuilder.path("/additionalService/list").build();
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "additionalServices", new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                log.debug("convertElement - additionalServices {}", element);
                if (element instanceof AdditionalServiceDto) {
                    return element;
                }
                if (element instanceof String) {
                    try {
                        return service.getAdditionalServiceByName((String) element);
                    } catch (ServiceFailureException e) {
                        log.debug("ServiceFailureException {}", e.getMessage());
                        e.printStackTrace();
                    }
                }
                log.debug("element not recognized");
                return null;
            }
        });
    }
}
