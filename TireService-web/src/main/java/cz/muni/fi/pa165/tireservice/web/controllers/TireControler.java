/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.web.controllers;

import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.enums.TireType;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import cz.muni.fi.pa165.tireservice.services.TireService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author SEMY
 */
@Controller
@RequestMapping("/tire")
public class TireControler {

    final static Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private TireService tireService;

    @Autowired
    private MessageSource messageSource;

    @ModelAttribute(value = "types")
    public List<String> types() {
        List<String> types = new ArrayList<String>();
        for (TireType type : TireType.values()) {
            types.add(type.toString());
        }
        return types;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String defaultView(Model model) {
        return showList(model);
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showList(Model model) {
        log.debug("showList()");
        model.addAttribute("tires", tireService.getAllTires());

        return "tire/list";
    }

    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String showNewTire(Model model) {
        log.debug("showNewTires()");
        model.addAttribute("tire", new TireDto());
        return "tire/new";
    }

    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public String showTireDetail(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showTireDetail() id: {}", id);
        try {
            TireDto dto = tireService.getTireById(id);
            if (dto != null) {
                model.addAttribute("tire", dto);
                log.debug("tire was found {}", dto);
                return "tire/detail";
            }
            log.debug("not found");
        } catch (ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("tire.not.found", new Object[]{id}, locale));
        return "tire/list";
    }

    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String showEditAdditionalService(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showEditTire() id: {}", id);
        try {
            TireDto dto = tireService.getTireById(id);
            if (dto != null) {
                model.addAttribute("tire", dto);
                log.debug("tire was found {}", dto);
                return "tire/edit";
            }
            log.debug("not found");
        } catch (ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("tire.not.found", new Object[]{id}, locale));
        return "tire/list";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public String deleteAdditionalService(@PathVariable("id") long id, Model model,
            RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("deleteTire() id: {}", id);
        try {
            TireDto dto = tireService.getTireById(id);
            if (dto != null) {
                log.debug("tire was found {}", dto);
                tireService.removeTire(dto);
                redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("tire.remove.message", new Object[]{String.valueOf(id)}, locale));
            } else {
                model.addAttribute("errorMessage",
                        messageSource.getMessage("tire.not.found", new Object[]{String.valueOf(id)}, locale));
            }
        } catch (EntityHasOrderException e) {
            log.debug("ServiceFailureException {}", e);
            redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("tire.remove.errorOrderMessage", new Object[]{String.valueOf(id)}, locale));
        } catch (ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
            model.addAttribute("errorMessage",
                    messageSource.getMessage("tire.remove.errorMessage", new Object[]{String.valueOf(id)}, locale));
        }
        return "redirect:" + uriBuilder.path("/tire/list").build();
    }

    @RequestMapping(value = "/create", method = {RequestMethod.POST})
    public String create(@Valid @ModelAttribute TireDto tire, BindingResult result,
            Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("confirm(locale={}, tire={})", locale, tire);
        if (result.hasErrors()) {
            for (ObjectError ge : result.getGlobalErrors()) {
                log.debug("ObjectError: {}", ge);
            }
            for (FieldError fe : result.getFieldErrors()) {
                log.debug("FieldError: {}", fe);
            }
            model.addAttribute("tire", tire);
            return "tire/new";
        }
        try {
            tireService.createTire(tire);
            redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("tire.add.message", new Object[]{}, locale)
            );
        } catch (ServiceFailureException t) {
            t.printStackTrace();
            redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("tire.add.message.error", new Object[]{}, locale));
        }

        return "redirect:" + uriBuilder.path("/tire/list").build();
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute TireDto tire,
            BindingResult result, Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("update(locale={}, additionalService={})", locale, tire);
        if (result.hasErrors()) {
            for (ObjectError ge : result.getGlobalErrors()) {
                log.debug("ObjectError: {}", ge);
            }
            for (FieldError fe : result.getFieldErrors()) {
                log.debug("FieldError: {}", fe);
            }
            model.addAttribute("tire", tire);
            return "tire/edit";
        }
        if (tire.getId() == 0) {
            try {
                tireService.createTire(tire);
                redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("tire.add.message", new Object[]{}, locale)
                );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                        "errorMessage",
                        messageSource.getMessage("tire.add.message.error", new Object[]{}, locale));
            }
        } else {
            try {
                tireService.updateTire(tire);
                redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("tire.edit.message", new Object[]{String.valueOf(tire.getId())}, locale)
                );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                        "errorMessage",
                        messageSource.getMessage("tire.edit.message.error", new Object[]{}, locale));
            }
        }
        return "redirect:" + uriBuilder.path("/tire/list").build();
    }

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "tires", new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                log.debug("convertElement - tires {}", element);
                if (element instanceof TireDto) {
                    return element;
                }
                log.debug("element not recognized");
                return null;
            }
        });
    }
}
