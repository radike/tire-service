/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.rest.resources;

import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.rest.RestError;
import cz.muni.fi.pa165.tireservice.rest.security.RestAuth;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import cz.muni.fi.pa165.tireservice.services.TireService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import java.net.HttpURLConnection;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ImportResource;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Controller;
import org.springframework.test.context.ContextConfiguration;

/**
 * Tire REST Controller
 *
 * @author Radoslav Rabara
 */
@Controller
@Path("/tires")
@ContextConfiguration({ "classpath:spring-security.xml"})
public class TireResource {

    final static Logger log = LoggerFactory.getLogger(TireResource.class);
    
    private static final RestError INTERNAL_SERVER_ERROR = new RestError("tire service error");
    private static final RestError NOT_FOUND_ERROR = new RestError("specified id was not found");
    private static final RestError BAD_REQUEST_ERROR = new RestError("bad request");
    private static final RestError ENTITY_HAS_ORDER_ERROR = new RestError("entity has order");

    private static final Response BAD_REQUEST_RESPONSE = Response
                                    .status(HttpURLConnection.HTTP_BAD_REQUEST)
                                    .entity(BAD_REQUEST_ERROR)
                                    .build();

    private static final Response ENTITY_HAS_ORDER_ERROR_RESPONSE = Response
                                    .status(HttpURLConnection.HTTP_BAD_REQUEST)
                                    .entity(ENTITY_HAS_ORDER_ERROR)
                                    .build();
    
    private static final Response SERVICE_FAILURE_RESPONSE = Response
            .status(HttpURLConnection.HTTP_SERVER_ERROR)
            .entity(INTERNAL_SERVER_ERROR)
            .build();
    
    private static final Response NOT_FOUND_RESPONSE = Response
            .status(HttpURLConnection.HTTP_NOT_FOUND)
            .entity(NOT_FOUND_ERROR)
            .build();

    @Autowired
    private TireService service;

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTires() {
        try {
            RestAuth.restAuthentication();
            List<TireDto> tires = service.getAllTires();
            return Response.ok(tires, MediaType.APPLICATION_JSON).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTire(@PathParam("id") long id) {
        try {
            RestAuth.restAuthentication();
            TireDto tire = service.getTireById(id);
            if (tire == null) {
                return NOT_FOUND_RESPONSE;
            }
            return Response.ok(tire, MediaType.APPLICATION_JSON).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createTire(TireDto tire) {
        try {
            RestAuth.restAuthentication();
            service.createTire(tire);
            return Response.status(HttpURLConnection.HTTP_CREATED).entity(tire).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateTire(@PathParam("id") long id, TireDto tire) {
        if (tire == null || tire.getId() != id) {
            return BAD_REQUEST_RESPONSE;
        }
        try {
            RestAuth.restAuthentication();
            TireDto fromDb = service.getTireById(id);
            if (fromDb == null) {
                return NOT_FOUND_RESPONSE;
            }
            if (tire.getManufacturer() != null) {
                fromDb.setManufacturer(tire.getManufacturer());
            }
            if (tire.getPrice() != null) {
                fromDb.setPrice(tire.getPrice());
            }
            fromDb.setSize(tire.getSize());
            if (tire.getType() != null) {
                fromDb.setType(tire.getType());
            }
            service.updateTire(tire);
            return Response.ok(tire, MediaType.APPLICATION_JSON).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteTire(@PathParam("id") long id) {
        try {
            RestAuth.restAuthentication();
            TireDto tire = service.getTireById(id);
            if (tire == null) {
                return NOT_FOUND_RESPONSE;
            }
            service.removeTire(tire);
            return Response.noContent().build();
        } catch(EntityHasOrderException e) {
            return ENTITY_HAS_ORDER_ERROR_RESPONSE;
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    private boolean serviceFailedOnIllegalArgument(ServiceFailureException e) {
        Throwable cause = e.getCause();
        if (cause != null && cause.getClass().isAssignableFrom(InvalidDataAccessApiUsageException.class)) {
            Throwable causeOfCause = cause.getCause();
            if (causeOfCause != null) {
                if (causeOfCause.getClass().isAssignableFrom(IllegalArgumentException.class)) {
                    return true;
                }
            }
        }
        return false;
    }
}
