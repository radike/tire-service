package cz.muni.fi.pa165.tireservice.web.controllers;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.dto.OrderDto;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.enums.VehicleType;
import cz.muni.fi.pa165.tireservice.security.SecurityHelper;
import cz.muni.fi.pa165.tireservice.services.AdditionalServiceService;
import cz.muni.fi.pa165.tireservice.services.CustomerService;
import cz.muni.fi.pa165.tireservice.services.OrderService;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import cz.muni.fi.pa165.tireservice.services.TireService;
import cz.muni.fi.pa165.tireservice.web.controllers.data.OrderFilterData;
import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.MessageSource;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * SpringMVC Controller for handling orders.
 *
 * @author Radoslav Rabara
 */
@Controller
@RequestMapping("/order")
public class OrderController {

    final static Logger log = LoggerFactory.getLogger(OrderController.class);

    @Autowired
    private OrderService service;

    @Autowired
    private CustomerService customerService;

    @Autowired
    private TireService tireService;

    @Autowired
    private AdditionalServiceService additionalServiceService;

    @Autowired
    private MessageSource messageSource;
    private Object customers;
    
    @ModelAttribute(value = "vehicles")
    public List<String> vehicles() {
        List<String> vehicles = new ArrayList<String>();
        for (VehicleType vehicle : VehicleType.values()) {
            vehicles.add(vehicle.toString());
        }
        return vehicles;
    }
    
    @ModelAttribute(value = "customers")
    public List<CustomerDto> customers() {
        return getSortedCustomers();
    }
    
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String defaultView(Model model) {
        return showList(model);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showList(Model model) {
        log.debug("showList()");
        if (SecurityHelper.loggedUserIsAdmin()) {
            model.addAttribute("orders", service.getAllOrders());
        } else {
            UserDetails user = SecurityHelper.getLoggedUser();
            model.addAttribute("orders", service.getOrdersByCustomer(
                    customerService.getCustomerByUsername(user.getUsername())));
        }
        model.addAttribute("orderFilterData", new OrderFilterData());
        return "order/list";
    }
    
    @RequestMapping(value = "/list/filter", method = RequestMethod.POST)
    public String showFilteredList(@ModelAttribute OrderFilterData data,
            Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {
        log.debug("showFilteredList() {}", data);
        String redirect = "redirect:" + uriBuilder.path("/order/list").build();
        List<OrderDto> orders = null;
        try {
            if (data.getCustomerId() != null && data.getCustomerId().trim().length() > 0) {
                try {
                    log.debug("customer filter");
                    long id = Long.parseLong(data.getCustomerId().trim());
                    if (id != -1) {
                        CustomerDto c = customerService.getCustomerById(id);
                        orders = service.getOrdersByCustomer(c);
                    }
                } catch(NumberFormatException e) {
                    model.addAttribute("errorMessage",
                        messageSource.getMessage("order.list.filter.customerIdError", new Object[]{}, locale));
                    return redirect;
                }
            } else if (data.getTotalPrice() != null && data.getTotalPrice().trim().length() > 0) {
                try {
                    log.debug("total price filter");
                    BigDecimal totalPrice = new BigDecimal(
                            data.getTotalPrice().trim()
                                    .replaceAll(",", ".")
                                    .replaceAll(" ", ""));
                    orders = service.getOrdersByTotalPrice(totalPrice);
                } catch(NumberFormatException e) {
                    model.addAttribute("errorMessage",
                        messageSource.getMessage("order.list.filter.totalPriceError", new Object[]{}, locale));
                    return redirect;
                }
            } else if (data.getDateFrom() != null && data.getDateTo() != null &&
                    data.getDateFrom().trim().length() > 0 && data.getDateTo().trim().length() > 0) {
                SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
                try {
                    log.debug("date filter");
                    Date from = format.parse(data.getDateFrom().trim());
                    Date to = format.parse(data.getDateTo().trim());
                    log.debug("from {}, to {}", from, to);
                    orders = service.getOrdersByDate(from, to);
                } catch(ParseException e) {
                    model.addAttribute("errorMessage",
                        messageSource.getMessage("order.list.filter.dateError", new Object[]{}, locale));
                    return redirect;
                }
            }
        } catch(ServiceFailureException e) {
            e.printStackTrace();
            model.addAttribute("errorMessage",
                messageSource.getMessage("order.list.filter.error", new Object[]{}, locale));
            return redirect;
        }
        if (orders == null) {
            return redirect;
        }
        model.addAttribute("orders", orders);
        model.addAttribute("orderFilterData", data);
        model.addAttribute("filter", true);
        return "order/list";
    }
    
    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String showNewOrder(Model model, Locale locale) {
        log.debug("showNewOrder()");
        model.addAttribute("order", new OrderDto());
        if (!SecurityHelper.loggedUserIsAdmin()) {
            CustomerDto customer = customerService.getCustomerByUsername(SecurityHelper.getLoggedUserUsername());
            model.addAttribute("user", customer);
        } else {
            List<CustomerDto> customers = getSortedCustomers();
            if (customers.size() == 0) {
                model.addAttribute("errorMessage",
                        messageSource.getMessage("order.new.noCustomersError", new Object[]{}, locale));
                return showList(model);
            }
            model.addAttribute("customers", customers);
        }
        List<TireDto> tires = getSortedTires();
        if (tires.size() == 0 ) {
            model.addAttribute("errorMessage",
                    messageSource.getMessage("order.new.noTiresError", new Object[]{}, locale));
            return showList(model);
        }
        model.addAttribute("tires", tires);
        model.addAttribute("additionalServices", getSortedAdditionalServices());
        return "order/new";
    }

    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public String showOrderDetail(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showOrderDetail() id: {}", id);
        try {
            OrderDto dto = service.getOrderById(id);
            if (dto != null) {
                model.addAttribute("order", dto);
                log.debug("order was found {}", dto);
                return "order/detail";
            }
            log.debug("not found");
        } catch(ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("order.not.found", new Object[]{id}, locale));
        return "order/list";  
    }
    
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String showEditOrder(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showEditOrder() id: {}", id);
        try {
            OrderDto dto = service.getOrderById(id);
            if (dto != null) {
                model.addAttribute("order", dto);
                model.addAttribute("customers", getSortedCustomers());
                model.addAttribute("tires", getSortedTires());
                model.addAttribute("additionalServices", getSortedAdditionalServices());
                log.debug("order was found {}", dto);
                return "order/edit";
            }
            log.debug("not found");
        } catch(ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("order.not.found", new Object[]{id}, locale));
        return "order/list";  
    }
    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public String deleteOrder(@PathVariable("id") long id, Model model,
            RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {
        
        log.debug("deleteOrder() id: {}", id);
        boolean removed = false;
        try {
            OrderDto dto = service.getOrderById(id);
            if (dto != null) {
                log.debug("order was found {}", dto);
                service.removeOrder(dto);
                removed = true;
            }
        } catch(ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        if (!removed) {
            model.addAttribute("errorMessage",
                    messageSource.getMessage("order.not.found", new Object[]{String.valueOf(id)}, locale));
        } else {
            redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("order.remove.message", new Object[]{String.valueOf(id)}, locale)
                );
        }
        return "redirect:" + uriBuilder.path("/order/list").build();
    }

    @RequestMapping(value = "/confirm", method = {RequestMethod.POST})
    public String confirm(@ModelAttribute OrderDto order,
            Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("confirm(locale={}, order={})", locale, order);
        if (order.getId() != 0 || order.getCustomer() == null || order.getTire() == null) {
            redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("order.confirm.error.message", new Object[]{}, locale));
            return "redirect:" + uriBuilder.path("/order/list").build();
        }
        try {
            order.setTotalPrice(service.calculateOrderTotalPrice(order));
        } catch (ServiceFailureException t) {
            t.printStackTrace();
            redirectAttributes.addFlashAttribute(
                "errorMessage",
                messageSource.getMessage("order.confirm.price.error.message", new Object[]{}, locale));
             return "redirect:" + uriBuilder.path("/order/list").build();
        }
        model.addAttribute("order", order);
        return "order/new_confirm";
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@ModelAttribute OrderDto order,
            BindingResult bindingResult, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("update(locale={}, order={})", locale, order);
        if (order.getId() == 0) {
            try {
                service.createOrder(order);
                redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("order.add.message", new Object[]{}, locale)
            );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("order.add.message.error", new Object[]{}, locale));
            }
        } else {
            try {
                service.updateOrder(order);
                redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("order.edit.message", new Object[]{String.valueOf(order.getId())}, locale)
            );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("order.edit.message.error", new Object[]{}, locale));
            }
        }
        return "redirect:" + uriBuilder.path("/order/list").build();
    }

    private List<CustomerDto> getSortedCustomers() {
        List<CustomerDto> customers = new ArrayList<>();
        
        if (SecurityHelper.loggedUserIsAdmin()) {
            customers = customerService.getAllCustomers();
        } else {
            customers.add(customerService.getCustomerByUsername(
                    SecurityHelper.getLoggedUserUsername()));
        }
        
        Collections.sort(customers, new Comparator<CustomerDto>() {
            @Override
            public int compare(CustomerDto a, CustomerDto b) {
                if (a.getName() == null || b.getName() == null ||
                        a.getName().equals(b.getName())) {
                    return (int) (b.getId() - a.getId());
                }
                return a.getName().compareTo(b.getName());
            }
        });
        return customers;
    }

    private List<TireDto> getSortedTires() {
        List<TireDto> tires = tireService.getAllTires();
        Collections.sort(tires, new Comparator<TireDto>() {
            @Override
            public int compare(TireDto a, TireDto b) {
                if (a.getManufacturer() == null || b.getManufacturer() == null || 
                        a.getManufacturer().equals(b.getManufacturer())) {
                    return (int) (b.getId() - a.getId());
                }
                return a.getManufacturer().compareTo(b.getManufacturer());
            }
        });
        return tires;
    }
    
    private List<AdditionalServiceDto> getSortedAdditionalServices() {
        List<AdditionalServiceDto> services = additionalServiceService.getAllAdditionalServices();
        Collections.sort(services, new Comparator<AdditionalServiceDto>() {
            @Override
            public int compare(AdditionalServiceDto a, AdditionalServiceDto b) {
                if (a.getName() == null || b.getName() == null || 
                        a.getName().equals(b.getName())) {
                    return (int) (b.getId() - a.getId());
                }
                return a.getName().compareTo(b.getName());
            }
        });
        return services;
    }
    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "additionalServices", new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                log.debug("convertElement - additionalServices {}", element);
                if (element instanceof AdditionalServiceDto) {
                    return element;
                }
                if (element instanceof String) {
                    try {
                        return additionalServiceService.getAdditionalServiceByName((String) element);
                    } catch (ServiceFailureException e) {
                        log.debug("ServiceFailureException {}", e.getMessage());
                        e.printStackTrace();
                    }
                }
                log.debug("element not recognized");
                return null;
            }
        });
        binder.registerCustomEditor(TireDto.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                log.debug("setAsText - tire {}", text);
                try {
                    TireDto dto = tireService.getTireById(Long.parseLong((String) text));
                    setValue(dto);
                    return;
                } catch (ServiceFailureException e) {
                    log.debug("ServiceFailureException {}", e.getMessage());
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    log.debug("NumberFormatException");
                }
                log.debug("not recognized");
            }
        });
        binder.registerCustomEditor(CustomerDto.class, new PropertyEditorSupport() {
            @Override
            public void setAsText(String text) throws IllegalArgumentException {
                log.debug("setAsText - customer {}", text);
                try {
                    CustomerDto dto = customerService.getCustomerById(Long.parseLong((String) text));
                    setValue(dto);
                    return;
                } catch (ServiceFailureException e) {
                    log.debug("ServiceFailureException {}", e.getMessage());
                    e.printStackTrace();
                } catch (NumberFormatException e) {
                    log.debug("NumberFormatException");
                }
                log.debug("not recognized");
            }
        });
    }
}
