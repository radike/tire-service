/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.web.controllers;

import java.util.Locale;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.UriComponentsBuilder;

/**
 * Homepage Spring MVC controller
 *
 * @author Radoslav Rábara
 */
@Controller
@RequestMapping("/")
public class HomepageController {

    @Autowired
    private MessageSource messageSource;
    
    @RequestMapping()
    public String showHomepage(UriComponentsBuilder uriBuilder) {
        return "redirect:" + uriBuilder.path("/order/list").build();
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login(
            @RequestParam(value = "error", required = false) String error,
            @RequestParam(value = "logout", required = false) String logout,
            Locale locale) {
        ModelAndView model = new ModelAndView();
        if (error != null) {
            model.addObject("error",
                    messageSource.getMessage("login.invalid", new Object[]{}, locale));
        }
        model.setViewName("login");
        return model;

    }
}
