/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.web.controllers;

import cz.muni.fi.pa165.tireservice.dto.CustomerDto;
import cz.muni.fi.pa165.tireservice.security.SecurityHelper;
import cz.muni.fi.pa165.tireservice.services.CustomerService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import javax.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomCollectionEditor;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.util.UriComponentsBuilder;

/**
 *
 * @author Marek Halas
 */
@Controller
@RequestMapping("/customer")
public class CustomerController {
 
    final static Logger log = LoggerFactory.getLogger(CustomerController.class);

    @Autowired
    private CustomerService service;

    @Autowired
    private MessageSource messageSource;
        
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String defaultView(Model model) {
        return showList(model);
    }
    
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String showList(Model model) {
        log.debug("showList()");
        
        if (SecurityHelper.loggedUserIsAdmin()) {
            model.addAttribute("customers", service.getAllCustomers());
        } else {
            model.addAttribute("customers", Arrays.asList(
                    service.getCustomerByUsername(
                    SecurityHelper.getLoggedUserUsername())));
        }        
        return "customer/list";
    }
    
    @RequestMapping(value = "/new", method = RequestMethod.GET)
    public String showNewCustomer(Model model) {
        log.debug("showNewCustomer()");
        model.addAttribute("customer", new CustomerDto());
        return "customer/new";
    }

    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public String showCustomerDetail(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showCustomerDetail() id: {}", id);
        try {
            CustomerDto dto = service.getCustomerById(id);
            if (dto != null) {
                model.addAttribute("customer", dto);
                log.debug("customer was found {}", dto);
                return "customer/detail";
            }
            log.debug("not found");
        } catch(ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("customer.not.found", new Object[]{id}, locale));
        return "customer/list";  
    }
    
    @RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
    public String showEditCustomer(@PathVariable("id") long id, Model model, Locale locale) {
        log.debug("showEditCustomer() id: {}", id);
        try {
            CustomerDto dto = service.getCustomerById(id);
            if (dto != null) {
                model.addAttribute("customer", dto);
                log.debug("customer was found {}", dto);
                return "customer/edit";
            }
            log.debug("not found");
        } catch(ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
        }
        model.addAttribute("errorMessage",
                messageSource.getMessage("customer.not.found", new Object[]{id}, locale));
        return "customer/list";  
    }
  
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public String deleteCustomer(@PathVariable("id") long id, Model model,
            RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {
        
        log.debug("deleteCustomer() id: {}", id);
        try {
            CustomerDto dto = service.getCustomerById(id);
            if (dto != null) {
                log.debug("customer was found {}", dto);
                service.removeCustomer(dto);
                redirectAttributes.addFlashAttribute(
                        "message",
                        messageSource.getMessage("customer.remove.message", new Object[]{String.valueOf(id)}, locale)
                );
            } else {
                redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("customer.not.found", new Object[]{String.valueOf(id)}, locale));
            }
        } catch(EntityHasOrderException e) {
            log.debug("ServiceFailureException {}", e);
            redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("customer.remove.errorOrderMessage", new Object[]{String.valueOf(id)}, locale));
        } catch(ServiceFailureException e) {
            log.debug("ServiceFailureException {}", e);
            e.printStackTrace();
            redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("customer.remove.errorMessage", new Object[]{String.valueOf(id)}, locale));
        }
        return "redirect:" + uriBuilder.path("/customer/list").build();
    }
    
    @RequestMapping(value = "/create", method = {RequestMethod.POST})
    public String create(@Valid @ModelAttribute CustomerDto customer,
            BindingResult result, Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("confirm(locale={}, customer={})", locale, customer);
        if (result.hasErrors()) {
            for (ObjectError ge : result.getGlobalErrors()) {
                log.debug("ObjectError: {}", ge);
            }
            for (FieldError fe : result.getFieldErrors()) {
                log.debug("FieldError: {}", fe);
            }
            model.addAttribute("customer", customer);
            return "customer/new";
        }
        try {
            service.createCustomer(customer);
            redirectAttributes.addFlashAttribute(
                "message",
                messageSource.getMessage("customer.add.message", new Object[]{}, locale)
            );
        } catch (ServiceFailureException t) {
            t.printStackTrace();
            redirectAttributes.addFlashAttribute(
                "errorMessage",
                messageSource.getMessage("customer.add.message.error", new Object[]{}, locale));
        }
            
        return "redirect:" + uriBuilder.path("/customer/list").build();
    }
    
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@Valid @ModelAttribute CustomerDto customer,
            BindingResult result, Model model, RedirectAttributes redirectAttributes,
            UriComponentsBuilder uriBuilder, Locale locale) {

        log.debug("update(locale={}, customer={})", locale, customer);
        if (result.hasErrors()) {
            for (ObjectError ge : result.getGlobalErrors()) {
                log.debug("ObjectError: {}", ge);
            }
            for (FieldError fe : result.getFieldErrors()) {
                log.debug("FieldError: {}", fe);
            }
            model.addAttribute("customer", customer);
            return "customer/edit";
        }
        if (customer.getId() == 0) {
            try {
                service.createCustomer(customer);
                redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("customer.add.message", new Object[]{}, locale)
            );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("customer.add.message.error", new Object[]{}, locale));
            }
        } else {
            try {
                service.updateCustomer(customer);
                redirectAttributes.addFlashAttribute(
                    "message",
                    messageSource.getMessage("customer.edit.message", new Object[]{String.valueOf(customer.getId())}, locale)
                );
            } catch (ServiceFailureException t) {
                t.printStackTrace();
                redirectAttributes.addFlashAttribute(
                    "errorMessage",
                    messageSource.getMessage("customer.edit.message.error", new Object[]{}, locale));
            }
        }
        return "redirect:" + uriBuilder.path("/customer/list").build();
    } 

    
    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(List.class, "customers", new CustomCollectionEditor(List.class) {
            @Override
            protected Object convertElement(Object element) {
                log.debug("convertElement - customers {}", element);
                if (element instanceof CustomerDto) {
                    return element;
                }

                log.debug("element not recognized");
                return null;
            }
        });   
    }
}
