/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.rest.security;

import cz.muni.fi.pa165.tireservice.enums.UserRole;
import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

/**
 *
 * @author Radoslav Rabara
 */
public class RestAuth {
    
    public static void restAuthentication() {
        Authentication authToken = new UsernamePasswordAuthenticationToken (
                "rest", "rest",
                Arrays.asList(new SimpleGrantedAuthority(UserRole.ROLE_ADMIN.toString())));
        SecurityContextHolder.getContext().setAuthentication(authToken);
    }
    
    private RestAuth() {
        
    }
}
