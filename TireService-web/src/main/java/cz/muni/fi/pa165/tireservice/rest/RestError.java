/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.rest;

/**
 * RestError entity used to return error from REST controllers.
 * 
 * @author Radoslav Rabara
 */
public class RestError {

    private String error;
    
    public RestError(String message) {
        this.error = message;
    }

    public String getError() {
        return error;
    }

    public void setError(String message) {
        this.error = message;
    }
    
}
