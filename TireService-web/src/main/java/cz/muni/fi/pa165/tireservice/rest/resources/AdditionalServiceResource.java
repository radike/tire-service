/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.rest.resources;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import cz.muni.fi.pa165.tireservice.rest.RestError;
import cz.muni.fi.pa165.tireservice.rest.security.RestAuth;
import cz.muni.fi.pa165.tireservice.services.exceptions.ServiceFailureException;
import cz.muni.fi.pa165.tireservice.services.AdditionalServiceService;
import cz.muni.fi.pa165.tireservice.services.exceptions.EntityHasOrderException;
import java.net.HttpURLConnection;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.stereotype.Controller;

/**
 * AdditionalService REST Controller
 *
 * @author martin
 */
@Controller
@Path("/additionalServices")
public class AdditionalServiceResource {

    final static Logger log = LoggerFactory.getLogger(AdditionalServiceResource.class);
    
    private static final RestError INTERNAL_SERVER_ERROR = new RestError("additionalService service error");
    private static final RestError NOT_FOUND_ERROR = new RestError("specified id was not found");
    private static final RestError BAD_REQUEST_ERROR = new RestError("bad request");
     private static final RestError ENTITY_ALREADY_EXISTS = new RestError("entity already exists");
    private static final RestError ENTITY_HAS_ORDER_ERROR = new RestError("entity has order");

    private static final Response BAD_REQUEST_RESPONSE = Response
                                    .status(HttpURLConnection.HTTP_BAD_REQUEST)
                                    .entity(BAD_REQUEST_ERROR)
                                    .build();
    
     private static final Response ENTITY_WITH_NAME_ALREADY_EXISTS= Response
                                    .status(HttpURLConnection.HTTP_CONFLICT)
                                    .entity(ENTITY_ALREADY_EXISTS)
                                    .build();

    private static final Response ENTITY_HAS_ORDER_ERROR_RESPONSE = Response
                                    .status(HttpURLConnection.HTTP_BAD_REQUEST)
                                    .entity(ENTITY_HAS_ORDER_ERROR)
                                    .build();
    
    private static final Response SERVICE_FAILURE_RESPONSE = Response
            .status(HttpURLConnection.HTTP_SERVER_ERROR)
            .entity(INTERNAL_SERVER_ERROR)
            .build();
    
    private static final Response NOT_FOUND_RESPONSE = Response
            .status(HttpURLConnection.HTTP_NOT_FOUND)
            .entity(NOT_FOUND_ERROR)
            .build();

    @Autowired
    private AdditionalServiceService service;

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAdditionalServices() {
        try {
            RestAuth.restAuthentication();
            List<AdditionalServiceDto> additionalServices = service.getAllAdditionalServices();
            return Response.ok(additionalServices, MediaType.APPLICATION_JSON).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @GET
    @Path("{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAdditionalService(@PathParam("id") long id) {
        try {
            RestAuth.restAuthentication();
            AdditionalServiceDto additionalService = service.getAdditionalServiceById(id);
            if (additionalService == null) {
                return NOT_FOUND_RESPONSE;
            }
            return Response.ok(additionalService, MediaType.APPLICATION_JSON).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @POST
    @Path("")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response createAdditionalService(AdditionalServiceDto additionalService) {
        try {
            RestAuth.restAuthentication();
            if(service.getAdditionalServiceByName(additionalService.getName()) != null) {
                return ENTITY_WITH_NAME_ALREADY_EXISTS;
            }
            service.createAdditionalService(additionalService);
            return Response.status(HttpURLConnection.HTTP_CREATED).entity(additionalService).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @PUT
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response updateAdditionalService(@PathParam("id") long id, AdditionalServiceDto additionalService) {
        if (additionalService == null || additionalService.getId() != id) {
            return BAD_REQUEST_RESPONSE;
        }
        try {
            RestAuth.restAuthentication();
            AdditionalServiceDto fromDb = service.getAdditionalServiceById(id);
            if (fromDb == null) {
                return NOT_FOUND_RESPONSE;
            }
            if (additionalService.getName() != null && !additionalService.getName().equals(fromDb.getName())) {
                fromDb.setName(additionalService.getName());
                if(service.getAdditionalServiceByName(additionalService.getName()) != null) {
                    return ENTITY_WITH_NAME_ALREADY_EXISTS;
                }
            }
            if (additionalService.getPrice() != null) {
                fromDb.setPrice(additionalService.getPrice());
            }
            service.updateAdditionalService(additionalService);
            return Response.ok(additionalService, MediaType.APPLICATION_JSON).build();
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    @DELETE
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteAdditionalService(@PathParam("id") long id) {
        try {
            RestAuth.restAuthentication();
            AdditionalServiceDto additionalService = service.getAdditionalServiceById(id);
            if (additionalService == null) {
                return NOT_FOUND_RESPONSE;
            }
            service.removeAdditionalService(additionalService);
            return Response.noContent().build();
        } catch(EntityHasOrderException e) {
            return ENTITY_HAS_ORDER_ERROR_RESPONSE;
        } catch (ServiceFailureException e) {
            log.error("ServiceFailureException", e);
            if (serviceFailedOnIllegalArgument(e)) {
                return BAD_REQUEST_RESPONSE;
            }
        }
        return SERVICE_FAILURE_RESPONSE;
    }

    private boolean serviceFailedOnIllegalArgument(ServiceFailureException e) {
        Throwable cause = e.getCause();
        if (cause != null && cause.getClass().isAssignableFrom(InvalidDataAccessApiUsageException.class)) {
            Throwable causeOfCause = cause.getCause();
            if (causeOfCause != null) {
                if (causeOfCause.getClass().isAssignableFrom(IllegalArgumentException.class)) {
                    return true;
                }
            }
        }
        return false;
    }
}
