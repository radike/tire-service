/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui.tires;

import cz.muni.fi.pa165.tireservice.dto.TireDto;
import cz.muni.fi.pa165.tireservice.enums.TireType;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Radoslav Rabara
 */
public class TireDialog extends JDialog {

    private TireDto tire;

    private JComboBox typeCombo;
    private JTextField sizeTextField, manufacturerTextField, priceTextField;
    private JButton button;

    public TireDialog(JFrame owner, TireDto tire) {
        super(owner);

        // add component
        addComponents();
        button.setText(tire == null ? "Create" : "Update");
        setTitle(tire == null ? "Create new tire" : "Update tire id: " + tire.getId());
        if (tire != null) {
            typeCombo.setSelectedItem(tire.getType());
            sizeTextField.setText(String.valueOf(tire.getSize()));
            manufacturerTextField.setText(tire.getManufacturer());
            priceTextField.setText(tire.getPrice().toPlainString());
        }

        setLayout(new GridLayout(0, 2));
        setModal(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(new Dimension(340, 160));
        setVisible(true);
    }

    private void addComponents() {
        add(new JLabel("Type:"));
        List<String> types = new ArrayList<String>();
        for (TireType val : TireType.values()) {
            types.add(val.toString());
        }
        add(typeCombo = new JComboBox(types.toArray(new String[0])));
        add(new JLabel("Size:"));
        add(sizeTextField = new JTextField("0"));
        add(new JLabel("Manufacturer:"));
        add(manufacturerTextField = new JTextField("Some manufacturer"));
        add(new JLabel("Price:"));
        add(priceTextField = new JTextField("0"));
        add(button = new JButton());
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String typeStr = typeCombo.getSelectedItem().toString();
                String manufacturer = manufacturerTextField.getText();
                String priceStr = priceTextField.getText();
                String sizeStr = sizeTextField.getText();
                if (typeStr.length() == 0) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Fill 'Type' field.");
                    typeCombo.requestFocus();
                    return;
                }
                if (sizeStr.length() == 0) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Fill 'Size' field.");
                    sizeTextField.requestFocus();
                    return;
                }
                if (manufacturer.length() == 0) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Fill 'Manufacturer' field.");
                    manufacturerTextField.requestFocus();
                    return;
                }
                if (priceStr.length() == 0) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Fill 'Price' field.");
                    priceTextField.requestFocus();
                    return;
                }
                if (sizeStr.length() == 0) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Fill 'Size' field.");
                    sizeTextField.requestFocus();
                    return;
                }
                TireType type = null;
                for (TireType val : TireType.values()) {
                    if (val.toString().equals(typeStr)) {
                        type = val;
                    }
                }
                if (type == null) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Invalid selection of 'Type'.");
                    typeCombo.requestFocus();
                    return;
                }
                int size = -1;
                try {
                    size = Integer.parseInt(sizeStr);
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Invalid size.");
                    sizeTextField.requestFocus();
                    return;
                }
                BigDecimal price = null;
                try {
                    price = new BigDecimal(priceTextField.getText());
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(TireDialog.this, "Invalid price.");
                    priceTextField.requestFocus();
                    return;
                }
                tire = new TireDto();
                tire.setManufacturer(manufacturer);
                tire.setType(type);
                tire.setSize(size);
                tire.setPrice(price);
                TireDialog.this.setVisible(false);
            }
        });
    }

    public TireDto getTire() {
        return tire;
    }
}
