/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client;

import cz.muni.fi.pa165.tireservice.client.gui.MainWindow;
import cz.muni.fi.pa165.tireservice.client.gui.tires.TirePresenter;
import java.util.logging.Logger;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author Radoslav Rabara
 */
public class Application {

    private static final Logger log = Logger.getLogger(TirePresenter.class.getName());
    
    public static void main(String[] args) {
        try {
            // Set System L&F
            UIManager.setLookAndFeel(
                    UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException e) {
            log.fine(e.getMessage());
        } catch (ClassNotFoundException e) {
            log.fine(e.getMessage());
        } catch (InstantiationException e) {
            log.fine(e.getMessage());
        } catch (IllegalAccessException e) {
            log.fine(e.getMessage());
        }
        MainWindow mainWindow = new MainWindow();
        mainWindow.setVisible(true);
    }
}
