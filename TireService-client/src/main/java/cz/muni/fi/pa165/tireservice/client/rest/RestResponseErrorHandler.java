/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.rest;

import java.io.IOException;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.DefaultResponseErrorHandler;

/**
 *
 * @author Radoslav Rabara
 */
public class RestResponseErrorHandler implements org.springframework.web.client.ResponseErrorHandler {

        private org.springframework.web.client.ResponseErrorHandler myErrorHandler = new DefaultResponseErrorHandler();

        @Override
        public boolean hasError(ClientHttpResponse chr) throws IOException {
            return myErrorHandler.hasError(chr);
        }

        @Override
        public void handleError(ClientHttpResponse chr) throws IOException {
            switch (chr.getStatusCode()) {
                case BAD_REQUEST:
                    if (chr.getBody() != null) {
                        java.util.Scanner s = new java.util.Scanner(chr.getBody()).useDelimiter("\\A");
                        String body = s.hasNext() ? s.next() : "";
                        if (body != null) {
                            final String errorStr = "\"error\"";
                            int idx = body.indexOf(errorStr);
                            if (idx >= 0) {
                                body = body.substring(idx+errorStr.length());
                                idx = body.indexOf("\"");
                                if (idx > 0) {
                                    body = body.substring(idx+1);
                                    idx = body.indexOf("\"");
                                    if (idx > 0) {
                                        body = body.substring(0, idx);
                                    }
                                }
                            }
                            if (body.length() > 0) {
                                throw new RestServiceFailure(RestServiceFailure.ErrorMessage.BAD_REQUEST, body);
                            }
                        }
                    }
                    throw new RestServiceFailure(RestServiceFailure.ErrorMessage.BAD_REQUEST);
                case NOT_FOUND:
                    throw new RestServiceFailure(RestServiceFailure.ErrorMessage.NOT_FOUND);
                default:
                    if (chr.getStatusCode().is5xxServerError()) {
                        throw new RestServiceFailure(RestServiceFailure.ErrorMessage.SERVER_ERROR);
                    }
            };
            myErrorHandler.handleError(chr);
        }
    }
