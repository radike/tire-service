/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui.additionalServices;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author martin
 */
public class AdditionalServiceTableModel extends AbstractTableModel {

    private List<AdditionalServiceDto> additionalServices = new ArrayList<AdditionalServiceDto>();
    private String[] columns = {"ID", "Name", "Price"};

    public void setData(List<AdditionalServiceDto> additionalServices) {
        this.additionalServices = additionalServices;
        this.fireTableDataChanged();
    }

    public void update(AdditionalServiceDto origin, AdditionalServiceDto updated) {
        int i = additionalServices.indexOf(origin);
        if (i >= 0) {
            additionalServices.set(i, updated);
            this.fireTableRowsUpdated(i, i);
        }
    }
    
    public void add(AdditionalServiceDto additionalService) {
        additionalServices.add(additionalService);
        this.fireTableRowsInserted(additionalServices.size()-1, additionalServices.size()-1);
    }
    
    public void remove(AdditionalServiceDto additionalService) {
        int i = additionalServices.indexOf(additionalService);
        if (i >= 0) {
            additionalServices.remove(additionalService);
            this.fireTableRowsDeleted(i, i);
        }
    }

    public AdditionalServiceDto get(int index) {
        return additionalServices.get(index);
    }
    
    @Override
    public int getRowCount() {
        return additionalServices.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        AdditionalServiceDto additionalService = additionalServices.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return additionalService.getId();
            case 1:
                return additionalService.getName();
            case 2:
                return additionalService.getPrice();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int i) {
        return columns[i];
    }
}
