/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui.additionalServices;

import cz.muni.fi.pa165.tireservice.client.gui.MainPanel;
import cz.muni.fi.pa165.tireservice.client.gui.MainPanel.MainPanelController;
import cz.muni.fi.pa165.tireservice.client.rest.RestServiceFailure;
import cz.muni.fi.pa165.tireservice.client.rest.AdditionalServiceRestService;
import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 * martin
 */
public class AdditionalServicePresenter implements MainPanelController {

    private static final Logger log = Logger.getLogger(AdditionalServicePresenter.class.getName());

    private final MainPanel mainPanel;
    private final AdditionalServiceTableModel tableModel = new AdditionalServiceTableModel();

    public AdditionalServicePresenter(MainPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    @Override
    public AbstractTableModel getTableModel() {
        return tableModel;
    }

    @Override
    public void actionAdd(JFrame owner) {
        AdditionalServiceDialog dialog = new AdditionalServiceDialog(owner, null);
        AdditionalServiceDto additionalService = dialog.getAdditionalService();
        if (additionalService != null) {
            try {
                mainPanel.disableActions();
                additionalService = new AdditionalServiceRestService().create(additionalService);
                tableModel.add(additionalService);
            } catch (RestServiceFailure ex) {
                log.log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                        "Create additional service", JOptionPane.ERROR_MESSAGE);
            } catch (IllegalArgumentException ex) {
                log.log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                        "Create additional service", JOptionPane.ERROR_MESSAGE);
            } finally {
                mainPanel.enableActions();
            }
        }
    }

    @Override
    public void actionEdit(JFrame owner, int selectedRow) {
        AdditionalServiceDto origin = tableModel.get(selectedRow);
        AdditionalServiceDialog dialog = new AdditionalServiceDialog(owner, origin);
        AdditionalServiceDto updated = dialog.getAdditionalService();
        if (updated != null) {
            updated.setId(origin.getId());
            try {
                mainPanel.disableActions();
                new AdditionalServiceRestService().update(updated);
                tableModel.update(origin, updated);
            } catch (RestServiceFailure ex) {
                log.log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                        "Update additionalService", JOptionPane.ERROR_MESSAGE);
            }  catch (IllegalArgumentException ex) {
                log.log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                        "Update additional service", JOptionPane.ERROR_MESSAGE);
            } finally {
                mainPanel.enableActions();
            }
        }
    }

    @Override
    public void actionRemove(JFrame owner, int selectedRow) {
        try {
            mainPanel.disableActions();
            AdditionalServiceDto additionalService = tableModel.get(selectedRow);
            new AdditionalServiceRestService().delete(additionalService.getId());
            tableModel.remove(additionalService);
        } catch (RestServiceFailure ex) {
            log.log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                    "Delete additional service", JOptionPane.ERROR_MESSAGE);
        } finally {
            mainPanel.enableActions();
        }

    }

    @Override
    public void actionRefresh(JFrame owner) {
        new LoadAdditionalServices(owner).start();
    }

    private class LoadAdditionalServices extends Thread {

        private JFrame owner;

        public LoadAdditionalServices(JFrame owner) {
            this.owner = owner;
        }

        @Override
        public void run() {
            try {
                mainPanel.showProgressBar("Loading additional services");
                mainPanel.disableActions();

                List<AdditionalServiceDto> additionalServices = new AdditionalServiceRestService().getAll();
                tableModel.setData(additionalServices);
            } catch (RestServiceFailure ex) {
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getErrorMessage(),
                        "Load list of additional services", JOptionPane.ERROR_MESSAGE);
                log.log(Level.SEVERE, null, ex);
            } finally {
                mainPanel.hideProgressBar();
                mainPanel.enableActions();
            }
        }
    }
}
