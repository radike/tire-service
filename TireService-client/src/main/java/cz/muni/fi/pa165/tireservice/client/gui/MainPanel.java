/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author radike
 */
public class MainPanel extends JPanel {

    private JFrame owner;
    private final JProgressBar progressBar = new JProgressBar();
    private final JTable table = new JTable();
    private JButton addButton, editButton, removeButton, refreshButton;

    public static interface MainPanelController {

        void actionAdd(JFrame owner);

        void actionEdit(JFrame owner, int i);

        void actionRemove(JFrame owner, int i);

        void actionRefresh(JFrame owner);

        AbstractTableModel getTableModel();
    }
    private MainPanelController controller;

    public MainPanel(JFrame owner) {
        this.owner = owner;
        setLayout(new BorderLayout());
        addComponents();
    }

    public void setController(MainPanelController controller) {
        if (this.controller != controller) {
            this.controller = controller;
            table.setModel(controller.getTableModel());
            controller.actionRefresh(owner);
        }
    }

    public void showProgressBar(String text) {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                progressBar.setString("Loading tires");
                progressBar.setVisible(true);
            }
        });
    }

    public void hideProgressBar() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                progressBar.setVisible(false);
            }
        });
    }

    private void addComponents() {
        // table
        add(new JScrollPane(table), BorderLayout.CENTER);
        table.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
                boolean enableTableActions = table.getRowCount() > 0;
                if (enableTableActions && table.getSelectedRow() < 0) {
                    table.setRowSelectionInterval(0, 0);
                }
                editButton.setEnabled(enableTableActions);
                removeButton.setEnabled(enableTableActions);
            }

            @Override
            public void focusLost(FocusEvent e) {
                enableTableActions();
            }

        });
        // progress
        add(progressBar, BorderLayout.SOUTH);
        progressBar.setIndeterminate(true);
        progressBar.setStringPainted(true);

        // controls
        JPanel controls = new JPanel();
        controls.setLayout(new FlowLayout());

        controls.add(addButton = new JButton("Add"));
        controls.add(editButton = new JButton("Edit"));
        controls.add(removeButton = new JButton("Remove"));
        controls.add(refreshButton = new JButton("Refresh List"));

        add(controls, BorderLayout.NORTH);

        addButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                controller.actionAdd(owner);
            }

        });

        editButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow >= 0) {
                    controller.actionEdit(owner, selectedRow);
                } else {
                    JOptionPane.showMessageDialog(owner, "First select tire to edit",
                            "Edit tire", JOptionPane.ERROR_MESSAGE);
                }
            }

        });
        removeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int selectedRow = table.getSelectedRow();
                if (selectedRow >= 0) {
                    controller.actionRemove(owner, selectedRow);
                } else {
                    JOptionPane.showMessageDialog(owner, "First select tire to delete",
                            "Delete tire", JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        refreshButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.actionRefresh(owner);
            }
        });
    }

    public void enableActions() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                // set controls
                addButton.setEnabled(true);
                enableTableActions();
                refreshButton.setEnabled(true);
                table.setEnabled(true);

                addButton.requestFocus();
                table.requestFocus();
            }

        });
    }

    private void enableTableActions() {
        boolean enableTableActions = table.getRowCount() > 0 && table.getSelectedRow() >= 0;
        editButton.setEnabled(enableTableActions);
        removeButton.setEnabled(enableTableActions);
    }

    public void disableActions() {
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                addButton.setEnabled(false);
                editButton.setEnabled(false);
                refreshButton.setEnabled(false);
                removeButton.setEnabled(false);
                table.setEnabled(false);
            }

        });
    }
}
