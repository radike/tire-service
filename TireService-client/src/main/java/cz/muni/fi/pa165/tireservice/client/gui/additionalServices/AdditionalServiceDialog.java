/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui.additionalServices;

import cz.muni.fi.pa165.tireservice.dto.AdditionalServiceDto;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Radoslav Rabara
 */
public class AdditionalServiceDialog extends JDialog {

    private AdditionalServiceDto additionalService;

    private JTextField nameTextField, priceTextField;
    private JButton button;

    public AdditionalServiceDialog(JFrame owner, AdditionalServiceDto additionalService) {
        super(owner);

        // add component
        addComponents();
        button.setText(additionalService == null ? "Create" : "Update");
        setTitle(additionalService == null ? "Create new additional service" : "Update additional service id: " + additionalService.getId());
        if (additionalService != null) {
            nameTextField.setText(String.valueOf(additionalService.getName()));
            priceTextField.setText(additionalService.getPrice().toPlainString());
        }

        setLayout(new GridLayout(0, 2));
        setModal(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        setSize(new Dimension(340, 160));
        setVisible(true);
    }

    private void addComponents() {
        add(new JLabel("Name:"));
        add(nameTextField = new JTextField(""));
        add(new JLabel("Price:"));
        add(priceTextField = new JTextField("0"));
        add(button = new JButton());
        button.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                String nameStr = nameTextField.getText();
                String priceStr = priceTextField.getText();
                
                if (nameStr.length() == 0) {
                    JOptionPane.showMessageDialog(AdditionalServiceDialog.this, "Fill 'Name' field.");
                    nameTextField.requestFocus();
                    return;
                }
                if (priceStr.length() == 0) {
                    JOptionPane.showMessageDialog(AdditionalServiceDialog.this, "Fill 'Price' field.");
                    priceTextField.requestFocus();
                    return;
                }
                
                BigDecimal price = null;
                try {
                    price = new BigDecimal(priceTextField.getText());
                } catch (NumberFormatException nfe) {
                    JOptionPane.showMessageDialog(AdditionalServiceDialog.this, "Invalid price.");
                    priceTextField.requestFocus();
                    return;
                }
                
                additionalService = new AdditionalServiceDto();
                additionalService.setName(nameStr);
                additionalService.setPrice(price);
                AdditionalServiceDialog.this.setVisible(false);
            }
        });
    }

    public AdditionalServiceDto getAdditionalService() {
        return additionalService;
    }
}
