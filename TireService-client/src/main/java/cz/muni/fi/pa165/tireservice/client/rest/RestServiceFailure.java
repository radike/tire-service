/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.rest;

/**
 *
 * @author Radoslav Rabara
 */
public class RestServiceFailure extends java.io.IOException {

        public static enum ErrorMessage {

            UNEXPECTED_RESPONSE("Unexpected response"),
            UNKNOWN_PROBLEM("Unknown problem"),
            BAD_REQUEST("Bad request"),
            REFUSED_CONNECTION("Refused connection"),
            SERVER_ERROR("Server error"),
            NOT_FOUND("Not found");
            private String text;

            ErrorMessage(String text) {
                this.text = text;
            }

            @Override
            public String toString() {
                return text;
            }
        }

        private final ErrorMessage message;

        public RestServiceFailure(ErrorMessage message, String description) {
            super(message.toString() + ", " + description);
            this.message = message;
        }
        
        public RestServiceFailure(ErrorMessage message) {
            super(message.toString());
            this.message = message;
        }

        public RestServiceFailure(ErrorMessage message, Throwable cause) {
            super(message.toString(), cause);
            this.message = message;
        }

        public ErrorMessage getErrorMessage() {
            return message;
        }
    }
