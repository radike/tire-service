/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui.tires;

import cz.muni.fi.pa165.tireservice.dto.TireDto;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author radike
 */
public class TireTableModel extends AbstractTableModel {

    private List<TireDto> tires = new ArrayList<TireDto>();
    private String[] columns = {"ID", "Type", "Size", "Manufacturer", "Price"};

    public void setData(List<TireDto> tires) {
        this.tires = tires;
        this.fireTableDataChanged();
    }

    public void update(TireDto origin, TireDto updated) {
        int i = tires.indexOf(origin);
        if (i >= 0) {
            tires.set(i, updated);
            this.fireTableRowsUpdated(i, i);
        }
    }
    
    public void add(TireDto tire) {
        tires.add(tire);
        this.fireTableRowsInserted(tires.size()-1, tires.size()-1);
    }
    
    public void remove(TireDto tire) {
        int i = tires.indexOf(tire);
        if (i >= 0) {
            tires.remove(tire);
            this.fireTableRowsDeleted(i, i);
        }
    }

    public TireDto get(int index) {
        return tires.get(index);
    }
    
    @Override
    public int getRowCount() {
        return tires.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        TireDto tire = tires.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return tire.getId();
            case 1:
                return tire.getType();
            case 2:
                return tire.getSize();
            case 3:
                return tire.getManufacturer();
            case 4:
                return tire.getPrice();
            default:
                return "";
        }
    }

    @Override
    public String getColumnName(int i) {
        return columns[i];
    }
}
