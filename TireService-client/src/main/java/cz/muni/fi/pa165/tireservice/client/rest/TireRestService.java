/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.rest;

import cz.muni.fi.pa165.tireservice.client.rest.RestServiceFailure.ErrorMessage;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import java.util.List;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author Radoslav Rabara
 */
public class TireRestService {

    final static Logger log = LoggerFactory.getLogger(TireRestService.class);

    private static final String URL = "http://localhost:8080/pa165/rest/tires/";

    public TireDto create(TireDto tire) throws RestServiceFailure {
        try {
            RestTemplate restTemplate = new RestTemplate();
            ResponseEntity<String> response = restTemplate.postForEntity(URL, tire, String.class);
            log.debug("Response, status code: " + response.getStatusCode() + ", body: " + response.getBody());
            switch (response.getStatusCode()) {
                case CREATED:
                    // ok
                    ObjectMapper om = new ObjectMapper();
                    TireDto parsed = om.readValue(response.getBody(), TireDto.class);
                    return parsed;
                default:
                    throw new RestServiceFailure(ErrorMessage.UNEXPECTED_RESPONSE);
            }
        } catch (ResourceAccessException e) {
            throw new RestServiceFailure(ErrorMessage.REFUSED_CONNECTION, e);
        } catch (HttpClientErrorException e) {
            handleHttpClientErrorException(e);
        } catch (Throwable t) {
            throw new RestServiceFailure(ErrorMessage.UNKNOWN_PROBLEM, t);
        }
        throw new RestServiceFailure(ErrorMessage.UNKNOWN_PROBLEM);
    }

    public void update(TireDto tire) throws RestServiceFailure {
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new RestResponseErrorHandler());
            restTemplate.put(URL + tire.getId(), tire, String.class);
        } catch (ResourceAccessException e) {
            handleResourceAccessException(e);
        } catch (HttpClientErrorException e) {
            handleHttpClientErrorException(e);
        } catch (Throwable t) {
            throw new RestServiceFailure(ErrorMessage.UNKNOWN_PROBLEM, t);
        }
    }

    public void delete(long id) throws RestServiceFailure {
        log.debug("delete " + id);
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new RestResponseErrorHandler());
            restTemplate.delete(URL + id);
        } catch (ResourceAccessException e) {
            handleResourceAccessException(e);
        } catch (HttpClientErrorException e) {
            handleHttpClientErrorException(e);
        } catch (Throwable t) {
            throw new RestServiceFailure(ErrorMessage.UNKNOWN_PROBLEM, t);
        }
    }

    public List<TireDto> getAll() throws RestServiceFailure{
        try {
            RestTemplate restTemplate = new RestTemplate();
            restTemplate.setErrorHandler(new RestResponseErrorHandler());
            ResponseEntity<String> response = restTemplate.getForEntity(URL, String.class);
            log.debug("Response, status code: " + response.getStatusCode() + ", body: " + response.getBody());
            switch (response.getStatusCode()) {
                case OK:
                    // parse data
                    ObjectMapper om = new ObjectMapper();
                    List<TireDto> tires = om.readValue(
                        response.getBody(),
                        om.getTypeFactory().constructCollectionType(
                            List.class, TireDto.class));
                    return tires;
                default:
                    throw new RestServiceFailure(ErrorMessage.UNEXPECTED_RESPONSE);
            }
        } catch (ResourceAccessException e) {
            handleResourceAccessException(e);
        } catch (HttpClientErrorException e) {
            handleHttpClientErrorException(e);
        } catch (Throwable t) {
            throw new RestServiceFailure(ErrorMessage.UNKNOWN_PROBLEM, t);
        }
        throw new RestServiceFailure(ErrorMessage.UNKNOWN_PROBLEM);
    }

    private void handleResourceAccessException(ResourceAccessException e) throws RestServiceFailure {
        if (e.getCause() != null && e.getCause().getClass().isAssignableFrom(RestServiceFailure.class)) {
            throw (RestServiceFailure) e.getCause();
        }
        throw new RestServiceFailure(ErrorMessage.REFUSED_CONNECTION, e);
    }

    private void handleHttpClientErrorException(HttpClientErrorException e) throws RestServiceFailure {
        switch (e.getStatusCode()) {
            case BAD_REQUEST:
                throw new RestServiceFailure(ErrorMessage.BAD_REQUEST, e);
            default:
                if (e.getStatusCode().is5xxServerError()) {
                    throw new RestServiceFailure(ErrorMessage.SERVER_ERROR, e);
                }
        };
    }
}
