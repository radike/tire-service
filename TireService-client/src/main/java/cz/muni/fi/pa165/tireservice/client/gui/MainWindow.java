/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui;

import cz.muni.fi.pa165.tireservice.client.gui.additionalServices.AdditionalServicePresenter;
import cz.muni.fi.pa165.tireservice.client.gui.tires.TirePresenter;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * Main window of the application.
 *
 * @author Radoslav Rabara
 */
public class MainWindow extends JFrame {

    private Dimension size = new Dimension(800, 600);

    private MainPanel mainPanel;
    private TirePresenter tirePresenter;
    private AdditionalServicePresenter additionalServicePresenter;

    private final JButton additionaServicesButton = new JButton("Additional Services");
    private final JButton tiresButton = new JButton("Tires");

    public MainWindow() {
        // add components
        addComponents();

        // set controller
        tiresButton.setSelected(true);
        mainPanel.setController(tirePresenter);

        // set window
        setTitle("TireService-client");
        setSize(size);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    private void addComponents() {
        setLayout(new BorderLayout());

        addMenu();

        add(mainPanel = new MainPanel(this), BorderLayout.CENTER);
        tirePresenter = new TirePresenter(mainPanel);
        additionalServicePresenter = new AdditionalServicePresenter(mainPanel);
    }

    private void addMenu() {
        JPanel menu = new JPanel();
        add(menu, BorderLayout.WEST);

        Dimension menuSize = new Dimension(196, 640);
        menu.setMaximumSize(menuSize);
        menu.setPreferredSize(menuSize);
        menu.setMinimumSize(menuSize);
        menu.setBackground(new Color(50, 50, 50));
        menu.setLayout(null);

        JLabel title = new JLabel("TireService");
        menu.add(title);
        title.setBounds(0, 0, menuSize.width, 32);
        title.setVerticalAlignment(JLabel.CENTER);
        title.setVerticalTextPosition(JLabel.CENTER);
        title.setHorizontalAlignment(JLabel.CENTER);
        title.setHorizontalTextPosition(JLabel.CENTER);
        title.setForeground(Color.WHITE);
        title.setFont(title.getFont().deriveFont(Font.PLAIN, 32));

        final int buttonWidth = 196;

        menu.add(tiresButton);
        tiresButton.setBounds((menuSize.width - buttonWidth) / 2, 64, buttonWidth, 48);
        tiresButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                additionaServicesButton.setSelected(false);
                tiresButton.setSelected(true);
                mainPanel.setController(tirePresenter);
            }
        });

        menu.add(additionaServicesButton);
        additionaServicesButton.setBounds((menuSize.width - buttonWidth) / 2, 128, buttonWidth, 48);
        additionaServicesButton.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                additionaServicesButton.setSelected(true);
                tiresButton.setSelected(false);
                mainPanel.setController(additionalServicePresenter);
            }
        });
    }
}
