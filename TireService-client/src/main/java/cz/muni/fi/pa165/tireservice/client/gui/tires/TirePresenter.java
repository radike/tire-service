/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cz.muni.fi.pa165.tireservice.client.gui.tires;

import cz.muni.fi.pa165.tireservice.client.gui.MainPanel;
import cz.muni.fi.pa165.tireservice.client.gui.MainPanel.MainPanelController;
import cz.muni.fi.pa165.tireservice.client.rest.RestServiceFailure;
import cz.muni.fi.pa165.tireservice.client.rest.TireRestService;
import cz.muni.fi.pa165.tireservice.dto.TireDto;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

/**
 * Tire presenter
 *
 * @author Radoslav Rabara
 */
public class TirePresenter implements MainPanelController {

    private static final Logger log = Logger.getLogger(TirePresenter.class.getName());

    private final MainPanel mainPanel;
    private final TireTableModel tableModel = new TireTableModel();

    public TirePresenter(MainPanel mainPanel) {
        this.mainPanel = mainPanel;
    }

    @Override
    public AbstractTableModel getTableModel() {
        return tableModel;
    }

    @Override
    public void actionAdd(JFrame owner) {
        TireDialog dialog = new TireDialog(owner, null);
        TireDto tire = dialog.getTire();
        if (tire != null) {
            try {
                mainPanel.disableActions();
                tire = new TireRestService().create(tire);
                tableModel.add(tire);
            } catch (RestServiceFailure ex) {
                log.log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                        "Create tire", JOptionPane.ERROR_MESSAGE);
            } finally {
                mainPanel.enableActions();
            }
        }
    }

    @Override
    public void actionEdit(JFrame owner, int selectedRow) {
        TireDto origin = tableModel.get(selectedRow);
        TireDialog dialog = new TireDialog(owner, origin);
        TireDto updated = dialog.getTire();
        if (updated != null) {
            updated.setId(origin.getId());
            try {
                mainPanel.disableActions();
                new TireRestService().update(updated);
                tableModel.update(origin, updated);
            } catch (RestServiceFailure ex) {
                log.log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                        "Update tire", JOptionPane.ERROR_MESSAGE);
            } finally {
                mainPanel.enableActions();
            }
        }
    }

    @Override
    public void actionRemove(JFrame owner, int selectedRow) {
        try {
            mainPanel.disableActions();
            TireDto tire = tableModel.get(selectedRow);
            new TireRestService().delete(tire.getId());
            tableModel.remove(tire);
        } catch (RestServiceFailure ex) {
            log.log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getMessage(),
                    "Delete tire", JOptionPane.ERROR_MESSAGE);
        } finally {
            mainPanel.enableActions();
        }

    }

    @Override
    public void actionRefresh(JFrame owner) {
        new LoadTires(owner).start();
    }

    private class LoadTires extends Thread {

        private JFrame owner;

        public LoadTires(JFrame owner) {
            this.owner = owner;
        }

        @Override
        public void run() {
            try {
                mainPanel.showProgressBar("Loading tires");
                mainPanel.disableActions();

                List<TireDto> tires = new TireRestService().getAll();
                tableModel.setData(tires);
            } catch (RestServiceFailure ex) {
                JOptionPane.showMessageDialog(owner, "Error occured: " + ex.getErrorMessage(),
                        "Load list of tires", JOptionPane.ERROR_MESSAGE);
                log.log(Level.SEVERE, null, ex);
            } finally {
                mainPanel.hideProgressBar();
                mainPanel.enableActions();
            }
        }
    }
}
